﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Data;
using System.Globalization;
using System.Text;
using System.IO;
using System.Drawing;
using System.Net.NetworkInformation;
using System.Net;

public partial class _Default : System.Web.UI.Page
{
    CultureInfo EngCI = new System.Globalization.CultureInfo("en-US");
    GetExecution GE = new GetExecution();
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                GetUser_IP();
            }
        }
        catch { }
    }



    protected void GetUser_IP()
    {
        string VisitorsIPAddr = string.Empty;
        if (HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null)
        {
            VisitorsIPAddr = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
        }
        else if (HttpContext.Current.Request.UserHostAddress.Length != 0)
        {
            VisitorsIPAddr = HttpContext.Current.Request.UserHostAddress;
        }
        var remoteIpAddress = Request.UserHostAddress;
        lblIPAddress.Text = VisitorsIPAddr;
    }


    private void MESSAGEBOX(string msg)
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("alert('");
        sb.Append(msg);
        sb.Append("');");
        ScriptManager.RegisterStartupScript(this, this.GetType(), "showalert", sb.ToString(), true);
    }
    protected void BtnLogin_Click(object sender, EventArgs e)
    {
        try
        {
            string UserID = "", UserName = "", UserPass = "", UserLevelID = "";

            if (txtUserName.Text.Trim() == "")
            {
                RadAjaxManager1.Alert("Enter UserName");
                txtUserName.Focus();
                return;
            }

            if (txtPassword.Text.Trim() == "")
            {
                RadAjaxManager1.Alert("Enter Password");
                txtPassword.Focus();
                return;
            }

            #region
            string strSQL = " SELECT TOP(1) * FROM TblUser WHERE Username = '" + txtUserName.Text.Trim() + "' AND [Password] = '" + txtPassword.Text.Trim() + "' ";
            DataTable dt = GE.SELECT_Executeion(strSQL);
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];

                if (dr["Active"].ToString() == "N")
                {
                    RadAjaxManager1.Alert("เข้าระบบไม่สำเร็จ!! This User Not Active กรุณาติดต่อผู้ดูแลระบบ");
                    return;
                }
                else
                {
                    UserID = dr["UserID"].ToString();
                    UserLevelID = dr["UserLevelID"].ToString();
                    Session["UserID"] = UserID;
                    Session["UserLevelID"] = UserLevelID;
                    Session["UserFullName"] = dr["Fname"].ToString() + " " + dr["Lname"].ToString();
                    GE.INSERT_Executeion(" INSERT INTO TblUserLog (LogIpAddress2,CreateID,CreateDate,LogType) VALUES ('" + lblIPAddress.Text.Trim() + "','" + UserID + "',GETDATE(),'L') ");
                    switch (UserLevelID)
                    {
                        case "S":
                            Response.Redirect("~/ListMonitor.aspx");
                            break;
                        case "T":
                            Response.Redirect("~/ListAgent.aspx");
                            break;
                        case "Q":
                            Response.Redirect("~/ListMonitor.aspx");
                            break;
                        default:
                            RadAjaxManager1.Alert("Error : UserLevelID not support!! กรุณาติดต่อผู้ดูแลระบบ");
                            break;
                    }
                }
            }
            else
            {
                RadAjaxManager1.Alert("เข้าระบบไม่สำเร็จ!! UserName or Password Incorrect");
                return;
            }
            #endregion
        }
        catch (Exception ex)
        {
            RadAjaxManager1.Alert(ex.Message);
        }
    }
    protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
    {
        if (e.Argument.ToString() == "ok")
        {
            RadAjaxManager1.Alert("OK");
        }
        else
        {
            RadAjaxManager1.Alert("Cancel");
        }
    }
}