﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Data;

public partial class ListAgent : System.Web.UI.Page
{
    GetExecution GE = new GetExecution();
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!this.IsPostBack)
            {
                if (Session["UserID"] == null)
                {
                    Response.Redirect("~/Default.aspx");
                }

                if (Session["UserLevelID"].ToString() != "T")
                {
                    Response.Redirect("~/Default.aspx");
                }

                lblUserID.Text = Session["UserID"].ToString();
                lblUserLevelID.Text = Session["UserLevelID"].ToString();

                LoadCallStatus();
                LoadCallStatusSuccess();
                LoadGrid();

            }
        }
        catch (Exception ex)
        {
            RadAjaxManager1.Alert(ex.Message);
        }
    }
    private void LoadCallStatus()
    {
        try
        {
            string strSQL = " SELECT * FROM "
                + " (SELECT s.SubStatusID, s.SubStatusNameTH,(SELECT COUNT(1) FROM TblCustomer_master c LEFT JOIN TblDataSource d ON d.SourceID = c.SourceID  "
                + " WHERE s.SubStatusID = c.SubStatusID AND c.Active = 'Y' AND c.OwnerID='"+ lblUserID.Text.Trim() +"' ) AS Rec, s.Ordering FROM TblSubStatus s WHERE s.Active='Y') as TB1  "
                + " WHERE TB1.Rec > 0 AND TB1.SubStatusID NOT IN('2','3')  Order by TB1.Ordering    ";
            DataTable dt = GE.SELECT_Executeion(strSQL);
            if (dt.Rows.Count > 0)
            {
                RadGridStatus.DataSource = dt;
                RadGridStatus.DataBind();
            }
            else
            {
                RadGridStatus.DataSource = new Object[0];
                RadGridStatus.DataBind();
            }
            
        }
        catch (Exception ex)
        {
            RadAjaxManager1.Alert(ex.Message);
        }
    }

    private void LoadCallStatusSuccess()
    {
        try
        {
            string strSQL = " SELECT * FROM "
                + " (SELECT s.SubStatusID, s.SubStatusNameTH,(SELECT COUNT(1) FROM TblCustomer_master c LEFT JOIN TblDataSource d ON d.SourceID = c.SourceID  "
                + " WHERE s.SubStatusID = c.SubStatusID AND c.Active = 'Y' AND c.OwnerID='" + lblUserID.Text.Trim() + "' ) AS Rec, s.Ordering FROM TblSubStatus s WHERE s.Active='Y') as TB1  "
                + " WHERE TB1.Rec > 0 AND TB1.SubStatusID IN('2','3')  Order by TB1.Ordering    ";
            DataTable dt = GE.SELECT_Executeion(strSQL);
            if (dt.Rows.Count > 0)
            {
                RadGridStatusSuccess.DataSource = dt;
                RadGridStatusSuccess.DataBind();
            }
            else
            {
                RadGridStatusSuccess.DataSource = new Object[0];
                RadGridStatusSuccess.DataBind();
            }

        }
        catch (Exception ex)
        {
            RadAjaxManager1.Alert(ex.Message);
        }
    }
    public void LoadGrid()
    {
        try
        {
            RadGridShowList.DataSource = new Object[0];
            RadGridShowList.DataBind();
        }
        catch (Exception ex)
        {
            RadAjaxManager1.Alert(ex.Message);
        }
    }

    private void ListAllDataBoxStatus(string SubStatusID)
    {
        try
        {
            string strSQL = "";

            strSQL = " SELECT c.*, c.Title+' '+c.Name AS Fullname, c.HomePhone as SHomePhone, c.mobile as SMobile, c.OfficePhone as Soffice_phone FROM "
                      + " TblCustomer_master c "
                      + " WHERE c.SubStatusID='" + SubStatusID + "' AND c.OwnerID='" + lblUserID.Text.Trim() + "' AND c.Active = 'Y' "
                      + " ORDER BY c.LastCallDate ASC ";

            DataTable dt = GE.SELECT_Executeion(strSQL);
            if (dt.Rows.Count > 0)
            {
                RadGridShowList.DataSource = dt;
                RadGridShowList.DataBind();
            }
            else
            {
                RadGridShowList.DataSource = new Object[0];
                RadGridShowList.DataBind();
            }
        }
        catch (Exception ex)
        {
            RadAjaxManager1.Alert(ex.Message);
        }
    }

    private void ListAllDataBoxStatusSuccess(string SubStatusID)
    {
        try
        {
            string strSQL = "";

            strSQL = " SELECT c.*, c.Title+' '+c.Name AS Fullname, c.HomePhone as SHomePhone, c.mobile as SMobile, c.OfficePhone as Soffice_phone FROM "
                      + " TblCustomer_master c "
                      + " WHERE c.SubStatusID='" + SubStatusID + "' AND c.OwnerID='" + lblUserID.Text.Trim() + "' AND c.Active = 'Y' "
                      + " ORDER BY c.LastCallDate ASC ";

            DataTable dt = GE.SELECT_Executeion(strSQL);
            if (dt.Rows.Count > 0)
            {
                RadGridShowList.DataSource = dt;
                RadGridShowList.DataBind();
            }
            else
            {
                RadGridShowList.DataSource = new Object[0];
                RadGridShowList.DataBind();
            }
        }
        catch (Exception ex)
        {
            RadAjaxManager1.Alert(ex.Message);
        }
    }
    protected void RadGridShowList_PageIndexChanged(object sender, GridPageChangedEventArgs e)
    {
        ListAllDataBoxStatus(lblLastStatus.Text.Trim());
    }

    protected void RadGridStatus_SelectedIndexChanged(object sender, EventArgs e)
    {
        //string strSeq = (RadGridShowList.SelectedItems[0] as GridDataItem).GetDataKeyValue("SubStatusID").ToString();
        //ListAllDataBoxStatus(strSeq);      
        //foreach (GridDataItem item in RadGridShowList.SelectedItems)
        //{
        //    //change the code here according to the value you need
        //    var strSeq = item["SubStatusID"].Text;
        //    RadAjaxManager1.Alert(strSeq);
        //    //ListAllDataBoxStatus(strSeq);
        //}
    }
    protected void RadGridStatus_ItemCommand(object sender, GridCommandEventArgs e)
    {
        if (e.Item is GridDataItem)
        {

            if (e.CommandName == "Select")
            {
                GridDataItem item = (GridDataItem)e.Item;
                LinkButton lnkbtn = (LinkButton)item["TempCol"].FindControl("LinkButton1");
                HiddenField hidf = (HiddenField)item["TempCol"].FindControl("HiddenField1");
                lblLastStatus.Text = hidf.Value;
                ListAllDataBoxStatus(hidf.Value);
            }
        }
    }
    protected void RadGridShowList_ItemDataBound(object sender, GridItemEventArgs e)
    {
        if (e.Item is GridPagerItem)
        {
            GridPagerItem pager = (GridPagerItem)e.Item;
            RadComboBox PageSizeComboBox = (RadComboBox)pager.FindControl("PageSizeComboBox");
            PageSizeComboBox.Visible = false;
        } 
    }
    protected void RadGridShowList_ItemCommand(object sender, GridCommandEventArgs e)
    {
        if (e.Item is GridDataItem)
        {

            if (e.CommandName == "Select")
            {
                GridDataItem item = (GridDataItem)e.Item;
                LinkButton lnkbtn = (LinkButton)item["TempCol"].FindControl("LinkButton2");
                HiddenField hidf = (HiddenField)item["TempCol"].FindControl("HiddenField2");
                Response.Redirect("~/FrmData.aspx?cusid=" + hidf.Value);
            }
        }
        ListAllDataBoxStatus(lblLastStatus.Text.Trim());
    }
    protected void RadGridStatusSuccess_ItemCommand(object sender, GridCommandEventArgs e)
    {
        if (e.Item is GridDataItem)
        {

            if (e.CommandName == "Select")
            {
                GridDataItem item = (GridDataItem)e.Item;
                LinkButton lnkbtn = (LinkButton)item["TempCol"].FindControl("LinkButton3");
                HiddenField hidf = (HiddenField)item["TempCol"].FindControl("HiddenField3");
                lblLastStatus.Text = hidf.Value;
                ListAllDataBoxStatus(hidf.Value);
            }
        }
        ListAllDataBoxStatusSuccess(lblLastStatus.Text.Trim());
    }
}