﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

/// <summary>
/// Summary description for GetExecution
/// </summary>
public class GetExecution
{
    Connection Conn = new Connection();
    public DataTable SELECT_Executeion(string ImSQL)
    {
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = null;

        strSQL = ImSQL;

        objConn.ConnectionString = Conn.getConnection();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }

    public void INSERT_Executeion(string SQL)
    {
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = null;

        strSQL = SQL;

        objConn.ConnectionString = Conn.getConnection();
        objConn.Open();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;

        objCmd.ExecuteNonQuery();

        dtAdapter = null;
        objConn.Close();
        objConn = null;

    }

    public void UPDATE_Executeion(string SQL)
    {
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = null;

        strSQL = SQL;

        objConn.ConnectionString = Conn.getConnection();
        objConn.Open();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;

        objCmd.ExecuteNonQuery();

        dtAdapter = null;
        objConn.Close();
        objConn = null;

    }
}