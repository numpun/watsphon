﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Data;

public partial class UserManagement : System.Web.UI.Page
{
    GetExecution GE = new GetExecution();
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                if (Session["UserID"] == null)
                {
                    Response.Redirect("~/Default.aspx");
                }
                lblUserID.Text = Session["UserID"].ToString();
                lblUserLevelID.Text = Session["UserLevelID"].ToString();

                LoadUserList();
                LoadControl();
            }

        }
        catch (Exception ex)
        {
            RadAjaxManager1.Alert(ex.Message);
        }
    }


    public void LoadControl()
    {
        try
        {
            PanelFrom.Visible = false;
        }
        catch (Exception ex)
        {
            RadAjaxManager1.Alert(ex.Message);
        }
    }
    public void LoadUserList()
    {
        try
        {
            string strSQL = " SELECT U.*, U.Fname+' '+U.Lname AS FullName FROM TblUser U ORDER BY CreateDATE DESC ";
            DataTable dt = GE.SELECT_Executeion(strSQL);
            if (dt.Rows.Count > 0)
            {
                RadGrid1.DataSource = dt;
                RadGrid1.DataBind();
            }
            else
            {
                RadGrid1.DataSource = new Object[0];
                RadGrid1.DataBind();
            }
        }
        catch (Exception ex)
        {
            RadAjaxManager1.Alert(ex.Message);
        }
    }
    protected void RadGrid1_ItemCommand(object sender, GridCommandEventArgs e)
    {
        try
        {
            LoadUserList();
        }
        catch (Exception ex)
        {
            RadAjaxManager1.Alert(ex.Message);
        }
    }

    //BtnSaveUser
    #region

    private bool ChkFrom()
    {
        try
        {
            if (txtUsername.Text.Trim() == "")
            {
                RadAjaxManager1.Alert("กรุณากรอกข้อมูลชื่อผู้ใช้");
                txtUsername.Focus();
                return false;
            }

            if (txtPassword.Text.Trim() == "")
            {
                RadAjaxManager1.Alert("กรุณากรอกข้อมูลรหัสผ่าน");
                txtPassword.Focus();
                return false;
            }

            if (txtFname.Text.Trim() == "")
            {
                RadAjaxManager1.Alert("กรุณากรอกข้อมูลชื่อ");
                txtFname.Focus();
                return false;
            }

            if (txtLname.Text.Trim() == "")
            {
                RadAjaxManager1.Alert("กรุณากรอกข้อมูลนามสกุล");
                txtLname.Focus();
                return false;
            }

            if (txtNname.Text.Trim() == "")
            {
                RadAjaxManager1.Alert("กรุณากรอกข้อมูลชื่อเล่น");
                txtNname.Focus();
                return false;
            }

            if (txtTel.Text.Trim() == "")
            {
                RadAjaxManager1.Alert("กรุณากรอกข้อมูลเบอร์โทร");
                txtTel.Focus();
                return false;
            }

            if (rcbUserLevel.SelectedValue == "")
            {
                RadAjaxManager1.Alert("กรุณาเลือกตำแหน่ง");
                rcbUserLevel.Focus();
                return false;
            }
            return true;
        }
        catch (Exception ex)
        {
            RadAjaxManager1.Alert(ex.Message);
            return false;
        }
    }

    private bool SaveUser()
    {
        try
        {
            if (lblToolBarMode.Text.Trim() == "Add")
            {
                string strSQL = " INSERT INTO TblUser (Username,[Password],UserLevelID,Fname,Lname,Nname,Active,Tel,CreateID,CreateDate) "
                        + " VALUES ('" + txtUsername.Text.Trim() + "' "
                        + ", '" + txtPassword.Text.Trim() + "' "
                        + ", '" + rcbUserLevel.SelectedValue + "' "
                        + ", '" + txtFname.Text.Trim() + "' "
                        + ", '" + txtLname.Text.Trim() + "' "
                        + ", '" + txtNname.Text.Trim() + "' "
                        + ", 'Y' "
                        + ", '" + txtTel.Text.Trim() + "' "
                        + ", '" + lblUserID.Text.Trim() + "' "
                        + ", GETDATE()) ";
                GE.INSERT_Executeion(strSQL);
                DataTable dtUserMax = GE.SELECT_Executeion(" SELECT MAX(UserID) AS UserID FROM TblUser ");
                GE.INSERT_Executeion(" INSERT INTO TblUserLog (UserID,LogType,CreateID,CreateDate,Active) VALUES ('" + dtUserMax.Rows[0]["UserID"].ToString() + "','C','" + lblUserID.Text.Trim() + "',GETDATE(),'Y') ");
                RadAjaxManager1.Alert("บันทึกข้อมูลเรียบร้อย");
                return true;
            }
            else if (lblToolBarMode.Text.Trim() == "Edit")
            {
                string strSQL = " UPDATE TblUser SET "
                        + " [Password] = '" + txtPassword.Text.Trim() + "'"
                        + ", Fname = '" + txtFname.Text.Trim() + "' "
                        + ", Lname = '" + txtLname.Text.Trim() + "' "
                        + ", Nname = '" + txtNname.Text.Trim() + "' "
                        + ", Tel = '" + txtTel.Text.Trim() + "' "
                        + ", UserlevelID = '" + rcbUserLevel.SelectedValue + "' "
                        + " WHERE UserID = '" + lblEditID.Text.Trim() + "' ";
                GE.UPDATE_Executeion(strSQL);
                GE.INSERT_Executeion(" INSERT INTO TblUserLog (UserID,LogType,CreateID,CreateDate,Active) VALUES ('" + lblEditID.Text.Trim() + "','E','" + lblUserID.Text.Trim() + "',GETDATE(),'Y') ");
                RadAjaxManager1.Alert("บันทึกข้อมูลเรียบร้อย");
                return true;
            }
            else
            {
                return false;
            }          
        }
        catch (Exception ex)
        {
            RadAjaxManager1.Alert(ex.Message);
            return false;
        }
    }

    public void ClearContorl()
    {
        txtUsername.Text = "";
        txtPassword.Text = "";
        txtFname.Text = "";
        txtLname.Text = "";
        txtNname.Text = "";
        txtTel.Text = "";
        lblEditID.Text = "";
        rcbUserLevel.SelectedIndex = 0;
    }
    protected void BtnSaveUser_Click(object sender, EventArgs e)
    {
        try
        {
            if (ChkFrom())
            {
                if (SaveUser())
                {
                    //ClearContorl();
                    //LoadUserList();
                    //PanelFrom.Visible = false;
                    Page.Response.Redirect(Page.Request.Url.ToString(), true);
                }
            }
        }
        catch (Exception ex)
        {
            RadAjaxManager1.Alert(ex.Message);
        }
    }
    #endregion

    protected void RadToolBar1_ButtonClick(object sender, RadToolBarEventArgs e)
    {
        try
        {
            switch (e.Item.Value)
            {
                case "Add":
                    PanelFrom.Visible = true;
                    lblToolBarMode.Text = "Add";
                    break;
                case "Edit":               
                    ChkEdit();
                    LoadUserEdit();
                    LoadHisEdit();
                    LoadHisLogin();
                    break;
            }
        }
        catch (Exception ex)
        {
            RadAjaxManager1.Alert(ex.Message);
        }
    }
    public void ChkEdit()
    {
        try
        {
            string sPID = "";
            foreach (GridDataItem data in RadGrid1.MasterTableView.GetSelectedItems())
            {
                string seq = data.OwnerTableView.DataKeyValues[data.ItemIndex]["UserID"].ToString();
                sPID = seq;
            }

            if (sPID.Trim() == "")
            {
                RadAjaxManager1.Alert("กรุณาเลือกรายชื่อพนักงานที่ต้องการแก้ไข");
                return;
            }
            PanelFrom.Visible = true;
            PanelList.Visible = false;
            lblToolBarMode.Text = "Edit";
            lblEditID.Text = sPID.Trim();
            txtUsername.Enabled = false;
        }
        catch (Exception ex)
        {
            RadAjaxManager1.Alert(ex.Message);
        }
    }
    public void LoadUserEdit()
    {
        try
        {
            string strSQL = " SELECT * FROM TblUser WHERE UserID = " + lblEditID.Text.Trim() + " ";
            DataTable dt = GE.SELECT_Executeion(strSQL);
            if (dt.Rows.Count > 0)
            {
                lblEditID.Text = dt.Rows[0]["UserID"].ToString();
                txtUsername.Text = dt.Rows[0]["Username"].ToString();
                txtPassword.Text = dt.Rows[0]["Password"].ToString();
                txtFname.Text = dt.Rows[0]["Fname"].ToString();
                txtLname.Text = dt.Rows[0]["Lname"].ToString();
                txtNname.Text = dt.Rows[0]["Nname"].ToString();
                txtTel.Text = dt.Rows[0]["Tel"].ToString();
                rcbUserLevel.SelectedValue = dt.Rows[0]["UserLevelID"].ToString();

            }
        }
        catch (Exception ex)
        {
            RadAjaxManager1.Alert(ex.Message);
        }
    }
    public void LoadHisLogin()
    {
        try
        {
            string strSQL = " SELECT UL.*, CASE UL.LogType WHEN 'C' THEN 'Create' ELSE 'Login' END AS LogDetail "
                    + " , (SELECT U1.Fname+' '+U1.Lname FROM TblUser U1 WHERE U1.UserID = 1) AS UpFullName "
                    + " FROM TblUserLog UL "
                    + " WHERE Active = 'Y' AND LogType IN('L') ORDER BY CreateDate DESC ";
            DataTable dt = GE.SELECT_Executeion(strSQL);
            if (dt.Rows.Count > 0)
            {
                RadGrid3.DataSource = dt;
                RadGrid3.DataBind();
            }
            else
            {
                RadGrid3.DataSource = new Object[0];
                RadGrid3.DataBind();
            }
        }
        catch (Exception ex)
        {
            RadAjaxManager1.Alert(ex.Message);
        }
    }

    public void LoadHisEdit()
    {
        try
        {
            string strSQL = " SELECT UL.*, CASE UL.LogType WHEN 'C' THEN 'Create' ELSE 'Edit' END AS LogDetail "
                    + " , (SELECT U1.Fname+' '+U1.Lname FROM TblUser U1 WHERE U1.UserID = 1) AS UpFullName "
                    + " FROM TblUserLog UL "
                    + " WHERE Active = 'Y' AND LogType IN('C','E') ORDER BY CreateDate DESC ";
            DataTable dt = GE.SELECT_Executeion(strSQL);
            if (dt.Rows.Count > 0)
            {
                RadGrid2.DataSource = dt;
                RadGrid2.DataBind();
            }
            else
            {
                RadGrid2.DataSource = new Object[0];
                RadGrid2.DataBind();
            }
        }
        catch (Exception ex)
        {
            RadAjaxManager1.Alert(ex.Message);
        }
    }
    protected void BtnBack_Click(object sender, EventArgs e)
    {
        Page.Response.Redirect(Page.Request.Url.ToString(), true);
    }
}