﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageInfo.master" AutoEventWireup="true" CodeFile="FrmData.aspx.cs" Inherits="FrmData" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        td {
            padding: 10px;
        }

        span {
            color: black;
            padding: 5px;
        }

        input[type="text"] {
            width: 80%;
            padding: 5px;
            border: 1px solid #ffa517;
        }

        tr {
            padding: 5px;
            border-bottom: solid 1px #000;
            border-top: solid 1px #000;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Label ID="lblUserLavel" runat="server" Text="" Visible="False"></asp:Label>
    <asp:Label ID="lblUserID" runat="server" Text="" Visible="False"></asp:Label>
    <asp:Label ID="lblCusID" runat="server" Text="" Visible="False"></asp:Label>
    <asp:Label ID="lblCntCall" runat="server" Text="" Visible="False"></asp:Label>
    <asp:Panel ID="PanelHead" runat="server">
        <div class="boxleft">
            <table border="0">
                <tr style="text-align: center">
                    <td style="width: 50%;" colspan="2">
                        <telerik:RadGrid
                            ID="RadGridCallOn"
                            runat="server"
                            Width="90%"
                            Height="50px"
                            GridLines="None"
                            Style="outline: none"
                            BorderWidth="1"
                            AllowPaging="false"
                            AllowSorting="true">
                            <PagerStyle Mode="NextPrevAndNumeric" />
                            <HeaderStyle CssClass="Appointmentheader" />
                            <MasterTableView
                                AutoGenerateColumns="False"
                                DataKeyNames="CusID"
                                ClientDataKeyNames="CusID"
                                CommandItemDisplay="none"
                                EnableHeaderContextMenu="true"
                                AllowPaging="True"
                                PageSize="20">
                                <Columns>
                                    <telerik:GridBoundColumn
                                        HeaderText="home_phone"
                                        DataField="SHomePhone">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" Wrap="False" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn
                                        HeaderText="mobile"
                                        DataField="SMobile">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" Wrap="False" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn
                                        HeaderText="office_phone"
                                        DataField="Sofficephone">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" Wrap="False" />
                                    </telerik:GridBoundColumn>
                                </Columns>
                            </MasterTableView>
                            <ClientSettings
                                AllowColumnsReorder="true"
                                AllowColumnHide="True"
                                EnableRowHoverStyle="True"
                                ReorderColumnsOnClient="true">
                                <Selecting AllowRowSelect="True" />
                                <Resizing AllowColumnResize="true" />
                                <Scrolling AllowScroll="True" UseStaticHeaders="false" SaveScrollPosition="True"></Scrolling>
                            </ClientSettings>
                        </telerik:RadGrid>
                    </td>
                </tr>
            </table>
            <asp:Panel ID="talkscript" runat="server" Visible="false">
                <table border="0">
                    <tr>
                        <td colspan="2">
                            <asp:Label ID="lbltalkscript" runat="server" Text="Label"></asp:Label>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </div>


        <div class="boxright">
            <asp:Panel ID="callstatus" runat="server" Visible="false">
                <table width="90%">
                    <tr style="text-align: center">
                        <td style="width: 50%; background: #e2e2e2;">
                            <p>Status </p>
                        </td>
                        <td style="background: #e2e2e2;">
                            <p>Sub Status </p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <telerik:RadComboBox
                                ID="rcbStatus"
                                runat="server"
                                LoadingMessage="กำลังโหลดข้อมูล ..."
                                EmptyMessage="กรุณาเลือกสถานะ ..."
                                MarkFirstMatch="true"
                                Width="245px"
                                DropDownWidth="245px"
                                HighlightTemplatedItems="true"
                                OnSelectedIndexChanged="rcbStatus_SelectedIndexChanged"
                                AutoPostBack="True">
                            </telerik:RadComboBox>

                        </td>
                        <td>
                            <telerik:RadComboBox
                                ID="rcbSubStatus"
                                runat="server"
                                LoadingMessage="กำลังโหลดข้อมูล ..."
                                EmptyMessage="กรุณาเลือกสถานะ ..."
                                MarkFirstMatch="true"
                                Width="245px"
                                DropDownWidth="245px"
                                HighlightTemplatedItems="true"
                                AutoPostBack="True">
                            </telerik:RadComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="background: #e2e2e2; color: black;" colspan="2">
                            <p>หมายเหตุ</p>
                            <asp:TextBox ID="txacomments" runat="server" TextMode="MultiLine" Rows="5" Columns="70"></asp:TextBox>
                            <br />
                            <asp:Button ID="BtnSaveCallStatus" CssClass="btn blue" runat="server" Text="บันทึกสถานะ" OnClick="BtnSaveCallStatus_Click" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <asp:Panel ID="PanelQC" runat="server" Visible="false">
                <p>ผลQC ?</p>
                <asp:RadioButtonList
                    ID="rdoQCAppStatus"
                    runat="server"
                    RepeatDirection="Horizontal">
                    <asp:ListItem Value="3">ผ่าน</asp:ListItem>
                    <asp:ListItem Value="4">ไม่ผ่าน</asp:ListItem>
                </asp:RadioButtonList>
                <asp:Button ID="BtnSaveQC" CssClass="btn yellow" runat="server" Text="บันทึกผล QC" />
            </asp:Panel>
        </div>



        <br clear="All" />
        <asp:Button ID="BtnSave" CssClass="btn green" runat="server" Text="บันทึกหน้าใบงาน" OnClick="BtnSave_Click" />
    </asp:Panel>
    <br clear="All" />
    <asp:Panel ID="PanelContent" runat="server">
        <div id="datacontent" style="height: 500px; overflow: scroll;">
            <div id="masterdata" style="width: 100%; border: 0px solid #000; float: left;">
                <table border="0">
                    <tr>
                        <td style="background: #e2e2e2;" colspan="2">
                            <p>Master Data</p>
                        </td>

                        <td style="background: #e2e2e2;" colspan="2">
                            <p>Update Data</p>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 20%;">
                            <span>VIN</span>
                        </td>
                        <td style="width: 25%;">
                            <asp:Label ID="Label48" runat="server" Text="Label"></asp:Label>
                        </td>
                        <td style="width: 25%;">
                            <span style="background: #ffa517;">VIN</span>
                        </td>
                        <td style="width: 35%;">
                            <asp:TextBox ID="TextBox48" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span>License</span>
                        </td>
                        <td>
                            <asp:Label ID="Label49" runat="server" Text="Label"></asp:Label>
                        </td>
                        <td>
                            <span style="background: #ffa517;">License</span>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBox49" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span>Car Model</span>
                        </td>
                        <td>
                            <asp:Label ID="Label51" runat="server" Text="Label"></asp:Label>
                        </td>
                        <td>
                            <span style="background: #ffa517;">Car Model</span>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBox51" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span>Retail Date</span>
                        </td>
                        <td>
                            <asp:Label ID="Label53" runat="server" Text="Label"></asp:Label>
                        </td>
                        <td>
                            <span style="background: #ffa517;">Retail Date</span>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBox53" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span>Last Service date</span>
                        </td>
                        <td>
                            <asp:Label ID="Label56" runat="server" Text="Label"></asp:Label>
                        </td>
                        <td>
                            <span style="background: #ffa517;">Last Service date</span>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBox56" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr style="display: none;">
                        <td style="width: 20%;">
                            <span>Customer Code</span>
                        </td>
                        <td style="width: 25%;">
                            <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>
                        </td>
                        <td style="width: 20%;">
                            <span style="background: #f10000;">Customer Code</span>
                        </td>
                        <td style="width: 35%;">
                            <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr style="display: none;">
                        <td>
                            <span>Car ID</span>
                        </td>
                        <td>
                            <asp:Label ID="Label2" runat="server" Text="Label"></asp:Label>
                        </td>
                        <td>
                            <span style="background: #f10000;">Car ID</span>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr style="display: none;">
                        <td>
                            <span>CDB ID</span>
                        </td>
                        <td>
                            <asp:Label ID="Label3" runat="server" Text="Label"></asp:Label>
                        </td>
                        <td>
                            <span style="background: #f10000;">CDB ID</span>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span>Customer Tupe</span>
                        </td>
                        <td>
                            <asp:Label ID="Label4" runat="server" Text="Label"></asp:Label>
                        </td>
                        <td>
                            <span style="background: #6fbb56;">Customer Tupe</span>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBox4" runat="server"></asp:TextBox>
                            <telerik:RadComboBox
                                runat="server"
                                ID="rcb4"
                                EmptyMessage="...Select change tupe..."
                                CausesValidation="false"
                                AutoPostBack="true"
                                Width="83%" OnSelectedIndexChanged="rcb4_SelectedIndexChanged">
                                <Items>
                                    <telerik:RadComboBoxItem runat="server" Text="I" Value="I" />
                                    <telerik:RadComboBoxItem runat="server" Text="F" Value="F" />
                                    <telerik:RadComboBoxItem runat="server" Text="C" Value="C" />
                                </Items>
                            </telerik:RadComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span>Customer Title</span>
                        </td>
                        <td>
                            <asp:Label ID="Label5" runat="server" Text="Label"></asp:Label>
                        </td>
                        <td>
                            <span style="background: #6fbb56;">Customer Title</span>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBox5" runat="server"></asp:TextBox>
                            <telerik:RadComboBox
                                ID="rcb5"
                                Width="83%"
                                runat="server"
                                AutoPostBack="true" OnSelectedIndexChanged="rcb5_SelectedIndexChanged">
                            </telerik:RadComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span>Customer Name</span>
                        </td>
                        <td>
                            <asp:Label ID="Label6" runat="server" Text="Label"></asp:Label>
                        </td>
                        <td>
                            <span style="background: #6fbb56;">Customer Name</span>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBox6" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr id="groupnoneI_7" runat="server">
                        <td>
                            <span>Customer Br. No.</span>
                        </td>
                        <td>
                            <asp:Label ID="Label7" runat="server" Text="Label"></asp:Label>
                        </td>
                        <td>
                            <span style="background: #ffa517;">Customer Br. No.</span>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBox7" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr style="display: none;">
                        <td>
                            <span>Customer Address 1</span>
                        </td>
                        <td>
                            <asp:Label ID="Label8" runat="server" Text="Label"></asp:Label>
                        </td>
                        <td>
                            <span style="background: #6fbb56;">Customer Address 1</span>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBox8" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr style="display: none;">
                        <td>
                            <span style="vertical-align: top;">Customer Address 2</span>
                        </td>
                        <td>
                            <asp:Label ID="Label9" runat="server" Text="Label"></asp:Label>
                        </td>
                        <td>
                            <span style="background: #6fbb56;">Customer Address 2</span>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBox9" runat="server"></asp:TextBox>
                        </td>
                    </tr>

                    <tr>
                        <td><span>Customer HomeNumber</span></td>
                        <td>
                            <asp:Label ID="lblCustoemrAddress1_1" runat="server" Text="Label"></asp:Label></td>
                        <td>
                            <span style="background: #6fbb56;">บ้านเลขที่</span>
                        </td>
                        <td>
                            <asp:TextBox ID="txtCustoemrAddress1_1" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td><span>Customer Village</span></td>
                        <td> <asp:Label ID="lblCustoemrAddress1_2" runat="server" Text="Label"></asp:Label></td>
                        <td>
                            <span style="background: #6fbb56;">อาคาร / หมู่บ้าน</span>
                        </td>
                        <td>
                            <asp:TextBox ID="txtCustoemrAddress1_2" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td><span>Customer Moo</span></td>
                        <td> <asp:Label ID="lblCustoemrAddress1_3" runat="server" Text="Label"></asp:Label></td>
                        <td>
                            <span style="background: #6fbb56;">หมู่</span>
                        </td>
                        <td>
                            <asp:TextBox ID="txtCustoemrAddress1_3" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td><span>Customer Soi</span></td>
                        <td> <asp:Label ID="lblCustoemrAddress1_4" runat="server" Text="Label"></asp:Label></td>
                        <td>
                            <span style="background: #6fbb56;">ซอย</span>
                        </td>
                        <td>
                            <asp:TextBox ID="txtCustoemrAddress1_4" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td><span>Customer Road</span></td>
                        <td> <asp:Label ID="lblCustoemrAddress1_5" runat="server" Text="Label"></asp:Label></td>
                        <td>
                            <span style="background: #6fbb56;">ถนน</span>
                        </td>
                        <td>
                            <asp:TextBox ID="txtCustoemrAddress1_5" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                         <td><span>Customer Province</span></td>
                        <td> <asp:Label ID="lblProvince10" runat="server" Text="Label"></asp:Label></td>
                        <td>
                            <span style="background: #6fbb56;">Customer จังหวัด</span>
                        </td>
                        <td>
                            <telerik:RadComboBox
                                ID="rcbProvince10"
                                runat="server"
                                Width="83%"
                                Height="200px"
                                Filter="Contains"
                                AutoPostBack="true"
                                EmptyMessage="-- เลือกจังหวัด --"
                                OnSelectedIndexChanged="rcbProvince10_SelectedIndexChanged">
                            </telerik:RadComboBox>
                        </td>
                    </tr>
                    <tr style="display: none;">
                        <td>
                            <span style="vertical-align: top;">Customer District</span>
                        </td>
                        <td>
                            <asp:Label ID="Label10" runat="server" Text="Label"></asp:Label>
                        </td>
                        <td>
                            <span style="background: #6fbb56;">Customer District</span>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBox10" runat="server" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                         <td><span>Customer District</span></td>
                        <td> <asp:Label ID="lblDistrict10" runat="server" Text="Label"></asp:Label></td>
                        <td><span style="background: #6fbb56;">Customer เขต / อำเภอ</span></td>
                        <td>
                            <telerik:RadComboBox
                                ID="rcbDistrict10"
                                runat="server"
                                Width="83%"
                                Filter="Contains"
                                AutoPostBack="true"
                                EmptyMessage="-- เลือกเขต/อำเภอ --"
                                OnSelectedIndexChanged="rcbDistrict10_SelectedIndexChanged">
                            </telerik:RadComboBox>
                        </td>
                    </tr>
                    <tr>
                         <td><span>Customer SubDrict</span></td>
                        <td> <asp:Label ID="lblSubDistrict10" runat="server" Text="Label"></asp:Label></td>
                        <td><span style="background: #6fbb56;">Customer แขวง / ตำบล</span></td>
                        <td>
                            <telerik:RadComboBox
                                ID="rcbSubDistrict10"
                                runat="server"
                                Width="83%"
                                Height="200px"
                                Filter="Contains"
                                AutoPostBack="true"
                                EmptyMessage="-- เลือกแขวง/ตำบล --"
                                OnSelectedIndexChanged="rcbSubDistrict10_SelectedIndexChanged">
                            </telerik:RadComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span>Customer ZipCode</span>
                        </td>
                        <td>
                            <asp:Label ID="Label11" runat="server" Text="Label"></asp:Label>
                        </td>
                        <td>
                            <span style="background: #6fbb56;">Customer ZipCode</span>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBox11" runat="server" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>
                    <tr id="groupnoneI_12" runat="server" >
                        <td>
                            <span>Customer Tax ID.</span>
                        </td>
                        <td>
                            <asp:Label ID="Label12" runat="server" Text="Label"></asp:Label>
                        </td>
                        <td>
                            <span style="background: #ffa517;">Customer Tax ID.</span>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBox12" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span>Completed account? (Y/N)</span>
                        </td>
                        <td>
                            <asp:Label ID="Label13" runat="server" Text="Label"></asp:Label>
                        </td>
                        <td>
                            <span style="background: #6fbb56;">Completed account? (Y/N)</span>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBox13" runat="server"></asp:TextBox>
                            <telerik:RadComboBox
                                runat="server"
                                ID="rcb13"
                                EmptyMessage="...Select Completed account..."
                                CausesValidation="false"
                                AutoPostBack="true"
                                Width="83%"
                                OnSelectedIndexChanged="rcb13_SelectedIndexChanged">
                                <Items>
                                    <telerik:RadComboBoxItem runat="server" Text="Y" Value="Y" />
                                    <telerik:RadComboBoxItem runat="server" Text="N" Value="N" />
                                </Items>
                            </telerik:RadComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span>Customer Home Phone</span>
                        </td>
                        <td>
                            <asp:Label ID="Label15" runat="server" Text="Label"></asp:Label>
                        </td>
                        <td>
                            <span style="background: #6fbb56;">Customer Home Phone</span>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBox15" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span>Customer Office Phone</span>
                        </td>
                        <td>
                            <asp:Label ID="Label16" runat="server" Text="Label"></asp:Label>
                        </td>
                        <td>
                            <span style="background: #6fbb56;">Customer Office Phone</span>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBox16" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span>Customer Mobile</span>
                        </td>
                        <td>
                            <asp:Label ID="Label17" runat="server" Text="Label"></asp:Label>
                        </td>
                        <td>
                            <span style="background: #6fbb56;">Customer Mobile</span>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBox17" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span>Customer Fax</span>
                        </td>
                        <td>
                            <asp:Label ID="Label18" runat="server" Text="Label"></asp:Label>
                        </td>
                        <td>
                            <span style="background: #6fbb56;">Customer Fax</span>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBox18" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span>Customer e-Mail address</span>
                        </td>
                        <td>
                            <asp:Label ID="Label19" runat="server" Text="Label"></asp:Label>
                        </td>
                        <td>
                            <span style="background: #6fbb56;">Customer e-Mail address</span>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBox19" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span>Customer Gender</span>
                        </td>
                        <td>
                            <asp:Label ID="Label20" runat="server" Text="Label"></asp:Label>
                        </td>
                        <td>
                            <span style="background: #6fbb56;">Customer Gender</span>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBox20" runat="server"></asp:TextBox>
                            <telerik:RadComboBox
                                runat="server"
                                ID="rcb20"
                                EmptyMessage="...Select customer gender..."
                                CausesValidation="false"
                                AutoPostBack="true"
                                Width="83%"
                                OnSelectedIndexChanged="rcb20_SelectedIndexChanged">
                                <Items>
                                    <telerik:RadComboBoxItem runat="server" Text="M" Value="M" />
                                    <telerik:RadComboBoxItem runat="server" Text="F" Value="F" />
                                    <telerik:RadComboBoxItem runat="server" Text="C (Corporate)" Value="C" />
                                </Items>
                            </telerik:RadComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span>Customer Marital Status</span>
                        </td>
                        <td>
                            <asp:Label ID="Label21" runat="server" Text="Label"></asp:Label>
                        </td>
                        <td>
                            <span style="background: #ffa517;">Customer Marital Status</span>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBox21" runat="server"></asp:TextBox>
                             <telerik:RadComboBox
                                runat="server"
                                ID="rcb21"
                                EmptyMessage="...Select customer marital status..."
                                CausesValidation="false"
                                AutoPostBack="true"
                                Width="83%" 
                                OnSelectedIndexChanged="rcb21_SelectedIndexChanged">
                                <Items>
                                    <telerik:RadComboBoxItem runat="server" Text="S" Value="S" />
                                    <telerik:RadComboBoxItem runat="server" Text="M" Value="M" />
                                    <telerik:RadComboBoxItem runat="server" Text="N" Value="N" />
                                </Items>
                            </telerik:RadComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span>Number of children</span>
                        </td>
                        <td>
                            <asp:Label ID="Label22" runat="server" Text="Label"></asp:Label>
                        </td>
                        <td>
                            <span style="background: #ffa517;">Number of children</span>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBox22" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span>Customer Birthdate (YYYYMM)</span>
                        </td>
                        <td>
                            <asp:Label ID="Label23" runat="server" Text="Label"></asp:Label>
                        </td>
                        <td>
                            <span style="background: #ffa517;">Customer Birthdate (YYYYMM)</span>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBox23" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span>Occupation</span>
                        </td>
                        <td>
                            <asp:Label ID="Label24" runat="server" Text="Label"></asp:Label>
                        </td>
                        <td>
                            <span style="background: #6fbb56;">Occupation</span>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBox24" runat="server"></asp:TextBox>
                            <telerik:RadComboBox
                                runat="server"
                                ID="rcb24"
                                EmptyMessage="...Select Occupation..."
                                CausesValidation="false"
                                AutoPostBack="true"
                                Width="83%"
                                OnSelectedIndexChanged="rcb24_SelectedIndexChanged">
                                <Items>
                                    <telerik:RadComboBoxItem runat="server" Text="พนง.บริษัทเอกชน" Value="พนง.บริษัทเอกชน" />
                                    <telerik:RadComboBoxItem runat="server" Text="ข้าราชการ" Value="ข้าราชการ" />
                                    <telerik:RadComboBoxItem runat="server" Text="พนง.รัฐวิสาหกิจ" Value="พนง.รัฐวิสาหกิจ" />
                                    <telerik:RadComboBoxItem runat="server" Text="แพทย์" Value="แพทย์" />
                                    <telerik:RadComboBoxItem runat="server" Text="ครู/อาจารย์" Value="ครู/อาจารย์" />
                                    <telerik:RadComboBoxItem runat="server" Text="นักศึกษา" Value="นักศึกษา" />
                                    <telerik:RadComboBoxItem runat="server" Text="นักบิน" Value="นักบิน" />
                                    <telerik:RadComboBoxItem runat="server" Text="พนักงานต้อนรับบนเครื่องบิน" Value="พนักงานต้อนรับบนเครื่องบิน" />
                                    <telerik:RadComboBoxItem runat="server" Text="แม่บ้าน" Value="แม่บ้าน" />
                                    <telerik:RadComboBoxItem runat="server" Text="ดารา/นักแสดง" Value="ดารา/นักแสดง" />
                                    <telerik:RadComboBoxItem runat="server" Text="เจ้าของธุรกิจ" Value="เจ้าของธุรกิจ" />
                                    <telerik:RadComboBoxItem runat="server" Text="เทคโนโลยี/ไอที" Value="เทคโนโลยี/ไอที" />
                                    <telerik:RadComboBoxItem runat="server" Text="อัยการ/ทนายความ" Value="อัยการ/ทนายความ" />
                                    <telerik:RadComboBoxItem runat="server" Text="เกษียณ" Value="เกษียณ" />
                                    <telerik:RadComboBoxItem runat="server" Text="อาชีพอิสระ/freeland" Value="อาชีพอิสระ/freeland" />
                                    <telerik:RadComboBoxItem runat="server" Text="พนักงานขาย" Value="พนักงานขาย" />
                                    <telerik:RadComboBoxItem runat="server" Text="ตำรวจ" Value="ตำรวจ" />
                                    <telerik:RadComboBoxItem runat="server" Text="อื่นๆ --> ระบุ" Value="อื่นๆ" />
                                </Items>
                            </telerik:RadComboBox>
                             <asp:TextBox ID="txt_rcb24_other" runat="server" Visible="false"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span>Hobby</span>
                        </td>
                        <td>
                            <asp:Label ID="Label25" runat="server" Text="Label"></asp:Label>
                        </td>
                        <td>
                            <span style="background: #6fbb56;">Hobby</span>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBox25" runat="server"></asp:TextBox>
                            <telerik:RadComboBox
                                runat="server"
                                ID="rcb25"
                                EmptyMessage="...Select Hobby..."
                                CausesValidation="false"
                                AutoPostBack="true"
                                Width="83%"
                                OnSelectedIndexChanged="rcb25_SelectedIndexChanged">
                                <Items>
                                    <telerik:RadComboBoxItem runat="server" Text="กอล์ฟ" Value="กอล์ฟ" />
                                    <telerik:RadComboBoxItem runat="server" Text="เทนนิส" Value="เทนนิส" />
                                    <telerik:RadComboBoxItem runat="server" Text="ว่ายน้ำ" Value="ว่ายน้ำ" />
                                    <telerik:RadComboBoxItem runat="server" Text="แพทย์" Value="แพทย์" />
                                    <telerik:RadComboBoxItem runat="server" Text="ดนตรี" Value="ดนตรี" />
                                    <telerik:RadComboBoxItem runat="server" Text="นักศึกษา" Value="นักศึกษา" />
                                    <telerik:RadComboBoxItem runat="server" Text="ช๊อปปิ้ง" Value="ช๊อปปิ้ง" />
                                    <telerik:RadComboBoxItem runat="server" Text="ท่องเที่ยว" Value="ท่องเที่ยว" />
                                    <telerik:RadComboBoxItem runat="server" Text="ดูภาพยนตร์" Value="ดูภาพยนตร์" />
                                    <telerik:RadComboBoxItem runat="server" Text="อ่านหนังสือ" Value="อ่านหนังสือ" />
                                    <telerik:RadComboBoxItem runat="server" Text="ปลูกต้นไม้" Value="ปลูกต้นไม้" />
                                    <telerik:RadComboBoxItem runat="server" Text="เจ็ตสกี" Value="เจ็ตสกี" />
                                    <telerik:RadComboBoxItem runat="server" Text="ฟิตเนส" Value="ฟิตเนส" />
                                    <telerik:RadComboBoxItem runat="server" Text="สควอช" Value="สควอช" />
                                    <telerik:RadComboBoxItem runat="server" Text="อื่นๆ --> ระบุ" Value="อื่นๆ" />
                                </Items>
                            </telerik:RadComboBox>
                            <asp:TextBox ID="txt_rcb25_other" runat="server" Visible="false"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span>Income /month</span>
                        </td>
                        <td>
                            <asp:Label ID="Label26" runat="server" Text="Label"></asp:Label>
                        </td>
                        <td>
                            <span style="background: #6fbb56;">Income /month</span>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBox26" runat="server"></asp:TextBox>
                            <telerik:RadComboBox
                                runat="server"
                                ID="rcb26"
                                EmptyMessage="...Select Income Sequence..."
                                CausesValidation="false"
                                AutoPostBack="true"
                                Width="83%" 
                                OnSelectedIndexChanged="rcb26_SelectedIndexChanged">
                                <Items>
                                    <telerik:RadComboBoxItem runat="server" Text="<80000" Value="<80000" />
                                    <telerik:RadComboBoxItem runat="server" Text="80000-100000" Value="80000-100000" />
                                    <telerik:RadComboBoxItem runat="server" Text="100000-150000" Value="100000-150000" />
                                    <telerik:RadComboBoxItem runat="server" Text="150000-200000" Value="150000-200000" />
                                    <telerik:RadComboBoxItem runat="server" Text=">200,000" Value=">200,000" />
                                    <telerik:RadComboBoxItem runat="server" Text="ไม่ระบุ" Value="ไม่ระบุ" />
                                </Items>
                            </telerik:RadComboBox>
                        </td>
                    </tr>
                    <tr style="display: none;">
                        <td>
                            <span>Customer preferred language</span>
                        </td>
                        <td>
                            <asp:Label ID="Label27" runat="server" Text="Label"></asp:Label>
                        </td>
                        <td>
                            <span style="background: #f10000;">Customer preferred language</span>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBox27" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span>Contact Type</span>
                        </td>
                        <td>
                            <asp:Label ID="Label28" runat="server" Text="Label"></asp:Label>
                        </td>
                        <td>
                            <span style="background: #6fbb56;">Contact Type</span>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBox28" runat="server"></asp:TextBox>
                            <p>1=Owner / 2=User / 3=Buye</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span>Prefer Contact channel</span>
                        </td>
                        <td>
                            <asp:Label ID="Label29" runat="server" Text="Label"></asp:Label>
                        </td>
                        <td>
                            <span style="background: #6fbb56;">Prefer Contact channel</span>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBox29" runat="server"></asp:TextBox>
                            <p>1=All / 2=Call / 3=Mail / 4=None</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span>Contact Title</span>
                        </td>
                        <td>
                            <asp:Label ID="Label30" runat="server" Text="Label"></asp:Label>
                        </td>
                        <td>
                            <span style="background: #6fbb56;">Contact Title</span>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBox30" runat="server"></asp:TextBox>
                            <telerik:RadComboBox
                                ID="rcb30"
                                Width="83%"
                                EmptyMessage="...Select contact title..."
                                runat="server"
                                AutoPostBack="true"
                                OnSelectedIndexChanged="rcb30_SelectedIndexChanged">
                            </telerik:RadComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span>Contact name</span>
                        </td>
                        <td>
                            <asp:Label ID="Label31" runat="server" Text="Label"></asp:Label>
                        </td>
                        <td>
                            <span style="background: #6fbb56;">Contact name</span>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBox31" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr style="display: none;">
                        <td>
                            <span>Contact Address 1</span>
                        </td>
                        <td>
                            <asp:Label ID="Label32" runat="server" Text="Label"></asp:Label>
                        </td>
                        <td>
                            <span style="background: #6fbb56;">Contact Address 1</span>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBox32" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr style="display: none;">
                        <td>
                            <span>Contact Address 2</span>
                        </td>
                        <td>
                            <asp:Label ID="Label33" runat="server" Text="Label"></asp:Label>
                        </td>
                        <td>
                            <span style="background: #6fbb56;">Contact Address 2</span>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBox33" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td>
                            <span style="background: #6fbb56;">Contact บ้านเลขที่</span>
                        </td>
                        <td>
                            <asp:TextBox ID="txtAddressCC1_1" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td>
                            <span style="background: #6fbb56;">Contact อาคาร / หมู่บ้าน</span>
                        </td>
                        <td>
                            <asp:TextBox ID="txtAddressCC1_2" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td>
                            <span style="background: #6fbb56;">Contact หมู่</span>
                        </td>
                        <td>
                            <asp:TextBox ID="txtAddressCC1_3" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td>
                            <span style="background: #6fbb56;">Contact ซอย</span>
                        </td>
                        <td>
                            <asp:TextBox ID="txtAddressCC1_4" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td>
                            <span style="background: #6fbb56;">Contact ถนน</span>
                        </td>
                        <td>
                            <asp:TextBox ID="txtAddressCC1_5" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td>
                            <span style="background: #6fbb56;">Contact จังหวัด</span>
                        </td>
                        <td>
                            <telerik:RadComboBox
                                ID="rcbProvince33"
                                runat="server"
                                Width="83%"
                                Height="200px"
                                Filter="Contains"
                                AutoPostBack="true"
                                EmptyMessage="-- เลือกจังหวัด --"
                                OnSelectedIndexChanged="rcbProvince33_SelectedIndexChanged">
                            </telerik:RadComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td><span style="background: #6fbb56;">Contact เขต / อำเภอ</span></td>
                        <td>
                            <telerik:RadComboBox
                                ID="rcbDistrict33"
                                runat="server"
                                Width="83%"
                                Filter="Contains"
                                AutoPostBack="true"
                                EmptyMessage="-- เลือกเขต/อำเภอ --"
                                OnSelectedIndexChanged="rcbDistrict33_SelectedIndexChanged">
                            </telerik:RadComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td><span style="background: #6fbb56;">Customer แขวง / ตำบล</span></td>
                        <td>
                            <telerik:RadComboBox
                                ID="rcbSubDistrict33"
                                runat="server"
                                Width="83%"
                                Height="200px"
                                Filter="Contains"
                                AutoPostBack="true"
                                EmptyMessage="-- เลือกแขวง/ตำบล --"
                                OnSelectedIndexChanged="rcbSubDistrict33_SelectedIndexChanged">
                            </telerik:RadComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span>Contact Zip Code</span>
                        </td>
                        <td>
                            <asp:Label ID="Label34" runat="server" Text="Label"></asp:Label>
                        </td>
                        <td>
                            <span style="background: #6fbb56;">Contact Zip Code</span>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBox34" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span>Contact</span>
                        </td>
                        <td>
                            <asp:Label ID="Label35" runat="server" Text="Label"></asp:Label>
                        </td>
                        <td>
                            <span style="background: #6fbb56;">Contact</span>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBox35" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span>Contact Office phone</span>
                        </td>
                        <td>
                            <asp:Label ID="Label36" runat="server" Text="Label"></asp:Label>
                        </td>
                        <td>
                            <span style="background: #6fbb56;">Contact Office phone</span>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBox36" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span>Contact Mobile</span>
                        </td>
                        <td>
                            <asp:Label ID="Label37" runat="server" Text="Label"></asp:Label>
                        </td>
                        <td>
                            <span style="background: #6fbb56;">Contact Mobile</span>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBox37" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span>Conatct Fax</span>
                        </td>
                        <td>
                            <asp:Label ID="Label38" runat="server" Text="Label"></asp:Label>
                        </td>
                        <td>
                            <span style="background: #6fbb56;">Conatct Fax</span>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBox38" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span>Contact eMail Address</span>
                        </td>
                        <td>
                            <asp:Label ID="Label39" runat="server" Text="Label"></asp:Label>
                        </td>
                        <td>
                            <span style="background: #6fbb56;">Contact eMail Address</span>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBox39" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span>Contact Gender</span>
                        </td>
                        <td>
                            <asp:Label ID="Label40" runat="server" Text="Label"></asp:Label>
                        </td>
                        <td>
                            <span style="background: #6fbb56;">Contact Gender</span>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBox40" runat="server"></asp:TextBox>
                            <telerik:RadComboBox
                                runat="server"
                                ID="rcb40"
                                EmptyMessage="...Select contact gender..."
                                CausesValidation="false"
                                AutoPostBack="true"
                                Width="83%"
                                OnSelectedIndexChanged="rcb40_SelectedIndexChanged">
                                <Items>
                                    <telerik:RadComboBoxItem runat="server" Text="M" Value="M" />
                                    <telerik:RadComboBoxItem runat="server" Text="F" Value="F" />
                                    <telerik:RadComboBoxItem runat="server" Text="C (Corporate)" Value="C" />
                                </Items>
                            </telerik:RadComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span>Contact Marital Status</span>
                        </td>
                        <td>
                            <asp:Label ID="Label41" runat="server" Text="Label"></asp:Label>
                        </td>
                        <td>
                            <span style="background: #6fbb56;">Contact Marital Status</span>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBox41" runat="server"></asp:TextBox>
                             <telerik:RadComboBox
                                runat="server"
                                ID="rcb41"
                                EmptyMessage="...Select contact marital status..."
                                CausesValidation="false"
                                AutoPostBack="true"
                                Width="83%" 
                                OnSelectedIndexChanged="rcb41_SelectedIndexChanged">
                                <Items>
                                    <telerik:RadComboBoxItem runat="server" Text="S" Value="S" />
                                    <telerik:RadComboBoxItem runat="server" Text="M" Value="M" />
                                    <telerik:RadComboBoxItem runat="server" Text="N" Value="N" />
                                </Items>
                            </telerik:RadComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span>Contact Number of children</span>
                        </td>
                        <td>
                            <asp:Label ID="Label42" runat="server" Text="Label"></asp:Label>
                        </td>
                        <td>
                            <span style="background: #ffa517;">Contact Number of children</span>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBox42" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span>Contact Birthdate (YYYYMM)</span>
                        </td>
                        <td>
                            <asp:Label ID="Label43" runat="server" Text="Label"></asp:Label>
                        </td>
                        <td>
                            <span style="background: #6fbb56;">Contact Birthdate (YYYYMM)</span>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBox43" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span>Contact Occupation</span>
                        </td>
                        <td>
                            <asp:Label ID="Label44" runat="server" Text="Label"></asp:Label>
                        </td>
                        <td>
                            <span style="background: #6fbb56;">Contact Occupation</span>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBox44" runat="server"></asp:TextBox>
                            <telerik:RadComboBox
                                runat="server"
                                ID="rcb44"
                                EmptyMessage="...Select Contact Occupation..."
                                CausesValidation="false"
                                AutoPostBack="true"
                                Width="83%" OnSelectedIndexChanged="rcb44_SelectedIndexChanged">
                                <Items>
                                    <telerik:RadComboBoxItem runat="server" Text="พนง.บริษัทเอกชน" Value="พนง.บริษัทเอกชน" />
                                    <telerik:RadComboBoxItem runat="server" Text="ข้าราชการ" Value="ข้าราชการ" />
                                    <telerik:RadComboBoxItem runat="server" Text="พนง.รัฐวิสาหกิจ" Value="พนง.รัฐวิสาหกิจ" />
                                    <telerik:RadComboBoxItem runat="server" Text="แพทย์" Value="แพทย์" />
                                    <telerik:RadComboBoxItem runat="server" Text="ครู/อาจารย์" Value="ครู/อาจารย์" />
                                    <telerik:RadComboBoxItem runat="server" Text="นักศึกษา" Value="นักศึกษา" />
                                    <telerik:RadComboBoxItem runat="server" Text="นักบิน" Value="นักบิน" />
                                    <telerik:RadComboBoxItem runat="server" Text="พนักงานต้อนรับบนเครื่องบิน" Value="พนักงานต้อนรับบนเครื่องบิน" />
                                    <telerik:RadComboBoxItem runat="server" Text="แม่บ้าน" Value="แม่บ้าน" />
                                    <telerik:RadComboBoxItem runat="server" Text="ดารา/นักแสดง" Value="ดารา/นักแสดง" />
                                    <telerik:RadComboBoxItem runat="server" Text="เจ้าของธุรกิจ" Value="เจ้าของธุรกิจ" />
                                    <telerik:RadComboBoxItem runat="server" Text="เทคโนโลยี/ไอที" Value="เทคโนโลยี/ไอที" />
                                    <telerik:RadComboBoxItem runat="server" Text="อัยการ/ทนายความ" Value="อัยการ/ทนายความ" />
                                    <telerik:RadComboBoxItem runat="server" Text="เกษียณ" Value="เกษียณ" />
                                    <telerik:RadComboBoxItem runat="server" Text="อาชีพอิสระ/freeland" Value="อาชีพอิสระ/freeland" />
                                    <telerik:RadComboBoxItem runat="server" Text="พนักงานขาย" Value="พนักงานขาย" />
                                    <telerik:RadComboBoxItem runat="server" Text="ตำรวจ" Value="ตำรวจ" />
                                    <telerik:RadComboBoxItem runat="server" Text="อื่นๆ --> ระบุ" Value="อื่นๆ" />
                                </Items>
                            </telerik:RadComboBox>
                             <asp:TextBox ID="txt_rcb44_other" runat="server" Visible="false"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span>Contact Hobby</span>
                        </td>
                        <td>
                            <asp:Label ID="Label45" runat="server" Text="Label"></asp:Label>
                        </td>
                        <td>
                            <span style="background: #6fbb56;">Contact Hobby</span>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBox45" runat="server"></asp:TextBox>
                              <telerik:RadComboBox
                                runat="server"
                                ID="rcb45"
                                EmptyMessage="...Select Contact Hobby..."
                                CausesValidation="false"
                                AutoPostBack="true"
                                Width="83%" OnSelectedIndexChanged="rcb45_SelectedIndexChanged">
                                <Items>
                                    <telerik:RadComboBoxItem runat="server" Text="กอล์ฟ" Value="กอล์ฟ" />
                                    <telerik:RadComboBoxItem runat="server" Text="เทนนิส" Value="เทนนิส" />
                                    <telerik:RadComboBoxItem runat="server" Text="ว่ายน้ำ" Value="ว่ายน้ำ" />
                                    <telerik:RadComboBoxItem runat="server" Text="แพทย์" Value="แพทย์" />
                                    <telerik:RadComboBoxItem runat="server" Text="ดนตรี" Value="ดนตรี" />
                                    <telerik:RadComboBoxItem runat="server" Text="นักศึกษา" Value="นักศึกษา" />
                                    <telerik:RadComboBoxItem runat="server" Text="ช๊อปปิ้ง" Value="ช๊อปปิ้ง" />
                                    <telerik:RadComboBoxItem runat="server" Text="ท่องเที่ยว" Value="ท่องเที่ยว" />
                                    <telerik:RadComboBoxItem runat="server" Text="ดูภาพยนตร์" Value="ดูภาพยนตร์" />
                                    <telerik:RadComboBoxItem runat="server" Text="อ่านหนังสือ" Value="อ่านหนังสือ" />
                                    <telerik:RadComboBoxItem runat="server" Text="ปลูกต้นไม้" Value="ปลูกต้นไม้" />
                                    <telerik:RadComboBoxItem runat="server" Text="เจ็ตสกี" Value="เจ็ตสกี" />
                                    <telerik:RadComboBoxItem runat="server" Text="ฟิตเนส" Value="ฟิตเนส" />
                                    <telerik:RadComboBoxItem runat="server" Text="สควอช" Value="สควอช" />
                                    <telerik:RadComboBoxItem runat="server" Text="อื่นๆ --> ระบุ" Value="อื่นๆ" />
                                </Items>
                            </telerik:RadComboBox>
                            <asp:TextBox ID="txt_rcb45_other" runat="server" Visible="false"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span>Contact Income /month</span>
                        </td>
                        <td>
                            <asp:Label ID="Label46" runat="server" Text="Label"></asp:Label>
                        </td>
                        <td>
                            <span style="background: #6fbb56;">Contact Income /month</span>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBox46" runat="server"></asp:TextBox>
                            <telerik:RadComboBox
                                runat="server"
                                ID="rcb46"
                                EmptyMessage="...Select contact Income Sequence..."
                                CausesValidation="false"
                                AutoPostBack="true"
                                Width="83%" OnSelectedIndexChanged="rcb46_SelectedIndexChanged">
                                <Items>
                                    <telerik:RadComboBoxItem runat="server" Text="<80000" Value="<80000" />
                                    <telerik:RadComboBoxItem runat="server" Text="80000-100000" Value="80000-100000" />
                                    <telerik:RadComboBoxItem runat="server" Text="100000-150000" Value="100000-150000" />
                                    <telerik:RadComboBoxItem runat="server" Text="150000-200000" Value="150000-200000" />
                                    <telerik:RadComboBoxItem runat="server" Text=">200,000" Value=">200,000" />
                                    <telerik:RadComboBoxItem runat="server" Text="ไม่ระบุ" Value="ไม่ระบุ" />
                                </Items>
                            </telerik:RadComboBox>
                        </td>
                    </tr>
                    <tr style="display: none;">
                        <td>
                            <span>Branch Owner</span>
                        </td>
                        <td>
                            <asp:Label ID="Label47" runat="server" Text="Label"></asp:Label>
                        </td>
                        <td>
                            <span style="background: #f10000;">Branch Owner</span>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBox47" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    
                    <tr style="display: none;">
                        <td>
                            <span>Car Variance</span>
                        </td>
                        <td>
                            <asp:Label ID="Label50" runat="server" Text="Label"></asp:Label>
                        </td>
                        <td>
                            <span style="background: #f10000;">Car Variance</span>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBox50" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                   
                    <tr style="display: none;">
                        <td>
                            <span>Car MY</span>
                        </td>
                        <td>
                            <asp:Label ID="Label52" runat="server" Text="Label"></asp:Label>
                        </td>
                        <td>
                            <span style="background: #f10000;">Car MY</span>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBox52" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    
                    <tr style="display: none;">
                        <td>
                            <span>Retail Branch</span>
                        </td>
                        <td>
                            <asp:Label ID="Label54" runat="server" Text="Label"></asp:Label>
                        </td>
                        <td>
                            <span style="background: #f10000;">Retail Branch</span>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBox54" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr style="display: none;">
                        <td>
                            <span>Last Service Br.</span>
                        </td>
                        <td>
                            <asp:Label ID="Label55" runat="server" Text="Label"></asp:Label>
                        </td>
                        <td>
                            <span style="background: #f10000;">Last Service Br.</span>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBox55" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                     
                    <tr style="display: none;">
                        <td>
                            <span>Sales code & Name</span>
                        </td>
                        <td>
                            <asp:Label ID="Label57" runat="server" Text="Label"></asp:Label>
                        </td>
                        <td>
                            <span style="background: #f10000;">Sales code & Name</span>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBox57" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr style="display: none;">
                        <td>
                            <span>Last Mileage</span>
                        </td>
                        <td>
                            <asp:Label ID="Label58" runat="server" Text="Label"></asp:Label>
                        </td>
                        <td>
                            <span style="background: #f10000;">Last Mileage</span>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBox58" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr style="display: none;">
                        <td>
                            <span>SA Code</span>
                        </td>
                        <td>
                            <asp:Label ID="Label59" runat="server" Text="Label"></asp:Label>
                        </td>
                        <td>
                            <span style="background: #f10000;">SA Code</span>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBox59" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr style="display: none;">
                        <td>
                            <span>SA Name</span>
                        </td>
                        <td>
                            <asp:Label ID="Label60" runat="server" Text="Label"></asp:Label>
                        </td>
                        <td>
                            <span style="background: #f10000;">SA Name</span>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBox60" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr style="display: none;">
                        <td>
                            <span>Car Usage Type</span>
                        </td>
                        <td>
                            <asp:Label ID="Label61" runat="server" Text="Label"></asp:Label>
                        </td>
                        <td>
                            <span style="background: #f10000;">Car Usage Type</span>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBox61" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr style="display: none;">
                        <td>
                            <span>Remarks</span>
                        </td>
                        <td>
                            <asp:Label ID="Label62" runat="server" Text="Label"></asp:Label>
                        </td>
                        <td>
                            <span>Remarks</span>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBox62" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            
                        </td>
                        <td>
                           
                        </td>
                        <td>
                            <span>แก้ไขข้อมูล Owner หรือไม่ Y/N</span>
                        </td>
                        <td>
                             <telerik:RadComboBox
                                runat="server"
                                ID="rcbIsEdit"
                                EmptyMessage="...Select contact marital status..."
                                CausesValidation="false"
                                AutoPostBack="true"
                                Width="83%" 
                                OnSelectedIndexChanged="rcbIsEdit_SelectedIndexChanged">
                                <Items>
                                    <telerik:RadComboBoxItem runat="server" Text="Y" Value="Y" />
                                    <telerik:RadComboBoxItem runat="server" Selected="true" Text="N" Value="N" />
                                </Items>
                            </telerik:RadComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                           
                        </td>
                        <td>
                        </td>
                        <td>
                            <span>Reason (If Yes)</span>
                        </td>
                        <td>
                             <telerik:RadComboBox
                                runat="server"
                                ID="rcbIsReason"
                                EmptyMessage="...Select reason..."
                                CausesValidation="false"
                                AutoPostBack="true"
                                Width="83%">
                                <Items>
                                    <%--<telerik:RadComboBoxItem runat="server" Text="No Change" Value="No Change" Selected="true" />--%>
                                    <telerik:RadComboBoxItem runat="server" Text="No Change" Value="No Change" Selected="true"/>
                                    <telerik:RadComboBoxItem runat="server" Text="Change owner - Complete" Value="Change owner - Complete" />
                                    <telerik:RadComboBoxItem runat="server" Text="Change owner - Incomplete" Value="Change owner - Incomplete" />
                                    <telerik:RadComboBoxItem runat="server" Text="Change Information" Value="Change Information" />
                                    <telerik:RadComboBoxItem runat="server" Text="Add information" Value="Add information" />
                                </Items>
                            </telerik:RadComboBox>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </asp:Panel>

    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server"></telerik:RadAjaxLoadingPanel>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" EnableAJAX="true" EnablePageHeadUpdate="false" EnableViewState="false">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="rcbStatus">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="rcbSubStatus" LoadingPanelID="RadAjaxLoadingPanel1"></telerik:AjaxUpdatedControl>
                </UpdatedControls>
            </telerik:AjaxSetting>
            <%--<telerik:AjaxSetting AjaxControlID="rcb4">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Textbox4" LoadingPanelID="RadAjaxLoadingPanel1"></telerik:AjaxUpdatedControl>
                </UpdatedControls>
            </telerik:AjaxSetting>--%>
            <telerik:AjaxSetting AjaxControlID="rcb5">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Textbox5" LoadingPanelID="RadAjaxLoadingPanel1"></telerik:AjaxUpdatedControl>
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="rcb24">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Textbox24" LoadingPanelID="RadAjaxLoadingPanel1"></telerik:AjaxUpdatedControl>
                    <telerik:AjaxUpdatedControl ControlID="txt_rcb24_other" LoadingPanelID="RadAjaxLoadingPanel1"></telerik:AjaxUpdatedControl>
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="rcb25">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Textbox25" LoadingPanelID="RadAjaxLoadingPanel1"></telerik:AjaxUpdatedControl>
                    <telerik:AjaxUpdatedControl ControlID="txt_rcb25_other" LoadingPanelID="RadAjaxLoadingPanel1"></telerik:AjaxUpdatedControl>
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="rcb26">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Textbox26" LoadingPanelID="RadAjaxLoadingPanel1"></telerik:AjaxUpdatedControl>
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="rcb13">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Textbox13" LoadingPanelID="RadAjaxLoadingPanel1"></telerik:AjaxUpdatedControl>
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="rcb20">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Textbox20" LoadingPanelID="RadAjaxLoadingPanel1"></telerik:AjaxUpdatedControl>
                </UpdatedControls>
            </telerik:AjaxSetting>
             <telerik:AjaxSetting AjaxControlID="rcb21">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Textbox21" LoadingPanelID="RadAjaxLoadingPanel1"></telerik:AjaxUpdatedControl>
                </UpdatedControls>
            </telerik:AjaxSetting>
            
            <telerik:AjaxSetting AjaxControlID="rcb30">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Textbox30" LoadingPanelID="RadAjaxLoadingPanel1"></telerik:AjaxUpdatedControl>
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="rcb40">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Textbox40" LoadingPanelID="RadAjaxLoadingPanel1"></telerik:AjaxUpdatedControl>
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="rcb44">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Textbox44" LoadingPanelID="RadAjaxLoadingPanel1"></telerik:AjaxUpdatedControl>
                    <telerik:AjaxUpdatedControl ControlID="txt_rcb44_other" LoadingPanelID="RadAjaxLoadingPanel1"></telerik:AjaxUpdatedControl>
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="rcb45">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Textbox45" LoadingPanelID="RadAjaxLoadingPanel1"></telerik:AjaxUpdatedControl>
                    <telerik:AjaxUpdatedControl ControlID="txt_rcb45_other" LoadingPanelID="RadAjaxLoadingPanel1"></telerik:AjaxUpdatedControl>
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="rcb46">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Textbox46" LoadingPanelID="RadAjaxLoadingPanel1"></telerik:AjaxUpdatedControl>
                </UpdatedControls>
            </telerik:AjaxSetting>
            <%--<telerik:AjaxSetting AjaxControlID="BtnSaveCallStatus">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="PanelContent" LoadingPanelID="RadAjaxLoadingPanel1"></telerik:AjaxUpdatedControl>
                </UpdatedControls>
            </telerik:AjaxSetting>--%>
            <telerik:AjaxSetting AjaxControlID="rcbProvince10">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="rcbDistrict10" LoadingPanelID="RadAjaxLoadingPanel1"></telerik:AjaxUpdatedControl>
                    <telerik:AjaxUpdatedControl ControlID="rcbSubDistrict10" LoadingPanelID="RadAjaxLoadingPanel1"></telerik:AjaxUpdatedControl>
                    <telerik:AjaxUpdatedControl ControlID="TextBox10" LoadingPanelID="RadAjaxLoadingPanel1"></telerik:AjaxUpdatedControl>
                    <telerik:AjaxUpdatedControl ControlID="TextBox11" LoadingPanelID="RadAjaxLoadingPanel1"></telerik:AjaxUpdatedControl>
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="rcbDistrict10">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="rcbSubDistrict10" LoadingPanelID="RadAjaxLoadingPanel1"></telerik:AjaxUpdatedControl>
                    <telerik:AjaxUpdatedControl ControlID="TextBox10" LoadingPanelID="RadAjaxLoadingPanel1"></telerik:AjaxUpdatedControl>
                    <telerik:AjaxUpdatedControl ControlID="TextBox11" LoadingPanelID="RadAjaxLoadingPanel1"></telerik:AjaxUpdatedControl>
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="rcbSubDistrict10">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="TextBox10" LoadingPanelID="RadAjaxLoadingPanel1"></telerik:AjaxUpdatedControl>
                    <telerik:AjaxUpdatedControl ControlID="TextBox11" LoadingPanelID="RadAjaxLoadingPanel1"></telerik:AjaxUpdatedControl>
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="rcbProvince33">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="rcbDistrict33" LoadingPanelID="RadAjaxLoadingPanel1"></telerik:AjaxUpdatedControl>
                    <telerik:AjaxUpdatedControl ControlID="rcbSubDistrict33" LoadingPanelID="RadAjaxLoadingPanel1"></telerik:AjaxUpdatedControl>
                    <telerik:AjaxUpdatedControl ControlID="TextBox34" LoadingPanelID="RadAjaxLoadingPanel1"></telerik:AjaxUpdatedControl>
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="rcbDistrict33">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="rcbSubDistrict33" LoadingPanelID="RadAjaxLoadingPanel1"></telerik:AjaxUpdatedControl>
                    <telerik:AjaxUpdatedControl ControlID="TextBox34" LoadingPanelID="RadAjaxLoadingPanel1"></telerik:AjaxUpdatedControl>
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="rcbSubDistrict33">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="TextBox34" LoadingPanelID="RadAjaxLoadingPanel1"></telerik:AjaxUpdatedControl>
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="rcb41">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Textbox41" LoadingPanelID="RadAjaxLoadingPanel1"></telerik:AjaxUpdatedControl>
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="rcbIsEdit">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="rcbIsReason" LoadingPanelID="RadAjaxLoadingPanel1"></telerik:AjaxUpdatedControl>
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>

</asp:Content>

