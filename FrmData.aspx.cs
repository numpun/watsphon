﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Data;

public partial class FrmData : System.Web.UI.Page
{
    GetExecution GE = new GetExecution();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserID"] == null || Session["UserLevelID"] == null)
        {
            Response.Redirect("~/Default.aspx");
        }
        if (!this.IsPostBack)
        {
            lblUserLavel.Text = Session["UserLevelID"].ToString();
            lblUserID.Text = Session["UserID"].ToString();
            if (!string.IsNullOrEmpty(Request.QueryString["cusid"]))
            {
                lblCusID.Text = Request.QueryString["cusid"].ToString();
                if (!ChkNewApp(lblCusID.Text.Trim()))
                {
                    InsertFirstApp(lblCusID.Text.Trim());
                    loadtools();
                    loadcustomermaster(lblCusID.Text.Trim());
                    loadcustomer(lblCusID.Text.Trim());
                }
                else
                {
                    loadtools();
                    loadcustomermaster(lblCusID.Text.Trim());
                    loadcustomer(lblCusID.Text.Trim());
                }
            }
            else
            {
                Response.Redirect("~/ListAgent.aspx");
            }
        }
    }
    public void loadprovince()
    {
        try
        {
            string strSQL = " SELECT PROVINCE_ID AS Value, PROVINCE_NAME AS ValueText FROM province ";
            var dt = GE.SELECT_Executeion(strSQL);
            if (dt.Rows.Count > 0)
            {
                rcbProvince10.DataSource = dt;
                rcbProvince10.DataTextField = "ValueText";
                rcbProvince10.DataValueField = "Value";
                rcbProvince10.DataBind();
                rcbProvince10.SelectedIndex = 0;
                rcbProvince10_SelectedIndexChanged(null, null);

                rcbProvince33.DataSource = dt;
                rcbProvince33.DataTextField = "ValueText";
                rcbProvince33.DataValueField = "Value";
                rcbProvince33.DataBind();
                rcbProvince33.SelectedIndex = 0;
                rcbProvince33_SelectedIndexChanged(null, null);
            }
        }
        catch (Exception ex)
        {
            RadAjaxManager1.Alert(ex.Message);
        }
    }
    private bool ChkNewApp(string CusID)
    {
        try
        {
            string strSQL = " SELECT CusID FROM TblCustomer WHERE ReferCusID = '" + CusID + "'  ";

            DataTable dt = GE.SELECT_Executeion(strSQL);
            if (dt.Rows.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        catch (Exception ex)
        {
            RadAjaxManager1.Alert(ex.Message);
            return false;
        }
    }

    public void InsertFirstApp(string CusID)
    {
        try
        {

            #region
            string strSQL = @"  INSERT INTO TblCustomer ( ReferCusID,
                                CreateID,
                                UpdateID,
                                CustomerCode,
                                CarID,
                                CDBID,
                                Tupe,
                                Title,
                                Name,
                                BrNo,
                                Address1,
                                Address2,
                                District,
                                Zipcode,
                                TaxID,
                                CompleateAccount,
                                HomePhone,
                                OfficePhone,
                                Mobile,
                                Fax,
                                Email,
                                Gender,
                                MaritalStatus,
                                NumberOfChilden,
                                Birthdate,
                                Occupation,
                                Hobby,
                                Income,
                                PerferredLanguage,
                                ContactType,
                                PreferContactChanel,
                                ContactTitle,
                                ContactName,
                                ContactAddress1,
                                ContactAddress2,
                                ContactZipcode,
                                Contact,
                                ContactOfficePhone,
                                ContactMobile,
                                ContactFax,
                                ContactEmail,
                                ContactGender,
                                ContactMaritalStatus,
                                ContactNumberOfChilden,
                                ContactBirthdate,
                                BaranchOwner,
                                VIN,
                                License,
                                CarVaiance,
                                CarModel,
                                CarMY,
                                RetailDate,
                                RetailBranch,
                                LastServiceBr,
                                LastServiceDate,
                                SalesCodeorName,
                                LastMileage,
                                SACode,
                                SAName,
                                CarUsageType,
                                Remarks,
                                IsEdit,
                                Reason,
                                ContactOccupation,
                                ContactHobby,
                                ContactIncome,
                                mCustomerProvinceName,
                                mCustomerDistrictID,
                                mCustomerDistrictName,
                                mCustomerSubDisName,
                                mCustomerZipcode,
                                mCustomerProvinceID,
                                mCustomerSubDistID,
                                mCustomerHNumber,
                                mCustomerHM,
                                mCustomerSoi,
                                mCustomerRoad,
                                mCustomerHV,
                                mCTProvinceName,
                                mCTProvinceID,
                                mCTDistrictName,
                                mCTDistrictID,
                                mSubDisName,
                                mCTSubDistID,
                                mCTHNumber,
                                mCTHM,
                                mCTHV,
                                mCTSoi,
                                mCTRoad,
                                mCTZipcode)
                                SELECT
                                CusID,
                                OwnerID,
                                OwnerID,
                                CustomerCode,
                                CarID,
                                CDBID,
                                Tupe,
                                Title,
                                Name,
                                BrNo,
                                Address1,
                                Address2,
                                District,
                                Zipcode,
                                TaxID,
                                CompleateAccount,
                                HomePhone,
                                OfficePhone,
                                Mobile,
                                Fax,
                                Email,
                                Gender,
                                MaritalStatus,
                                NumberOfChilden,
                                Birthdate,
                                Occupation,
                                Hobby,
                                Income,
                                PerferredLanguage,
                                ContactType,
                                PreferContactChanel,
                                ContactTitle,
                                ContactName,
                                ContactAddress1,
                                ContactAddress2,
                                ContactZipcode,
                                Contact,
                                ContactOfficePhone,
                                ContactMobile,
                                ContactFax,
                                ContactEmail,
                                ContactGender,
                                ContactMaritalStatus,
                                ContactNumberOfChilden,
                                ContactBirthdate,
                                BaranchOwner,
                                VIN,
                                License,
                                CarVaiance,
                                CarModel,
                                CarMY,
                                RetailDate,
                                RetailBranch,
                                LastServiceBr,
                                LastServiceDate,
                                SalesCodeorName,
                                LastMileage,
                                SACode,
                                SAName,
                                CarUsageType,
                                Remarks,
                                IsEdit,
                                Reason,
                                ContactOccupation,
                                ContactHobby,
                                ContactIncome,
                                mCustomerProvinceName,
                                mCustomerDistrictID,
                                mCustomerDistrictName,
                                mCustomerSubDisName,
                                mCustomerZipcode,
                                mCustomerProvinceID,
                                mCustomerSubDistID,
                                mCustomerHNumber,
                                mCustomerHM,
                                mCustomerSoi,
                                mCustomerRoad,
                                mCustomerHV,
                                mCTProvinceName,
                                mCTProvinceID,
                                mCTDistrictName,
                                mCTDistrictID,
                                mSubDisName,
                                mCTSubDistID,
                                mCTHNumber,
                                mCTHM,
                                mCTHV,
                                mCTSoi,
                                mCTRoad,
                                mCTZipcode FROM TblCustomer_master WHERE CusID = " + CusID;
            #endregion
            GE.INSERT_Executeion(strSQL);
        }
        catch (Exception ex)
        {
            RadAjaxManager1.Alert(ex.Message);
        }
    }

    public void loadtools()
    {
        loaduserlevel();
        loadprovince();
        loadcallon();
        if (lblUserLavel.Text.Trim() == "T")
        {
            loadcallstatus();
            loadtalkscript();
        }
        loadenabled();
        loadradcombobox();

    }

    public void loaduserlevel()
    {
        if (lblUserLavel.Text.Trim() == "T")
        {
            talkscript.Visible = true;
            callstatus.Visible = true;
        }

        if (lblUserLavel.Text.Trim() == "Q")
        {
            PanelQC.Visible = true;
        }
    }

    public void loadradcombobox()
    {
        //rcb5 title
        DataTable dtrcb5 = GE.SELECT_Executeion(" SELECT DISTINCT Title as title FROM TblCustomer_master "); rcb5.DataSource = dtrcb5; rcb5.DataTextField = "title"; rcb5.DataValueField = "title"; rcb5.DataBind();

        //rcb20 contact title
        DataTable dtrcb30 = GE.SELECT_Executeion(" SELECT DISTINCT ContactTitle as ctitle FROM TblCustomer_master "); rcb30.DataSource = dtrcb30; rcb30.DataTextField = "ctitle"; rcb30.DataValueField = "ctitle"; rcb30.DataBind();
    }

    public void loadenabled()
    {
        TextBox1.Enabled = false;
        TextBox2.Enabled = false;
        TextBox3.Enabled = false;
        TextBox4.Enabled = false;
        TextBox5.Enabled = false;

        TextBox13.Enabled = false;

        TextBox20.Enabled = false;
        TextBox21.Enabled = false;
        TextBox24.Enabled = false;
        TextBox25.Enabled = false;
        TextBox26.Enabled = false;
        TextBox27.Enabled = false;
        TextBox30.Enabled = false;
        TextBox40.Enabled = false;

        TextBox41.Enabled = false;
        TextBox47.Enabled = false;
        TextBox48.Enabled = false;
        TextBox49.Enabled = false;
        TextBox50.Enabled = false;
        TextBox51.Enabled = false;
        TextBox52.Enabled = false;
        TextBox53.Enabled = false;
        TextBox54.Enabled = false;
        TextBox55.Enabled = false;
        TextBox56.Enabled = false;
        TextBox57.Enabled = false;
        TextBox58.Enabled = false;
        TextBox59.Enabled = false;
        TextBox60.Enabled = false;
        TextBox61.Enabled = false;
        TextBox62.Enabled = false;
    }

    public void loadtupegroup(string TupeType)
    {
        try
        {
            if (TupeType == "I")
            {
                groupnoneI_7.Visible = false;
                groupnoneI_12.Visible = false;
            }
            else
            {
                groupnoneI_7.Visible = true;
                groupnoneI_12.Visible = true;
            }
        }
        catch (Exception ex)
        {
            RadAjaxManager1.Alert(ex.Message);
        }
    }

    public void loadcustomermaster(string CusID)
    {
        try
        {
            string strSQL = " SELECT TOP(1) * FROM TblCustomer_master WHERE CusID = '" + CusID + "' ";
            DataTable dt = GE.SELECT_Executeion(strSQL);
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];

                lblCntCall.Text = dr["CntCall"].ToString();

                if (dr["AppStatus"].ToString() != "1")
                rdoQCAppStatus.SelectedValue = dr["AppStatus"].ToString();

                loadtupegroup(dr["Tupe"].ToString());
                rcbIsEdit.SelectedValue = dr["IsEdit"].ToString();
                rcbIsReason.SelectedValue = dr["IsReason"].ToString();
                txacomments.Text = dr["comments"].ToString();


                //==========================================
                //1-10
                Label1.Text = dr["CustomerCode"].ToString();
                Label2.Text = dr["CarID"].ToString();
                Label3.Text = dr["CDBID"].ToString();
                Label4.Text = dr["Tupe"].ToString();
                Label5.Text = dr["Title"].ToString();
                Label6.Text = dr["Name"].ToString();
                Label7.Text = dr["BrNo"].ToString();
                Label8.Text = dr["Address1"].ToString();
                Label9.Text = dr["Address2"].ToString();
                Label10.Text = dr["District"].ToString();

                lblCustoemrAddress1_1.Text = dr["mCustomerHNumber"].ToString();
                lblCustoemrAddress1_2.Text = dr["mCustomerHV"].ToString();
                lblCustoemrAddress1_3.Text = dr["mCustomerHM"].ToString();
                lblCustoemrAddress1_4.Text = dr["mCTSoi"].ToString();
                lblCustoemrAddress1_5.Text = dr["mCTRoad"].ToString();
                lblProvince10.Text = dr["mCustomerProvinceName"].ToString();
                lblDistrict10.Text = dr["mCustomerDistrictName"].ToString();
                lblSubDistrict10.Text = dr["mCustomerSubDisName"].ToString();

                //==========================================
                //11-20 !=14
                Label11.Text = dr["Zipcode"].ToString();
                Label12.Text = dr["TaxID"].ToString();
                Label13.Text = dr["CompleateAccount"].ToString();
                Label15.Text = dr["HomePhone"].ToString();
                Label16.Text = dr["OfficePhone"].ToString();
                Label17.Text = dr["Mobile"].ToString();
                Label18.Text = dr["Fax"].ToString();
                Label19.Text = dr["Email"].ToString();
                Label20.Text = dr["Gender"].ToString();
                //==========================================
                //21-30
                Label21.Text = dr["MaritalStatus"].ToString();
                Label22.Text = dr["NumberOfChilden"].ToString();
                Label23.Text = dr["Birthdate"].ToString();
                Label24.Text = dr["Occupation"].ToString();
                Label25.Text = dr["Hobby"].ToString();
                Label26.Text = dr["Income"].ToString();
                Label27.Text = dr["PerferredLanguage"].ToString();
                Label28.Text = dr["ContactType"].ToString();
                Label29.Text = dr["PreferContactChanel"].ToString();
                Label30.Text = dr["ContactTitle"].ToString();
                //==========================================
                //31-40
                Label31.Text = dr["ContactName"].ToString();
                Label32.Text = dr["ContactAddress1"].ToString();
                Label33.Text = dr["ContactAddress2"].ToString();
                Label34.Text = dr["ContactZipcode"].ToString();
                Label35.Text = dr["Contact"].ToString();
                Label36.Text = dr["ContactOfficePhone"].ToString();
                Label37.Text = dr["ContactMobile"].ToString();
                Label38.Text = dr["ContactFax"].ToString();
                Label39.Text = dr["ContactEmail"].ToString();
                Label40.Text = dr["ContactGender"].ToString();
                //==========================================
                //41-50
                Label41.Text = dr["ContactMaritalStatus"].ToString();
                Label42.Text = dr["ContactNumberOfChilden"].ToString();
                Label43.Text = dr["ContactBirthdate"].ToString();
                Label44.Text = dr["ContactOccupation"].ToString();
                Label45.Text = dr["ContactHobby"].ToString();
                Label46.Text = dr["ContactIncome"].ToString();
                Label47.Text = dr["BaranchOwner"].ToString();
                Label48.Text = dr["VIN"].ToString();
                Label49.Text = dr["License"].ToString();
                Label50.Text = dr["CarVaiance"].ToString();
                //==========================================
                //51-62
                Label51.Text = dr["CarModel"].ToString();
                Label52.Text = dr["CarMY"].ToString();
                Label53.Text = dr["RetailDate"].ToString();
                Label54.Text = dr["RetailBranch"].ToString();
                Label55.Text = dr["LastServiceBr"].ToString();
                Label56.Text = dr["LastServiceDate"].ToString();
                Label57.Text = dr["SalesCodeorName"].ToString();
                Label58.Text = dr["LastMileage"].ToString();
                Label59.Text = dr["SACode"].ToString();
                Label60.Text = dr["SAName"].ToString();
                Label61.Text = dr["CarUsageType"].ToString();
                Label62.Text = dr["Remarks"].ToString();

            }
        }
        catch (Exception ex)
        {
            RadAjaxManager1.Alert(ex.Message);
        }
    }
    public void loadcustomer(string CusID)
    {
        try
        {
            string strSQL = " SELECT TOP(1) * FROM TblCustomer WHERE ReferCusID = '" + CusID + "' ";
            DataTable dt = GE.SELECT_Executeion(strSQL);
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];

                //==========================================
                //1-10
                TextBox1.Text = dr["CustomerCode"].ToString();
                TextBox2.Text = dr["CarID"].ToString();
                TextBox3.Text = dr["CDBID"].ToString();
                TextBox4.Text = dr["Tupe"].ToString(); rcb4.SelectedValue = dr["Tupe"].ToString();
                TextBox5.Text = dr["Title"].ToString(); rcb5.SelectedValue = dr["Title"].ToString();
                TextBox6.Text = dr["Name"].ToString();
                TextBox7.Text = dr["BrNo"].ToString();
                TextBox8.Text = dr["Address1"].ToString();
                TextBox9.Text = dr["Address2"].ToString();
                TextBox10.Text = dr["District"].ToString();

                //customer address 1 + 2 + distrinct name
                txtCustoemrAddress1_1.Text = dr["mCustomerHNumber"].ToString();
                txtCustoemrAddress1_2.Text = dr["mCustomerHV"].ToString();
                txtCustoemrAddress1_3.Text = dr["mCustomerHM"].ToString();
                txtCustoemrAddress1_4.Text = dr["mCustomerSoi"].ToString();
                txtCustoemrAddress1_5.Text = dr["mCustomerRoad"].ToString();

                if (dr["mCustomerProvinceID"].ToString().Trim() != "")
                {
                    rcbProvince10.SelectedValue = dr["mCustomerProvinceID"].ToString();
                    rcbProvince10_SelectedIndexChanged(null, null);
                }
                if (dr["mCustomerDistrictID"].ToString().Trim() != "")
                {
                    rcbDistrict10.SelectedValue = dr["mCustomerDistrictID"].ToString();
                    rcbDistrict10_SelectedIndexChanged(null, null);
                }
                if (dr["mCustomerSubDistID"].ToString().Trim() != "")
                {
                    rcbSubDistrict10.SelectedValue = dr["mCustomerSubDistID"].ToString();
                    rcbSubDistrict10_SelectedIndexChanged(null, null);
                }

                //==========================================
                //11-20 !=14
                TextBox11.Text = dr["Zipcode"].ToString();
                TextBox12.Text = dr["TaxID"].ToString();
                TextBox13.Text = dr["CompleateAccount"].ToString(); rcb13.SelectedValue = dr["CompleateAccount"].ToString();
                TextBox15.Text = dr["HomePhone"].ToString();
                TextBox16.Text = dr["OfficePhone"].ToString();
                TextBox17.Text = dr["Mobile"].ToString();
                TextBox18.Text = dr["Fax"].ToString();
                TextBox19.Text = dr["Email"].ToString();
                TextBox20.Text = dr["Gender"].ToString(); rcb20.SelectedValue = dr["Gender"].ToString();
                //==========================================
                //21-30
                TextBox21.Text = dr["MaritalStatus"].ToString(); rcb21.SelectedValue = dr["MaritalStatus"].ToString();
                TextBox22.Text = dr["NumberOfChilden"].ToString();
                TextBox23.Text = dr["Birthdate"].ToString();
                TextBox24.Text = dr["Occupation"].ToString(); rcb24.SelectedValue = dr["Occupation"].ToString(); if (dr["Occupation"].ToString() == "อื่นๆ") { txt_rcb24_other.Visible=true; } else { txt_rcb24_other.Visible = false; } txt_rcb24_other.Text = dr["mOccupationOther"].ToString();
                TextBox25.Text = dr["Hobby"].ToString(); rcb25.SelectedValue = dr["Hobby"].ToString(); if (dr["Hobby"].ToString() == "อื่นๆ") { txt_rcb25_other.Visible = true; } else { txt_rcb25_other.Visible = false; } txt_rcb25_other.Text = dr["mHobbyOther"].ToString();
                TextBox26.Text = dr["Income"].ToString(); rcb26.SelectedValue = dr["Income"].ToString();
                TextBox27.Text = dr["PerferredLanguage"].ToString();
                TextBox28.Text = dr["ContactType"].ToString();
                TextBox29.Text = dr["PreferContactChanel"].ToString();
                TextBox30.Text = dr["ContactTitle"].ToString(); rcb30.SelectedValue = dr["ContactTitle"].ToString();
                //==========================================
                //31-40
                TextBox31.Text = dr["ContactName"].ToString();
                TextBox32.Text = dr["ContactAddress1"].ToString();
                TextBox33.Text = dr["ContactAddress2"].ToString();

                //customer address 1 + 2 + distrinct name

                txtAddressCC1_1.Text = dr["mCTHNumber"].ToString();
                txtAddressCC1_2.Text = dr["mCTHV"].ToString();
                txtAddressCC1_3.Text = dr["mCTHM"].ToString();
                txtAddressCC1_4.Text = dr["mCTSoi"].ToString();
                txtAddressCC1_5.Text = dr["mCTRoad"].ToString();
                if (dr["mCTProvinceID"].ToString().Trim() != "")
                {
                    rcbProvince33.SelectedValue = dr["mCTProvinceID"].ToString();
                    rcbProvince33_SelectedIndexChanged(null, null);
                }
                if (dr["mCTDistrictID"].ToString().Trim() != "")
                {
                    rcbDistrict33.SelectedValue = dr["mCTDistrictID"].ToString();
                    rcbDistrict33_SelectedIndexChanged(null, null);
                }
                if (dr["mCTSubDistID"].ToString().Trim() != "")
                {
                    rcbSubDistrict33.SelectedValue = dr["mCTSubDistID"].ToString();
                    rcbSubDistrict33_SelectedIndexChanged(null, null);
                }
                TextBox34.Text = dr["ContactZipcode"].ToString();
                TextBox35.Text = dr["Contact"].ToString();
                TextBox36.Text = dr["ContactOfficePhone"].ToString();
                TextBox37.Text = dr["ContactMobile"].ToString();
                TextBox38.Text = dr["ContactFax"].ToString();
                TextBox39.Text = dr["ContactEmail"].ToString();
                TextBox40.Text = dr["ContactGender"].ToString(); rcb40.SelectedValue = dr["ContactGender"].ToString();
                //==========================================
                //41-50
                TextBox41.Text = dr["ContactMaritalStatus"].ToString(); rcb41.SelectedValue = dr["ContactMaritalStatus"].ToString();
                TextBox42.Text = dr["ContactNumberOfChilden"].ToString();
                TextBox43.Text = dr["ContactBirthdate"].ToString();
                TextBox44.Text = dr["ContactOccupation"].ToString(); rcb44.SelectedValue = dr["ContactOccupation"].ToString(); if (dr["ContactOccupation"].ToString() == "อื่นๆ") { txt_rcb44_other.Visible = true; } else { txt_rcb44_other.Visible = false; } txt_rcb44_other.Text = dr["mContactOccupationOther"].ToString();
                TextBox45.Text = dr["ContactHobby"].ToString(); rcb45.SelectedValue = dr["ContactHobby"].ToString(); if (dr["ContactHobby"].ToString() == "อื่นๆ") { txt_rcb45_other.Visible = true; } else { txt_rcb45_other.Visible = false; } txt_rcb45_other.Text = dr["mContactHobbyOther"].ToString();
                TextBox46.Text = dr["ContactIncome"].ToString(); rcb46.SelectedValue = dr["ContactIncome"].ToString();
                TextBox47.Text = dr["BaranchOwner"].ToString();
                TextBox48.Text = dr["VIN"].ToString();
                TextBox49.Text = dr["License"].ToString();
                TextBox50.Text = dr["CarVaiance"].ToString();
                //==========================================
                //51-62
                TextBox51.Text = dr["CarModel"].ToString();
                TextBox52.Text = dr["CarMY"].ToString();
                TextBox53.Text = dr["RetailDate"].ToString();
                TextBox54.Text = dr["RetailBranch"].ToString();
                TextBox55.Text = dr["LastServiceBr"].ToString();
                TextBox56.Text = dr["LastServiceDate"].ToString();
                TextBox57.Text = dr["SalesCodeorName"].ToString();
                TextBox58.Text = dr["LastMileage"].ToString();
                TextBox59.Text = dr["SACode"].ToString();
                TextBox60.Text = dr["SAName"].ToString();
                TextBox61.Text = dr["CarUsageType"].ToString();
                TextBox62.Text = dr["Remarks"].ToString();

            }
        }
        catch (Exception ex)
        {
            RadAjaxManager1.Alert(ex.Message);
        }
    }
    public void loadcallon()
    {
        try
        {
            string strSQL = " SELECT TOP(1) CusID, HomePhone as SHomePhone , Mobile as SMobile, OfficePhone as Sofficephone  "
                          + " FROM TblCustomer_master WHERE CusID = '"+ lblCusID.Text.Trim() +"' ";

            DataTable dt = GE.SELECT_Executeion(strSQL);
            if (dt.Rows.Count > 0)
            {
                RadGridCallOn.DataSource = dt;
                RadGridCallOn.DataBind();
            }
            else
            {
                RadGridCallOn.DataSource = new Object[0];
                RadGridCallOn.DataBind();
            }
        }
        catch (Exception ex)
        {
            RadAjaxManager1.Alert(ex.Message);
        }
    }
    public void loadcallstatus()
    {
        try
        {
            string strSQL = " SELECT StatusID AS Value, StatusNameTH AS ValueText "
                          + " FROM TblStatus WHERE Active = 'Y' AND StatusID NOT IN(1) ";

            DataTable dt = GE.SELECT_Executeion(strSQL);
            if (dt.Rows.Count > 0)
            {
                rcbStatus.DataSource = dt;
                rcbStatus.DataValueField = "Value";
                rcbStatus.DataTextField = "ValueText";
                rcbStatus.DataBind();
                rcbStatus.Items.Insert(0, new RadComboBoxItem("--- ระบุสถานะการโทร ---", "0"));
                rcbStatus.SelectedIndex = 0;
                rcbStatus_SelectedIndexChanged(null, null);
            }
        }
        catch (Exception ex)
        {
            RadAjaxManager1.Alert(ex.Message);
        }
    }
    public void loadtalkscript()
    {
        try
        {
            string FullNameUser = "";
            string strSQL = " SELECT TOP(1) Fname+' '+Lname AS FullName FROM TblUser WHERE UserID = '" + lblUserID.Text.Trim() + "' ";
            DataTable dt = GE.SELECT_Executeion(strSQL);
            if (dt.Rows.Count > 0)
            {
                FullNameUser = dt.Rows[0]["FullName"].ToString();
            }

            lbltalkscript.Text = " <p> ผม / ดิฉัน <b>" + FullNameUser + "</b> เป็นพนักงานสัมภาษณ์จากบริษัท MOCAP  <br />"
                + "  ซึ่งได้รับมอบหมายจาก Volvo Cars ประเทศไทย ในการสัมภาษณ์เพื่อสอบถาม <br /> "
                + " และ Update ในระบบฐานข้อมูลของทาง Volvo ขอรบกวนเวลาสักครู่ สะดวกนะครับ / ค่ะ <br /> "
                ;
        }
        catch (Exception ex)
        {

        }
    }


    protected void BtnSaveCallStatus_Click(object sender, EventArgs e)
    {
        try
        {
            if (rcbStatus.SelectedIndex == 0)
            {
                RadAjaxManager1.Alert("กรุณาระบุสถานะการโทรให้ครบถ้วน !!");
                rcbStatus.Focus();
                return;
            }
            
            if (rcbStatus.SelectedValue == "2" && rcbSubStatus.SelectedValue == "2")
            {
                if (!CheckAppfrom(lblCusID.Text.Trim()))
                {
                    RadAjaxManager1.Alert("ไม่สำเร็จ กรุณากรอกข้อมูล <p style='blackground: green;'>แถบสีเขียว</p> ให้ครบถ้วน !!");
                    return;
                }

                if (rcbIsEdit.SelectedIndex < 0 )
                {
                    rcbIsEdit.Focus();
                    RadAjaxManager1.Alert("ไม่สำเร็จ กรุณากรอกข้อมูล สำเร็จ ให้ครบถ้วน !!");
                    return;
                }

                if (rcbIsReason.SelectedIndex < 0)
                {
                    rcbIsReason.Focus();
                    RadAjaxManager1.Alert("ไม่สำเร็จ กรุณากรอกข้อมูล สำเร็จ ให้ครบถ้วน !!");
                    return;
                }
            }


            if (UpdateCustomerByID(lblCusID.Text.Trim()))
            {
                UpdateCustomerMasterByID(lblCusID.Text.Trim());
                InserCallHistory();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "AlertMessage",
                      "alert('บันทึกข้อมูลเรียบร้อย'); window.location='" +
                      Request.ApplicationPath + "/ListAgent.aspx';", true);
            }
            else
            {
                RadAjaxManager1.Alert("ไม่สำเร็จ กรุณากดบันทึกอีกครั้ง !!");
                return;
            }
        }
        catch (Exception ex)
        {
            RadAjaxManager1.Alert(ex.Message);
        }
    }

    protected void BtnSave_Click(object sender, EventArgs e)
    {
        try
        {
            UpdateCustomerByID(lblCusID.Text.Trim());
            RadAjaxManager1.Alert("บันทึกข้อมูลเรียบร้อย");
        }
        catch (Exception ex)
        {
            RadAjaxManager1.Alert(ex.Message);
        }
    }

    private bool CheckAppfrom(string CusID)
    {
        try
        {

            if (TextBox5.Text.Trim() == "")
                return false;
            if (TextBox6.Text.Trim() == "")
                return false;

            //if (TextBox8.Text.Trim() == "")
            //    return false;
            //if (TextBox9.Text.Trim() == "")
            //    return false;
            //if (TextBox10.Text.Trim() == "")
            //    return false;


            if (txtCustoemrAddress1_1.Text.Trim() == "")
                return false;
            if (txtCustoemrAddress1_2.Text.Trim() == "")
                return false;
            if (txtCustoemrAddress1_3.Text.Trim() == "")
                return false;
            if (txtCustoemrAddress1_4.Text.Trim() == "")
                return false;
            if (txtCustoemrAddress1_5.Text.Trim() == "")
                return false;
            if (TextBox11.Text.Trim() == "")
                return false;
            if (TextBox13.Text.Trim() == "")
                return false;
            if (TextBox15.Text.Trim() == "")
                return false;
            if (TextBox16.Text.Trim() == "")
                return false;
            if (TextBox17.Text.Trim() == "")
                return false;
            if (TextBox18.Text.Trim() == "")
                return false;
            if (TextBox19.Text.Trim() == "")
                return false;
            if (TextBox20.Text.Trim() == "")
                return false;
            if (TextBox21.Text.Trim() == "")
                return false;
            if (TextBox24.Text.Trim() == "")
                return false;
            if (TextBox24.Text.Trim() == "อื่นๆ" && txt_rcb24_other.Text.Trim() == "")
                return false;
            if (TextBox25.Text.Trim() == "")
                return false;
            if (TextBox25.Text.Trim() == "อื่นๆ" && txt_rcb25_other.Text.Trim() == "")
                return false;
            if (TextBox26.Text.Trim() == "")
                return false;
            if (TextBox28.Text.Trim() == "")
                return false;
            if (TextBox29.Text.Trim() == "")
                return false;
            if (TextBox30.Text.Trim() == "")
                return false;

            if (TextBox31.Text.Trim() == "")
                return false;
            //if (TextBox32.Text.Trim() == "")
            //    return false;
            //if (TextBox33.Text.Trim() == "")
            //    return false;
            if (txtAddressCC1_1.Text.Trim() == "")
                return false;
            if (txtAddressCC1_2.Text.Trim() == "")
                return false;
            if (txtAddressCC1_3.Text.Trim() == "")
                return false;
            if (txtAddressCC1_4.Text.Trim() == "")
                return false;
            if (txtAddressCC1_5.Text.Trim() == "")
                return false;
            if (TextBox34.Text.Trim() == "")
                return false;
            if (TextBox35.Text.Trim() == "")
                return false;
            if (TextBox36.Text.Trim() == "")
                return false;
            if (TextBox37.Text.Trim() == "")
                return false;
            if (TextBox38.Text.Trim() == "")
                return false;
            if (TextBox39.Text.Trim() == "")
                return false;
            if (TextBox40.Text.Trim() == "")
                return false;


            if (TextBox41.Text.Trim() == "")
                return false;
            if (TextBox43.Text.Trim() == "")
                return false;
            if (TextBox44.Text.Trim() == "")
                return false;
            if (TextBox44.Text.Trim() == "อื่นๆ" && txt_rcb44_other.Text.Trim() == "")
                return false;
            if (TextBox45.Text.Trim() == "")
                return false;
            if (TextBox45.Text.Trim() == "อื่นๆ" && txt_rcb45_other.Text.Trim() == "")
                return false;
            if (TextBox46.Text.Trim() == "")
                return false;


            return true;
        }
        catch (Exception ex)
        {
            RadAjaxManager1.Alert(ex.Message);
            return false;
        }
    }
    private void UpdateCustomerMasterByID(string CusID)
    {
        try
        {

            string strSQL = " UPDATE TblCustomer_master SET "
                + " UpdateID = '" + lblUserID.Text.Trim() + "' "
                + " , UpdateDate = GETDATE() ";


            if (lblUserLavel.Text.Trim() == "T")
            {
                if (lblCntCall.Text.Trim() == "0")
                {
                    strSQL += ", FirstCallDate = GETDATE() ";
                }

                if (CheckAttemps(lblCusID.Text.Trim()))
                {
                    strSQL += ", StatusID = '" + rcbStatus.SelectedValue + "' "
                    + ", SubStatusID = '" + rcbSubStatus.SelectedValue + "' "
                    + ", Active = 'N' ";
                }
                else
                {
                    strSQL += ", StatusID = '" + rcbStatus.SelectedValue + "' "
                    + ", SubStatusID = '" + rcbSubStatus.SelectedValue + "' ";
                }

                strSQL += ", LastCallDate = GETDATE() "
                    + ", CntCall = CntCall+1 ";

                if (rcbStatus.SelectedValue == "2")
                {

                    strSQL += ", AppStatus = 2 "
                    + " ,IsEdit = '"+ rcbIsEdit.SelectedValue +"' "
                    + " ,IsReason = '" + rcbIsReason.SelectedValue + "' "
                    + " ,Reason = '" + rcbIsReason.SelectedItem.Text + "' "
                    + " ,ClosedDate = GETDATE() "
                    + " ,ClosedID = '" + lblUserID.Text.Trim() + "' ";
                }
            }


            if (lblUserLavel.Text.Trim() == "Q")
            {

            strSQL += ", QCID = '"+ lblUserID.Text.Trim() +"' "
                   + ", QCDate = GETDATE() "
                   + ", AppStatus = '" + rdoQCAppStatus.SelectedValue + "' ";
            }
            strSQL += ", comments = '" + txacomments.Text.Trim() + "' ";
            strSQL += " WHERE CusID = '" + CusID + "' ";

            GE.UPDATE_Executeion(strSQL);
        }
        catch (Exception ex)
        {
            RadAjaxManager1.Alert(ex.Message);
        }
    }
    private bool CheckAttemps(string CusID)
    {
        string strSQL = " SELECT COUNT(1) AS Cnt, SubStatusID  ";
        strSQL += " FROM TblCallHistory ";
        strSQL += " WHERE SubStatusID IN (7,8,9,10,11) AND CusID = '"+ CusID +"' ";
        strSQL += " GROUP BY SubStatusID ";
        strSQL += " HAVING COUNT(1) >= 3 ";
        DataTable dt = GE.SELECT_Executeion(strSQL);
        if (dt.Rows.Count > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    private void InserCallHistory()
    {
        string strSQL = " INSERT INTO TblCallHistory (CusID,StatusID, StatusName, SubStatusID, SubStatusName, CreateID, CreateDate, Active) "
                + " VALUES ( "
                + " " + lblCusID.Text.Trim() + " "
                + ", " + rcbStatus.SelectedValue + " "
                + ", '" + rcbStatus.SelectedItem.Text + "' "
                + ", " + rcbSubStatus.SelectedValue + " "
                + ", '" + rcbSubStatus.SelectedItem.Text + "' "
                + ", " + lblUserID.Text.Trim() + " "
                + ", GETDATE() "
                + ", 'Y') ";
        GE.INSERT_Executeion(strSQL);
    }

    private bool UpdateCustomerByID(string CusID)
    {
        try
        {
            string strSQL = " UPDATE TblCustomer SET "
                              + " UpdateID = '" + lblUserID.Text.Trim() + "' "
                              + " ,UpdateDate = GETDATE() "

                              + "  ,CustomerCode = '" + TextBox1.Text.Trim() + "' "
                              + "  ,CarID = '" + TextBox2.Text.Trim() + "' "
                              + "  ,CDBID = '" + TextBox3.Text.Trim() + "' "
                              + "  ,Tupe = '" + TextBox4.Text.Trim() + "' "
                              + "  ,Title = '" + TextBox5.Text.Trim() + "' "
                              + "  ,Name = '" + TextBox6.Text.Trim() + "' "
                              + "  ,BrNo = '" + TextBox7.Text.Trim() + "' "
                              + "  ,Address1 = '" + TextBox8.Text.Trim() + "' "
                              + "  ,Address2 = '" + TextBox9.Text.Trim() + "' "
                              + "  ,District = '" + TextBox10.Text.Trim() + "' "

                              + "  ,Zipcode = '" + TextBox11.Text.Trim() + "' "
                              + "  ,TaxID = '" + TextBox12.Text.Trim() + "' "
                              + "  ,CompleateAccount = '" + TextBox13.Text.Trim() + "' "
                              + "  ,HomePhone = '" + TextBox15.Text.Trim() + "' "
                              + "  ,OfficePhone = '" + TextBox16.Text.Trim() + "' "
                              + "  ,Mobile = '" + TextBox17.Text.Trim() + "' "
                              + "  ,Fax = '" + TextBox18.Text.Trim() + "' "
                              + "  ,Email = '" + TextBox19.Text.Trim() + "' "
                              + "  ,Gender = '" + TextBox20.Text.Trim() + "' "

                              + "  ,MaritalStatus = '" + TextBox21.Text.Trim() + "' "
                              + "  ,NumberOfChilden = '" + TextBox22.Text.Trim() + "' "
                              + "  ,Birthdate = '" + TextBox23.Text.Trim() + "' "
                              + "  ,Occupation = '" + TextBox24.Text.Trim() + "' "
                              + "  ,Hobby = '" + TextBox25.Text.Trim() + "' "
                              + "  ,Income = '" + TextBox26.Text.Trim() + "' "
                              + "  ,PerferredLanguage = '" + TextBox27.Text.Trim() + "' "
                              + "  ,ContactType = '" + TextBox28.Text.Trim() + "' "
                              + "  ,PreferContactChanel = '" + TextBox29.Text.Trim() + "' "
                              + "  ,ContactTitle = '" + TextBox30.Text.Trim() + "' "

                              + "  ,ContactName = '" + TextBox31.Text.Trim() + "' "
                              + "  ,ContactAddress1 = '" + TextBox32.Text.Trim() + "' "
                              + "  ,ContactAddress2 = '" + TextBox33.Text.Trim() + "' "
                              + "  ,ContactZipcode = '" + TextBox34.Text.Trim() + "' "
                              + "  ,Contact = '" + TextBox35.Text.Trim() + "' "
                              + "  ,ContactOfficePhone = '" + TextBox36.Text.Trim() + "' "
                              + "  ,ContactMobile = '" + TextBox37.Text.Trim() + "' "
                              + "  ,ContactFax = '" + TextBox38.Text.Trim() + "' "
                              + "  ,ContactEmail = '" + TextBox39.Text.Trim() + "' "
                              + "  ,ContactGender = '" + TextBox40.Text.Trim() + "' "

                              + "  ,ContactMaritalStatus = '" + TextBox41.Text.Trim() + "' "
                              + "  ,ContactNumberOfChilden = '" + TextBox42.Text.Trim() + "' "
                              + "  ,ContactBirthdate = '" + TextBox43.Text.Trim() + "' "

                              + "  ,ContactOccupation = '" + TextBox44.Text.Trim() + "' "
                              + "  ,ContactHobby = '" + TextBox45.Text.Trim() + "' "
                              + "  ,ContactIncome = '" + TextBox46.Text.Trim() + "' "
                               + "  ,BaranchOwner = '" + TextBox47.Text.Trim() + "' "
                              + "  ,VIN = '" + TextBox45.Text.Trim() + "' "
                              + "  ,License = '" + TextBox49.Text.Trim() + "' "
                              + "  ,CarVaiance = '" + TextBox50.Text.Trim() + "' "
                              + "  ,CarModel = '" + TextBox51.Text.Trim() + "' "
                              + "  ,CarMY = '" + TextBox52.Text.Trim() + "' "



                              + "  ,RetailDate = '" + TextBox53.Text.Trim() + "' "
                              + "  ,RetailBranch = '" + TextBox54.Text.Trim() + "' "
                              + "  ,LastServiceBr = '" + TextBox55.Text.Trim() + "' "
                              + "  ,LastServiceDate = '" + TextBox56.Text.Trim() + "' "
                              + "  ,SalesCodeorName = '" + TextBox57.Text.Trim() + "' "
                              + "  ,LastMileage = '" + TextBox58.Text.Trim() + "' "
                              + "  ,SACode = '" + TextBox59.Text.Trim() + "' "
                              + "  ,SAName = '" + TextBox60.Text.Trim() + "' "

                              + "  ,CarUsageType = '" + TextBox61.Text.Trim() + "' "
                              + "  ,Remarks = '" + TextBox62.Text.Trim() + "' ";



            //CustomerAddress
            #region
            if (rcbProvince10.Text == String.Empty)
                strSQL += " ,mCustomerProvinceID = null, mCustomerProvinceName = null ";
            else
                strSQL += " ,mCustomerProvinceID = '" + rcbProvince10.SelectedValue + "' ,mCustomerProvinceName = '" + rcbProvince10.SelectedItem.Text + "' ";

            if (rcbDistrict10.Text == String.Empty)
                strSQL += " ,mCustomerDistrictID = null ,mCustomerDistrictName = null ";
            else
                strSQL += " ,mCustomerDistrictID = '" + rcbDistrict10.SelectedValue + "' ,mCustomerDistrictName = '" + rcbDistrict10.SelectedItem.Text + "' ";

            if (rcbSubDistrict10.Text == String.Empty)
                strSQL += " ,mCustomerSubDistID = null ,mCustomerSubDisName = null ";
            else
                strSQL += " ,mCustomerSubDistID = '" + rcbSubDistrict10.SelectedValue + "' ,mCustomerSubDisName = '" + rcbSubDistrict10.SelectedItem.Text + "' ";

            strSQL += " ,mCustomerZipcode = '" + TextBox11.Text.Trim() + "' ";
            strSQL += " ,mCustomerHNumber = '" + txtCustoemrAddress1_1.Text.Trim() + "' ";
            strSQL += " ,mCustomerHV = '" + txtCustoemrAddress1_2.Text.Trim() + "' ";
            strSQL += " ,mCustomerHM = '" + txtCustoemrAddress1_3.Text.Trim() + "' ";
            strSQL += " ,mCustomerSoi = '" + txtCustoemrAddress1_4.Text.Trim() + "' ";
            strSQL += " ,mCustomerRoad = '" + txtCustoemrAddress1_5.Text.Trim() + "' ";
            #endregion

            //ContactAddress
            #region
            if (rcbProvince33.Text == String.Empty)
                strSQL += " ,mCTProvinceID = null, mCTProvinceName = null ";
            else
                strSQL += " ,mCTProvinceID = '" + rcbProvince33.SelectedValue + "' ,mCTProvinceName = '" + rcbProvince33.SelectedItem.Text + "' ";

            if (rcbDistrict33.Text == String.Empty)
                strSQL += " ,mCTDistrictID = null ,mCTDistrictName = null ";
            else
                strSQL += " ,mCTDistrictID = '" + rcbDistrict33.SelectedValue + "' ,mCTDistrictName = '" + rcbDistrict33.SelectedItem.Text + "' ";

            if (rcbSubDistrict33.Text == String.Empty)
                strSQL += " ,mCTSubDistID = null ,mSubDisName = null ";
            else
                strSQL += " ,mCTSubDistID = '" + rcbSubDistrict33.SelectedValue + "' ,mSubDisName = '" + rcbSubDistrict33.SelectedItem.Text + "' ";

            strSQL += " ,mCTZipcode = '" + TextBox34.Text.Trim() + "' ";
            strSQL += " ,mCTHNumber = '" + txtAddressCC1_1.Text.Trim() + "' ";
            strSQL += " ,mCTHV = '" + txtAddressCC1_2.Text.Trim() + "' ";
            strSQL += " ,mCTHM = '" + txtAddressCC1_3.Text.Trim() + "' ";
            strSQL += " ,mCTSoi = '" + txtAddressCC1_4.Text.Trim() + "' ";
            strSQL += " ,mCTRoad = '" + txtAddressCC1_5.Text.Trim() + "' ";
            #endregion

            //Moreadd
            #region
            strSQL += " ,mOccupationOther = '" + txt_rcb24_other.Text.Trim() + "' "
                + " ,mHobbyOther = '" + txt_rcb25_other.Text.Trim() + "'"
                + " ,mContactOccupationOther = '" + txt_rcb44_other.Text.Trim() + "'"
                + " ,mContactHobbyOther = '" + txt_rcb45_other.Text.Trim() + "'"
                ;
            #endregion

            strSQL += "  WHERE ReferCusID = '" + CusID + "' ";

                            GE.UPDATE_Executeion(strSQL);

            return true;
        }
        catch (Exception ex)
        {
            RadAjaxManager1.Alert(ex.Message);
            return false;
        }
    }
    protected void rcbStatus_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        try
        {
            string strSQL = " SELECT SubStatusID AS Value, SubStatusNameTH AS ValueText "
                          + " FROM TblSubStatus WHERE Active = 'Y' AND StatusID = '" + rcbStatus.SelectedValue + "' ";
            DataTable dt = GE.SELECT_Executeion(strSQL);
            if (dt.Rows.Count > 0)
            {
                rcbSubStatus.DataSource = dt;
                rcbSubStatus.DataValueField = "Value";
                rcbSubStatus.DataTextField = "ValueText";
                rcbSubStatus.DataBind();
                rcbSubStatus.SelectedIndex = 0;
            }
            else
            {
                rcbSubStatus.Items.Clear();
                rcbSubStatus.ClearSelection();
                rcbSubStatus.Items.Insert(0, new RadComboBoxItem("--- ระบุสถานะการโทร ---", "0"));
                rcbSubStatus.SelectedIndex = 0;
            }

        }
        catch (Exception ex)
        {
            RadAjaxManager1.Alert(ex.Message);
        }
    }
    protected void rcb5_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        TextBox5.Text = rcb5.SelectedValue;
    }
    protected void rcb24_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        TextBox24.Text = rcb24.SelectedValue;
        if (rcb24.SelectedValue == "อื่นๆ")
            txt_rcb24_other.Visible = true;
        else
            txt_rcb24_other.Visible = false; txt_rcb24_other.Text = "";
    }
    protected void rcb25_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        TextBox25.Text = rcb25.SelectedValue;
        if (rcb25.SelectedValue == "อื่นๆ")
            txt_rcb25_other.Visible = true;
        else
            txt_rcb25_other.Visible = false; txt_rcb25_other.Text = "";
    }
    protected void rcb26_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        TextBox26.Text = rcb26.SelectedValue;
    }
    protected void rcb13_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        TextBox13.Text = rcb13.SelectedValue;
    }
    protected void rcb20_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        TextBox20.Text = rcb20.SelectedValue;
    }
    protected void rcb30_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        TextBox30.Text = rcb30.SelectedValue;
    }
    protected void rcb40_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        TextBox40.Text = rcb40.SelectedValue;
    }
    protected void rcbProvince10_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        try
        {
            string strSQL = "SELECT AMPHUR_ID AS Value, AMPHUR_NAME AS ValueText FROM amphures "
                       + " WHERE PROVINCE_ID = '" + rcbProvince10.SelectedValue + "'";
            DataTable dt = GE.SELECT_Executeion(strSQL);
            if (dt.Rows.Count > 0)
            {
                rcbDistrict10.DataSource = dt;
                rcbDistrict10.DataValueField = "Value";
                rcbDistrict10.DataTextField = "ValueText";
                rcbDistrict10.DataBind();
                rcbDistrict10.SelectedIndex = 0;
                rcbDistrict10_SelectedIndexChanged(null, null);
            }
        }
        catch (Exception ex)
        {
            RadAjaxManager1.Alert(ex.Message);
        }
    }
    protected void rcbDistrict10_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        try
        {

            string strSQL = "SELECT DISTRICT_ID AS Value, DISTRICT_NAME AS ValueText FROM districts "
                       + " WHERE PROVINCE_ID = '" + rcbProvince10.SelectedValue + "' AND AMPHUR_ID = '" + rcbDistrict10.SelectedValue + "' ";
            DataTable dt = GE.SELECT_Executeion(strSQL);
            if (dt.Rows.Count > 0)
            {
                rcbSubDistrict10.DataSource = dt;
                rcbSubDistrict10.DataValueField = "Value";
                rcbSubDistrict10.DataTextField = "ValueText";
                rcbSubDistrict10.DataBind();
                rcbSubDistrict10.SelectedIndex = 0;
                rcbSubDistrict10_SelectedIndexChanged(null, null);

            }
        }
        catch (Exception ex)
        {
            RadAjaxManager1.Alert(ex.Message);
        }
    }
    protected void rcbSubDistrict10_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        TextBox10.Text = GetDistName(rcbSubDistrict10.SelectedValue);
        TextBox11.Text = GetZipCode(rcbSubDistrict10.SelectedValue);
    }
    private string GetDistName(string DistrictID)
    {

        string DisName = "";
        string strSQL = " SELECT TOP(1) DISTRICT_NAME FROM districts WHERE DISTRICT_ID = '" + DistrictID + "' ";
        DataTable dt = GE.SELECT_Executeion(strSQL);
        if (dt != null)
        {
            if (dt.Rows.Count > 0)
            {
                DisName = dt.Rows[0]["DISTRICT_NAME"].ToString();
            }
        }
        return DisName;
    }
    private string GetZipCode(string DistrictID)
    {

        string ZipCode = "";
        string strSQL = " SELECT TOP(1) zipcode FROM zipCode  "
                    + " WHERE district_code IN (SELECT TOP(1) DISTRICT_CODE FROM districts WHERE DISTRICT_ID = '" + DistrictID + "') ";
        DataTable dt = GE.SELECT_Executeion(strSQL);
        if (dt != null)
        {
            if (dt.Rows.Count > 0)
            {
                ZipCode = dt.Rows[0]["ZipCode"].ToString();
            }
        }
        return ZipCode;
    }

    protected void rcbProvince33_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        try
        {
            string strSQL = "SELECT AMPHUR_ID AS Value, AMPHUR_NAME AS ValueText FROM amphures "
                       + " WHERE PROVINCE_ID = '" + rcbProvince33.SelectedValue + "'";
            DataTable dt = GE.SELECT_Executeion(strSQL);
            if (dt.Rows.Count > 0)
            {
                rcbDistrict33.DataSource = dt;
                rcbDistrict33.DataValueField = "Value";
                rcbDistrict33.DataTextField = "ValueText";
                rcbDistrict33.DataBind();
                rcbDistrict33.SelectedIndex = 0;
                rcbDistrict33_SelectedIndexChanged(null, null);
            }
        }
        catch (Exception ex)
        {
            RadAjaxManager1.Alert(ex.Message);
        }
    }
    protected void rcbDistrict33_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        try
        {

            string strSQL = "SELECT DISTRICT_ID AS Value, DISTRICT_NAME AS ValueText FROM districts "
                       + " WHERE PROVINCE_ID = '" + rcbProvince33.SelectedValue + "' AND AMPHUR_ID = '" + rcbDistrict33.SelectedValue + "' ";
            DataTable dt = GE.SELECT_Executeion(strSQL);
            if (dt.Rows.Count > 0)
            {
                rcbSubDistrict33.DataSource = dt;
                rcbSubDistrict33.DataValueField = "Value";
                rcbSubDistrict33.DataTextField = "ValueText";
                rcbSubDistrict33.DataBind();
                rcbSubDistrict33.SelectedIndex = 0;
                rcbSubDistrict33_SelectedIndexChanged(null, null);

            }
        }
        catch (Exception ex)
        {
            RadAjaxManager1.Alert(ex.Message);
        }
    }
    protected void rcbSubDistrict33_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        TextBox34.Text = GetZipCode(rcbSubDistrict33.SelectedValue);
    }
    protected void rcb21_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        TextBox21.Text = rcb21.SelectedValue;
    }
    protected void rcb41_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        TextBox41.Text = rcb41.SelectedValue;
    }
    protected void rcb4_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        loadtupegroup(rcb4.SelectedValue);
        TextBox4.Text = rcb4.SelectedValue;
    }
    protected void rcbIsEdit_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        //if (rcbIsEdit.SelectedValue == "N")
        //{
        //    rcbIsReason.SelectedIndex = 0;
        //}
        //else
        //{
        //    rcbIsReason.SelectedIndex = 1;
        //}
    }
    protected void rcb46_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        TextBox46.Text = rcb46.SelectedValue;
    }
    protected void rcb45_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        TextBox45.Text = rcb45.SelectedValue;
        if (rcb45.SelectedValue == "อื่นๆ")
            txt_rcb45_other.Visible = true;
        else
            txt_rcb45_other.Visible = false; txt_rcb45_other.Text = "";
    }
    protected void rcb44_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        TextBox44.Text = rcb44.SelectedValue;
        if (rcb44.SelectedValue == "อื่นๆ")
            txt_rcb44_other.Visible = true;
        else
            txt_rcb44_other.Visible = false; txt_rcb44_other.Text = "";
    }
}
