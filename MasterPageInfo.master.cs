﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

public partial class MasterPageInfo : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserID"] == null || Session["UserLevelID"] == null)
        {
            Response.Redirect("~/Default.aspx");
        }

        Session.Timeout = 120;
        lblUserLevelID.Text = Session["UserLevelID"].ToString();
        lblUserID.Text = Session["UserID"].ToString();

        if (lblUserLevelID.Text.Trim() == "T")
        {
            liHomeSup.Style.Add("display", "none");
            //liImport.Style.Add("display", "none");
            liTranferlist.Style.Add("display", "none");
            liUserManagement.Style.Add("display", "none");
            liMenuReport.Style.Add("display", "none");
            
        }
        else if (lblUserLevelID.Text.Trim() == "S")
        {
            liHomeTsr.Style.Add("display", "none");
        }
        else if (lblUserLevelID.Text.Trim() == "Q")
        {
            liHomeTsr.Style.Add("display", "none");
            //liImport.Style.Add("display", "none");
            liTranferlist.Style.Add("display", "none");
            liUserManagement.Style.Add("display", "none");
            liMenuReport.Style.Add("display", "none");
        }
    }
}
