﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageInfo.master" AutoEventWireup="true" CodeFile="ImportList.aspx.cs" Inherits="ImportList" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .btnGreen {
            width: 90%;
            background-color: #1CB94E;
            border: none;
            outline: none;
            color: #fff;
            font-size: 14px;
            font-weight: normal;
            padding: 14px 0;
            border-radius: 2px;
            text-transform: uppercase;
        }

        .btnBlue {
            width: 94%;
            background-color: #3C59A8;
            border: none;
            outline: none;
            color: #fff;
            font-size: 14px;
            font-weight: normal;
            padding: 14px 0;
            border-radius: 2px;
            text-transform: uppercase;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Label ID="lblUserID" runat="server" Visible="false" Text="Label"></asp:Label>
    <asp:Label ID="lblUserLevelID" runat="server" Visible="false" Text="Label"></asp:Label>
    <asp:Label ID="lblDataSourceID" runat="server" Visible="false" Text="Label"></asp:Label>
    <h1>นำเข้ารายชื่อ</h1>

    <asp:Panel ID="PanelImport" runat="server">
        <td valign="top">Excel File :<telerik:RadUpload ID="RadUpload1"
            runat="server"
            InitialFileInputsCount="1"
            ControlObjectsVisibility="None"
            InputSize="32" Width="90%" />
            <asp:Button ID="buttonSubmitUpload"
                CssClass="btnGreen"
                runat="server"
                ValidationGroup="1"
                Text="Start Upload"
                OnClick="buttonSubmitUpload_Click" />
            <asp:CustomValidator
                ID="Customvalidator1"
                runat="server"
                Display="Dynamic"
                ValidationGroup="1"
                ClientValidationFunction="validateRadUpload1" ForeColor="Red"
                Font-Size="11pt">
                    <span style="FONT-SIZE: 13px;">Invalid extensions .xls</span>
            </asp:CustomValidator>
            <br />
            <asp:Label ID="lblTotalReccord" runat="server" Text="-" Font-Bold="True" Font-Size="Larger" ForeColor="#3333FF"></asp:Label>
        </td>
    </asp:Panel>
    <asp:Panel ID="PanelList" runat="server">
        <td>
            <telerik:RadGrid
                ID="RadGrid1"
                runat="server"
                Width="94%"
                Height="500px"
                AutoGenerateColumns="false"
                GridLines="None"
                BorderWidth="1px"
                Style="outline: none"
                BorderStyle="Solid">
                <PagerStyle Mode="NextPrevAndNumeric" />
                <MasterTableView
                    PageSize="20"
                    NoMasterRecordsText="&lt; ไม่มีข้อมูล &gt;"
                    CommandItemDisplay="none"
                    AllowMultiColumnSorting="True"
                    EnableHeaderContextMenu="true">
                    <CommandItemTemplate>
                        <table width="100%">
                            <tr>
                                <td>test button and set CommandItemDisplay="Top"
                                </td>
                            </tr>
                        </table>
                    </CommandItemTemplate>
                    <Columns>

                        <%--<telerik:GridClientSelectColumn UniqueName="ClientSelectColumn">
                                                <HeaderStyle Width="27px" HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </telerik:GridClientSelectColumn>--%>
                        <telerik:GridTemplateColumn AllowFiltering="false"
                            HeaderStyle-HorizontalAlign="Center" HeaderText="No."
                            ItemStyle-HorizontalAlign="Center" ShowFilterIcon="false"
                            UniqueName="TemplateColumn">
                            <ItemTemplate>
                                <%#Container.DataSetIndex+1%>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Width="40px" />
                            <ItemStyle HorizontalAlign="Center" Width="40px" />
                        </telerik:GridTemplateColumn>

                        <telerik:GridBoundColumn AutoPostBackOnFilter="true"
                            CurrentFilterFunction="Contains"
                            ShowFilterIcon="false"
                            FilterControlWidth="100%"
                            HeaderText="เบอร์โทร1"
                            DataField="cus_1"
                            UniqueName="cus_1">
                            <HeaderStyle HorizontalAlign="Center" Width="100px" />
                            <ItemStyle HorizontalAlign="Center" Wrap="False" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn AutoPostBackOnFilter="true"
                            CurrentFilterFunction="Contains"
                            ShowFilterIcon="false"
                            FilterControlWidth="100%"
                            HeaderText="เบอร์โทร2"
                            DataField="cus_2"
                            UniqueName="cus_2">
                            <HeaderStyle HorizontalAlign="Center" Width="100px" />
                            <ItemStyle HorizontalAlign="Center" Wrap="False" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn AutoPostBackOnFilter="true"
                            CurrentFilterFunction="Contains"
                            ShowFilterIcon="false"
                            FilterControlWidth="100%"
                            HeaderText="ชื่อสินค้า"
                            DataField="cus_3"
                            UniqueName="cus_3">
                            <HeaderStyle HorizontalAlign="Center" Width="100px" />
                            <ItemStyle HorizontalAlign="Center" Wrap="False" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn AutoPostBackOnFilter="true"
                            CurrentFilterFunction="Contains"
                            ShowFilterIcon="false"
                            FilterControlWidth="100%"
                            HeaderText="หัวข้อ"
                            DataField="cus_4"
                            UniqueName="cus_4">
                            <HeaderStyle HorizontalAlign="Center" Width="100px" />
                            <ItemStyle HorizontalAlign="Center" Wrap="False" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn AutoPostBackOnFilter="true"
                            CurrentFilterFunction="Contains"
                            ShowFilterIcon="false"
                            FilterControlWidth="100%"
                            HeaderText="วันที่โพส"
                            DataField="cus_5"
                            UniqueName="cus_5">
                            <HeaderStyle HorizontalAlign="Center" Width="100px" />
                            <ItemStyle HorizontalAlign="Center" Wrap="False" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn AutoPostBackOnFilter="true"
                            CurrentFilterFunction="Contains"
                            ShowFilterIcon="false"
                            FilterControlWidth="100%"
                            HeaderText="Url"
                            DataField="cus_6"
                            UniqueName="cus_6">
                            <HeaderStyle HorizontalAlign="Center" Width="100px" />
                            <ItemStyle HorizontalAlign="Center" Wrap="False" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn AutoPostBackOnFilter="true"
                            CurrentFilterFunction="Contains"
                            ShowFilterIcon="false"
                            FilterControlWidth="100%"
                            HeaderText="ราคาสินค้า"
                            DataField="cus_7"
                            UniqueName="cus_7">
                            <HeaderStyle HorizontalAlign="Center" Width="100px" />
                            <ItemStyle HorizontalAlign="Center" Wrap="False" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn AutoPostBackOnFilter="true"
                            CurrentFilterFunction="Contains"
                            ShowFilterIcon="false"
                            FilterControlWidth="100%"
                            HeaderText="UserName"
                            DataField="cus_8"
                            UniqueName="cus_8">
                            <HeaderStyle HorizontalAlign="Center" Width="100px" />
                            <ItemStyle HorizontalAlign="Center" Wrap="False" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn AutoPostBackOnFilter="true"
                            CurrentFilterFunction="Contains"
                            ShowFilterIcon="false"
                            FilterControlWidth="100%"
                            HeaderText="Province"
                            DataField="cus_9"
                            UniqueName="cus_9">
                            <HeaderStyle HorizontalAlign="Center" Width="100px" />
                            <ItemStyle HorizontalAlign="Center" Wrap="False" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn AutoPostBackOnFilter="true"
                            CurrentFilterFunction="Contains"
                            ShowFilterIcon="false"
                            FilterControlWidth="100%"
                            HeaderText="WebName"
                            DataField="cus_10"
                            UniqueName="cus_10">
                            <HeaderStyle HorizontalAlign="Center" Width="100px" />
                            <ItemStyle HorizontalAlign="Center" Wrap="False" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn AutoPostBackOnFilter="true"
                            CurrentFilterFunction="Contains"
                            ShowFilterIcon="false"
                            FilterControlWidth="100%"
                            HeaderText="Date"
                            DataField="cus_11"
                            UniqueName="cus_11">
                            <HeaderStyle HorizontalAlign="Center" Width="100px" />
                            <ItemStyle HorizontalAlign="Center" Wrap="False" />
                        </telerik:GridBoundColumn>
                    </Columns>
                </MasterTableView>

                <ClientSettings
                    AllowColumnsReorder="true"
                    AllowColumnHide="True"
                    EnableRowHoverStyle="True"
                    ReorderColumnsOnClient="true">
                    <Selecting AllowRowSelect="True" />
                    <Resizing AllowColumnResize="true" />
                    <Scrolling AllowScroll="True" UseStaticHeaders="false" SaveScrollPosition="True"></Scrolling>
                </ClientSettings>
            </telerik:RadGrid>
            <br />
            <br />
            <asp:Button ID="BtnConfirm" Visible="false"
                OnClick="BtnConfirm_Click"
                CssClass="btnBlue"
                runat="server"
                Text="ยืนยันการบันนำเข้า" />
        </td>
    </asp:Panel>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" EnableAJAX="true" EnablePageHeadUpdate="false" EnableViewState="false">
    </telerik:RadAjaxManager>
</asp:Content>

