﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Volvo outbound || Login</title>
    <link rel="shortcut icon" href="logo.ico" />
    <script type="text/javascript">
        function Confirm() {
            var confirm_value = document.createElement("INPUT");
            confirm_value.type = "hidden";
            confirm_value.name = "confirm_value";
            if (confirm("Do you want to save data?")) {
                confirm_value.value = "Yes";
            } else {
                confirm_value.value = "No";
            }
            document.forms[0].appendChild(confirm_value);
        }
    </script>
    <style>
        .btnLogincl {
            -webkit-appearance: none;
            -moz-appearance: none;
            appearance: none;
            outline: 0;
            background-color: white;
            border: 0;
            padding: 15px 15px;
            color: #924b0c;
            border-radius: 3px;
            width: 400px;
            cursor: pointer;
            font-size: 20px;
            -webkit-transition-duration: 0.25s;
            transition-duration: 0.25s;
        }

            .btnLogincl:hover {
                background-color: #f5f7f9;
            }

        @import url(http://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300);

        * {
            box-sizing: border-box;
            margin: 0;
            padding: 0;
        }

        body {
            font-family: 'Source Sans Pro', sans-serif;
            color: white;
            font-size: 20px;
        }

        p {
            text-align: center;
            margin-bottom: 20px;
        }

            body ::-webkit-input-placeholder {
                /* WebKit browsers */
                font-family: 'Source Sans Pro', sans-serif;
                color: white;
                font-weight: 300;
            }

            body :-moz-placeholder {
                /* Mozilla Firefox 4 to 18 */
                font-family: 'Source Sans Pro', sans-serif;
                color: white;
                opacity: 1;
                font-weight: 300;
            }

            body ::-moz-placeholder {
                /* Mozilla Firefox 19+ */
                font-family: 'Source Sans Pro', sans-serif;
                color: white;
                opacity: 1;
                font-weight: 300;
            }

            body :-ms-input-placeholder {
                /* Internet Explorer 10+ */
                font-family: 'Source Sans Pro', sans-serif;
                color: white;
                font-weight: 300;
            }

        .wrapper {
            background: url(images/bg/bg2.png) no-repeat scroll center 0px !important;
            position: absolute;
            left: 0;
            width: 100%;
            height: 100%;
            overflow: hidden;
        }

            .wrapper.form-success .container h1 {
                -webkit-transform: translateY(85px);
                -ms-transform: translateY(85px);
                transform: translateY(85px);
            }

        .container {
            max-width: 600px;
            margin: 0 auto;
            padding: 80px 0;
            height: 500px;
            text-align: center;
        }

            .container h1 {
                font-size: 40px;
                -webkit-transition-duration: 1s;
                transition-duration: 1s;
                -webkit-transition-timing-function: ease-in-put;
                transition-timing-function: ease-in-put;
                font-weight: 200;
            }

        form {
            padding: 20px 0;
            position: relative;
            z-index: 2;
        }

            form input {
                -webkit-appearance: none;
                -moz-appearance: none;
                outline: 0;
                border: 1px solid rgba(255, 255, 255, 0.4);
                background-color: rgba(255, 255, 255, 0.2);
                width: 400px;
                border-radius: 3px;
                padding: 15px 15px;
                margin: 0 auto 10px auto;
                display: block;
                text-align: center;
                font-size: 20px;
                color: white;
                -webkit-transition-duration: 0.25s;
                transition-duration: 0.25s;
                font-weight: 300;
            }

                form input:hover {
                    background-color: rgba(255, 255, 255, 0.4);
                }

                form input:focus {
                    background-color: white;
                    width: 450px;
                    color: #924b0c;
                }


        .bg-bubbles {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            z-index: 1;
        }

            .bg-bubbles li {
                position: absolute;
                list-style: none;
                display: block;
                width: 40px;
                height: 40px;
                background-color: rgba(255, 255, 255, 0.15);
                bottom: -160px;
                -webkit-animation: square 25s infinite;
                animation: square 25s infinite;
                -webkit-transition-timing-function: linear;
                transition-timing-function: linear;
            }

                .bg-bubbles li:nth-child(1) {
                    left: 10%;
                }

                .bg-bubbles li:nth-child(2) {
                    left: 20%;
                    width: 80px;
                    height: 80px;
                    -webkit-animation-delay: 2s;
                    animation-delay: 2s;
                    -webkit-animation-duration: 17s;
                    animation-duration: 17s;
                }

                .bg-bubbles li:nth-child(3) {
                    left: 25%;
                    -webkit-animation-delay: 4s;
                    animation-delay: 4s;
                }

                .bg-bubbles li:nth-child(4) {
                    left: 40%;
                    width: 60px;
                    height: 60px;
                    -webkit-animation-duration: 22s;
                    animation-duration: 22s;
                    background-color: rgba(255, 255, 255, 0.25);
                }

                .bg-bubbles li:nth-child(5) {
                    left: 70%;
                }

                .bg-bubbles li:nth-child(6) {
                    left: 80%;
                    width: 120px;
                    height: 120px;
                    -webkit-animation-delay: 3s;
                    animation-delay: 3s;
                    background-color: rgba(255, 255, 255, 0.2);
                }

                .bg-bubbles li:nth-child(7) {
                    left: 32%;
                    width: 160px;
                    height: 160px;
                    -webkit-animation-delay: 7s;
                    animation-delay: 7s;
                }

                .bg-bubbles li:nth-child(8) {
                    left: 55%;
                    width: 20px;
                    height: 20px;
                    -webkit-animation-delay: 15s;
                    animation-delay: 15s;
                    -webkit-animation-duration: 40s;
                    animation-duration: 40s;
                }

                .bg-bubbles li:nth-child(9) {
                    left: 25%;
                    width: 10px;
                    height: 10px;
                    -webkit-animation-delay: 2s;
                    animation-delay: 2s;
                    -webkit-animation-duration: 40s;
                    animation-duration: 40s;
                    background-color: rgba(255, 255, 255, 0.3);
                }

                .bg-bubbles li:nth-child(10) {
                    left: 90%;
                    width: 160px;
                    height: 160px;
                    -webkit-animation-delay: 11s;
                    animation-delay: 11s;
                }

        @-webkit-keyframes square {
            0% {
                -webkit-transform: translateY(0);
                transform: translateY(0);
            }

            100% {
                -webkit-transform: translateY(-700px) rotate(600deg);
                transform: translateY(-700px) rotate(600deg);
            }
        }

        @keyframes square {
            0% {
                -webkit-transform: translateY(0);
                transform: translateY(0);
            }

            100% {
                -webkit-transform: translateY(-700px) rotate(600deg);
                transform: translateY(-700px) rotate(600deg);
            }
        }
    </style>
</head>
<body>
    <div class="wrapper">
        <div class="container">
            <h1>
                <%-- <img src="images/logo/shell.png" width="120" height="100">  --%>
                <img src="images/logo/volvo.png" height="200">
                <%--<img src="images/logo/mocap.png" width="200" height="100">--%>
            </h1>
            <form class="form" runat="server">
                <p>
                Your IP Address:
                <asp:Label ID="lblIPAddress" runat="server" Text="Label"></asp:Label>
                    </p>
                <telerik:RadScriptManager ID="RadScriptManager1" runat="server"></telerik:RadScriptManager>
                <asp:TextBox ID="txtUserName" runat="server" placeholder="Username"></asp:TextBox>
                <asp:TextBox ID="txtPassword" runat="server" placeholder="Password" TextMode="Password"></asp:TextBox>
                <asp:Button ID="BtnLogin" runat="server" CssClass="btnLogincl" OnClick="BtnLogin_Click" Text="Sing In" />
                <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Simple"></telerik:RadAjaxLoadingPanel>
                <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" EnableAJAX="true" EnablePageHeadUpdate="false" EnableViewState="false">
                    <AjaxSettings>
                        <telerik:AjaxSetting AjaxControlID="BtnLogin">
                            <UpdatedControls>
                                <telerik:AjaxUpdatedControl ControlID="txtUserName" LoadingPanelID="RadAjaxLoadingPanel1"></telerik:AjaxUpdatedControl>
                                <telerik:AjaxUpdatedControl ControlID="txtPassword" LoadingPanelID="RadAjaxLoadingPanel1"></telerik:AjaxUpdatedControl>
                            </UpdatedControls>
                        </telerik:AjaxSetting>
                    </AjaxSettings>
                </telerik:RadAjaxManager>
            </form>
        </div>
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <p>Volvo Outbound Application v1.0 by Watsphon Chanchring (9007)</p>
        <p>&copy; 2016 Web Application All Right Reserved. | Develop & Design by MOCAP Limited.</p>
        <ul class="bg-bubbles">
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
        </ul>

    </div>
</body>
</html>
