﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using Excel = Microsoft.Office.Interop.Excel;
using System.Configuration;
using System.Text;
using Telerik.Web.UI;

public partial class ReportCallSummary : System.Web.UI.Page
{
    GetExecution GE = new GetExecution();
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!this.IsPostBack)
            {
                if (Session["UserID"] != null)
                {
                    ScriptManager.GetCurrent(this.Page).RegisterPostBackControl(BtnFindReport);
                    lblUserID.Text = Session["UserID"].ToString();
                    lblUserLevelID.Text = Session["UserLevelID"].ToString();
                    rdpDateS.SelectedDate = DateTime.Today;
                    rdpDateE.SelectedDate = DateTime.Today;
                }
                else
                {
                    Response.Redirect("~/Default.aspx");
                }
            }

        }
        catch (Exception ex)
        {
            RadAjaxManager1.Alert(ex.Message);
        }
    }


    protected void BtnFindReport_Click(object sender, EventArgs e)
    {
        try
        {
            #region
            string[] arrDateS = rdpDateS.DateInput.SelectedDate.Value.ToShortDateString().Split('/');
            string[] arrDateE = rdpDateE.DateInput.SelectedDate.Value.ToShortDateString().Split('/');
            //'2014-12-03 00:00:00'
            string dateS = "";
            string dateE = "";

            string chkMounthS = arrDateS[1];
            string chkDayS = arrDateS[0];
            string chkMounthE = arrDateE[1];
            string chkDayE = arrDateE[0];

            string GetmounthS = "";
            string GetdayS = "";
            string GetmounthE = "";
            string GetdayE = "";


            if (chkMounthS.Length < 2)
            {
                GetmounthS = "0" + arrDateS[1];
            }
            else
            {
                GetmounthS = arrDateS[1];
            }

            if (chkDayS.Length < 2)
            {
                GetdayS = "0" + arrDateS[0];
            }
            else
            {
                GetdayS = arrDateS[0];
            }

            if (chkMounthE.Length < 2)
            {
                GetmounthE = "0" + arrDateE[1];
            }
            else
            {
                GetmounthE = arrDateE[1];
            }

            if (chkDayE.Length < 2)
            {
                GetdayE = "0" + arrDateE[0];
            }
            else
            {
                GetdayE = arrDateE[0];
            }

            dateS = arrDateS[2] + "-" + GetmounthS + "-" + GetdayS + " 00:00:00";
            dateE = arrDateE[2] + "-" + GetmounthE + "-" + GetdayE + " 23:59:59";
            #endregion

            //RadAjaxManager1.Alert(dateS + "<>" + dateE);
            DataTable dt = GetData(dateS, dateE);

            //**** เปลี่ยนออกแบบ Excel แทน
            if (dt.Rows.Count > 0)
            {
                ExportToExcelTemplate(dt);
            }
            else
            {
                RadAjaxManager1.Alert("ไม่มีข้อมูลออกรายงาน");
            }
        }
        catch (Exception ex)
        {
            RadAjaxManager1.Alert(ex.Message);
        }
    }

    private DataTable GetData(string DateStart, string DateEnd)
    {
        try
        {
            string strSQL = " SELECT CONVERT(VARCHAR(10),C.LastCallDate,120) AS CallDate  "
                + ", (SELECT COUNT(1) FROM TblCustomer CU WHERE CONVERT(VARCHAR(10),CU.LastCallDate,120) = CONVERT(VARCHAR(10),C.LastCallDate,120) AND CntCall = 1) AS NewListUse "
                + ", (SELECT COUNT(1) FROM TblCustomer CU WHERE CONVERT(VARCHAR(10),CU.LastCallDate,120) = CONVERT(VARCHAR(10),C.LastCallDate,120)) AS TotalUse "              
                //Success
                + ", (SELECT COUNT(1) FROM TblCustomer CU WHERE CONVERT(VARCHAR(10),CU.LastCallDate,120) = CONVERT(VARCHAR(10),C.LastCallDate,120) AND CU.StatusID = 2 AND CU.SubStatusID = 2) AS Success1 "
                + ", ROUND(CAST(((SELECT COUNT(1) FROM TblCustomer CU WHERE CONVERT(VARCHAR(10),CU.LastCallDate,120) = CONVERT(VARCHAR(10),C.LastCallDate,120) AND CU.StatusID = 2 AND CU.SubStatusID = 2) * 100) AS FLOAT)/CAST((SELECT COUNT(1) FROM TblCustomer CU WHERE CONVERT(VARCHAR(10),CU.LastCallDate,120) = CONVERT(VARCHAR(10),C.LastCallDate,120) AND CU.StatusID IN(2,3,4,5)) AS FLOAT),2) AS PSuccess1 "
                //Refuse
                + " , (SELECT COUNT(1) FROM TblCustomer CU WHERE CONVERT(VARCHAR(10),CU.LastCallDate,120) = CONVERT(VARCHAR(10),C.LastCallDate,120) AND CU.StatusID = 3 AND CU.SubStatusID = 3) AS Refuse1 "
                + ", ROUND(CAST(((SELECT COUNT(1) FROM TblCustomer CU WHERE CONVERT(VARCHAR(10),CU.LastCallDate,120) = CONVERT(VARCHAR(10),C.LastCallDate,120) AND CU.StatusID = 3 AND CU.SubStatusID = 3) * 100) AS FLOAT)/CAST((SELECT COUNT(1) FROM TblCustomer CU WHERE CONVERT(VARCHAR(10),CU.LastCallDate,120) = CONVERT(VARCHAR(10),C.LastCallDate,120) AND CU.StatusID IN(2,3,4,5)) AS FLOAT),2) AS PRefuse1 "
                //Dup
                + " , (SELECT COUNT(1) FROM TblCustomer CU WHERE CONVERT(VARCHAR(10),CU.LastCallDate,120) = CONVERT(VARCHAR(10),C.LastCallDate,120) AND CU.StatusID = 4 AND CU.SubStatusID = 4) AS Dup1 "
                + ", ROUND(CAST(((SELECT COUNT(1) FROM TblCustomer CU WHERE CONVERT(VARCHAR(10),CU.LastCallDate,120) = CONVERT(VARCHAR(10),C.LastCallDate,120) AND CU.StatusID = 4 AND CU.SubStatusID = 4) * 100) AS FLOAT)/CAST((SELECT COUNT(1) FROM TblCustomer CU WHERE CONVERT(VARCHAR(10),CU.LastCallDate,120) = CONVERT(VARCHAR(10),C.LastCallDate,120) AND CU.StatusID IN(2,3,4,5)) AS FLOAT),2) AS PDup1 "
                //Busy
                + ", (SELECT COUNT(1) FROM TblCustomer CU WHERE CONVERT(VARCHAR(10),CU.LastCallDate,120) = CONVERT(VARCHAR(10),C.LastCallDate,120) AND CU.StatusID = 5 AND CU.SubStatusID = 5) AS Busy1 "
                + ", ROUND(CAST(((SELECT COUNT(1) FROM TblCustomer CU WHERE CONVERT(VARCHAR(10),CU.LastCallDate,120) = CONVERT(VARCHAR(10),C.LastCallDate,120) AND CU.StatusID = 5 AND CU.SubStatusID = 5) * 100) AS FLOAT)/CAST((SELECT COUNT(1) FROM TblCustomer CU WHERE CONVERT(VARCHAR(10),CU.LastCallDate,120) = CONVERT(VARCHAR(10),C.LastCallDate,120) AND CU.StatusID IN(2,3,4,5)) AS FLOAT),2) AS PBusy1 "
                + ", (SELECT COUNT(1) FROM TblCustomer CU WHERE CONVERT(VARCHAR(10),CU.LastCallDate,120) = CONVERT(VARCHAR(10),C.LastCallDate,120) AND CU.StatusID = 5 AND CU.SubStatusID = 6) AS Busy2 "
                + ", ROUND(CAST(((SELECT COUNT(1) FROM TblCustomer CU WHERE CONVERT(VARCHAR(10),CU.LastCallDate,120) = CONVERT(VARCHAR(10),C.LastCallDate,120) AND CU.StatusID = 5 AND CU.SubStatusID = 6) * 100) AS FLOAT)/CAST((SELECT COUNT(1) FROM TblCustomer CU WHERE CONVERT(VARCHAR(10),CU.LastCallDate,120) = CONVERT(VARCHAR(10),C.LastCallDate,120) AND CU.StatusID IN(2,3,4,5)) AS FLOAT),2) AS PBusy2 "
                //TotalReached
                + ", (SELECT COUNT(1) FROM TblCustomer CU WHERE CONVERT(VARCHAR(10),CU.LastCallDate,120) = CONVERT(VARCHAR(10),C.LastCallDate,120) AND CU.StatusID IN(2,3,4,5))  AS TotalReached "
                + ",  ROUND(CAST((SELECT COUNT(1) FROM TblCustomer CU WHERE CONVERT(VARCHAR(10),CU.LastCallDate,120) = CONVERT(VARCHAR(10),C.LastCallDate,120) AND CU.StatusID IN(2,3,4,5)) AS FLOAT)/CAST((SELECT COUNT(1) FROM TblCustomer CU WHERE CONVERT(VARCHAR(10),CU.LastCallDate,120) = CONVERT(VARCHAR(10),C.LastCallDate,120)) AS FLOAT),2) * 100 AS PTotalReached "
                //NoContact
                + ", (SELECT COUNT(1) FROM TblCustomer CU WHERE CONVERT(VARCHAR(10),CU.LastCallDate,120) = CONVERT(VARCHAR(10),C.LastCallDate,120) AND CU.StatusID = 6 AND CU.SubStatusID = 7) AS NotContact1 "
                + ", CASE (SELECT COUNT(1) FROM TblCustomer CU WHERE CONVERT(VARCHAR(10),CU.LastCallDate,120) = CONVERT(VARCHAR(10),C.LastCallDate,120) AND CU.StatusID IN(6)) WHEN 0 THEN 0 ELSE ROUND(CAST(((SELECT COUNT(1) FROM TblCustomer CU WHERE CONVERT(VARCHAR(10),CU.LastCallDate,120) = CONVERT(VARCHAR(10),C.LastCallDate,120) AND CU.StatusID = 6 AND CU.SubStatusID = 7) * 100) AS FLOAT)/CAST((SELECT COUNT(1) FROM TblCustomer CU WHERE CONVERT(VARCHAR(10),CU.LastCallDate,120) = CONVERT(VARCHAR(10),C.LastCallDate,120) AND CU.StatusID IN(6)) AS FLOAT),2) END AS PNoContact1 "
                + ", (SELECT COUNT(1) FROM TblCustomer CU WHERE CONVERT(VARCHAR(10),CU.LastCallDate,120) = CONVERT(VARCHAR(10),C.LastCallDate,120) AND CU.StatusID = 6 AND CU.SubStatusID = 8) AS NotContact2 "
                + ", CASE (SELECT COUNT(1) FROM TblCustomer CU WHERE CONVERT(VARCHAR(10),CU.LastCallDate,120) = CONVERT(VARCHAR(10),C.LastCallDate,120) AND CU.StatusID IN(6)) WHEN 0 THEN 0 ELSE ROUND(CAST(((SELECT COUNT(1) FROM TblCustomer CU WHERE CONVERT(VARCHAR(10),CU.LastCallDate,120) = CONVERT(VARCHAR(10),C.LastCallDate,120) AND CU.StatusID = 6 AND CU.SubStatusID = 8) * 100) AS FLOAT)/CAST((SELECT COUNT(1) FROM TblCustomer CU WHERE CONVERT(VARCHAR(10),CU.LastCallDate,120) = CONVERT(VARCHAR(10),C.LastCallDate,120) AND CU.StatusID IN(6)) AS FLOAT),2) END AS PNoContact2 "
                + ", (SELECT COUNT(1) FROM TblCustomer CU WHERE CONVERT(VARCHAR(10),CU.LastCallDate,120) = CONVERT(VARCHAR(10),C.LastCallDate,120) AND CU.StatusID = 6 AND CU.SubStatusID = 9) AS NotContact3 "
                + ", CASE (SELECT COUNT(1) FROM TblCustomer CU WHERE CONVERT(VARCHAR(10),CU.LastCallDate,120) = CONVERT(VARCHAR(10),C.LastCallDate,120) AND CU.StatusID IN(6)) WHEN 0 THEN 0 ELSE ROUND(CAST(((SELECT COUNT(1) FROM TblCustomer CU WHERE CONVERT(VARCHAR(10),CU.LastCallDate,120) = CONVERT(VARCHAR(10),C.LastCallDate,120) AND CU.StatusID = 6 AND CU.SubStatusID = 9) * 300) AS FLOAT)/CAST((SELECT COUNT(1) FROM TblCustomer CU WHERE CONVERT(VARCHAR(10),CU.LastCallDate,120) = CONVERT(VARCHAR(10),C.LastCallDate,120) AND CU.StatusID IN(6)) AS FLOAT),2) END AS PNoContact3 "
                + ", (SELECT COUNT(1) FROM TblCustomer CU WHERE CONVERT(VARCHAR(10),CU.LastCallDate,120) = CONVERT(VARCHAR(10),C.LastCallDate,120) AND CU.StatusID = 6 AND CU.SubStatusID = 10) AS NotContact4 "
                + ", CASE (SELECT COUNT(1) FROM TblCustomer CU WHERE CONVERT(VARCHAR(10),CU.LastCallDate,120) = CONVERT(VARCHAR(10),C.LastCallDate,120) AND CU.StatusID IN(6)) WHEN 0 THEN 0 ELSE ROUND(CAST(((SELECT COUNT(1) FROM TblCustomer CU WHERE CONVERT(VARCHAR(10),CU.LastCallDate,120) = CONVERT(VARCHAR(10),C.LastCallDate,120) AND CU.StatusID = 6 AND CU.SubStatusID = 10) * 100) AS FLOAT)/CAST((SELECT COUNT(1) FROM TblCustomer CU WHERE CONVERT(VARCHAR(10),CU.LastCallDate,120) = CONVERT(VARCHAR(10),C.LastCallDate,120) AND CU.StatusID IN(6)) AS FLOAT),2) END AS PNoContact4 "
                + ", (SELECT COUNT(1) FROM TblCustomer CU WHERE CONVERT(VARCHAR(10),CU.LastCallDate,120) = CONVERT(VARCHAR(10),C.LastCallDate,120) AND CU.StatusID = 6 AND CU.SubStatusID = 11) AS NotContact5  "
                + ", CASE (SELECT COUNT(1) FROM TblCustomer CU WHERE CONVERT(VARCHAR(10),CU.LastCallDate,120) = CONVERT(VARCHAR(10),C.LastCallDate,120) AND CU.StatusID IN(6)) WHEN 0 THEN 0 ELSE ROUND(CAST(((SELECT COUNT(1) FROM TblCustomer CU WHERE CONVERT(VARCHAR(10),CU.LastCallDate,120) = CONVERT(VARCHAR(10),C.LastCallDate,120) AND CU.StatusID = 6 AND CU.SubStatusID = 11) * 100) AS FLOAT)/CAST((SELECT COUNT(1) FROM TblCustomer CU WHERE CONVERT(VARCHAR(10),CU.LastCallDate,120) = CONVERT(VARCHAR(10),C.LastCallDate,120) AND CU.StatusID IN(6)) AS FLOAT),2) END AS PNoContact5 "
                + ", (SELECT COUNT(1) FROM TblCustomer CU WHERE CONVERT(VARCHAR(10),CU.LastCallDate,120) = CONVERT(VARCHAR(10),C.LastCallDate,120) AND CU.StatusID = 6 AND CU.SubStatusID = 12) AS NotContact6  "
                + ", CASE (SELECT COUNT(1) FROM TblCustomer CU WHERE CONVERT(VARCHAR(10),CU.LastCallDate,120) = CONVERT(VARCHAR(10),C.LastCallDate,120) AND CU.StatusID IN(6)) WHEN 0 THEN 0 ELSE ROUND(CAST(((SELECT COUNT(1) FROM TblCustomer CU WHERE CONVERT(VARCHAR(10),CU.LastCallDate,120) = CONVERT(VARCHAR(10),C.LastCallDate,120) AND CU.StatusID = 6 AND CU.SubStatusID = 12) * 100) AS FLOAT)/CAST((SELECT COUNT(1) FROM TblCustomer CU WHERE CONVERT(VARCHAR(10),CU.LastCallDate,120) = CONVERT(VARCHAR(10),C.LastCallDate,120) AND CU.StatusID IN(6)) AS FLOAT),2) END AS PNoContact6 "
                //TotalUnReached
                + ", (SELECT COUNT(1) FROM TblCustomer CU WHERE CONVERT(VARCHAR(10),CU.LastCallDate,120) = CONVERT(VARCHAR(10),C.LastCallDate,120) AND CU.StatusID IN(6))  AS TotalUnReached "
                + ", ROUND(CAST((SELECT COUNT(1) FROM TblCustomer CU WHERE CONVERT(VARCHAR(10),CU.LastCallDate,120) = CONVERT(VARCHAR(10),C.LastCallDate,120) AND CU.StatusID IN(6)) AS FLOAT)/CAST((SELECT COUNT(1) FROM TblCustomer CU WHERE CONVERT(VARCHAR(10),CU.LastCallDate,120) = CONVERT(VARCHAR(10),C.LastCallDate,120)) AS FLOAT),2) * 100 AS PTotalUnReached "

                + " FROM TblCustomer C "
                + " WHERE LastCallDate IS NOT NULL "
                + " AND CONVERT(VARCHAR(24),C.LastCallDate,120) BETWEEN '" + DateStart + "' AND '" + DateEnd + "' "
                + " GROUP BY CONVERT(VARCHAR(10),C.LastCallDate,120) "
                + " ORDER BY CONVERT(VARCHAR(10),C.LastCallDate,120) ";
            

            DataTable dt = GE.SELECT_Executeion(strSQL);
            return dt;
        }
        catch (Exception ex)
        {
            RadAjaxManager1.Alert(ex.Message);
            return null;
        }
    }

    public void ExportToExcelTemplate(DataTable dt)
    {
        try
        {
            Excel.Application xlApp;
            Excel.Workbook xlWorkBook;
            Excel.Worksheet xlWorkSheet = null;
            object mis = Type.Missing;

            string strTemplate = @"C:\inetpub\wwwroot\outbound.kaidee\_FileTemplate\CallSummary.xlsx";

            string strExcelName = "CALLSUMMARY_" + DateTime.Now.ToString("yyyyMMddHHmmssss") + ".xlsx";

            string strFileExport = @"C:\inetpub\wwwroot\outbound.kaidee\_FileExport\" + strExcelName;

            xlApp = new Excel.Application();
            xlWorkBook = xlApp.Workbooks.Open(strTemplate, 0, true, 2, "", "", true, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
            xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
            xlWorkSheet.Name = "CallSummary";


            string sDate = rdpDateS.DateInput.SelectedDate.Value.ToShortDateString();
            string eDate = rdpDateE.DateInput.SelectedDate.Value.ToShortDateString();
            xlWorkSheet.Cells[3, 1] = "Report Name : [ Outbound ] CallSummay ";
            xlWorkSheet.Cells[4, 1] = "Report Date : " + DateTime.Now.ToString("dd/MM/yyyy hh:mm");
            xlWorkSheet.Cells[5, 1] = "ช่วงวันที่ระบุ : " + sDate.Trim() + "  ถึง  " + eDate.Trim();

            int no;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                for (int iColumn = 0; iColumn < dt.Columns.Count; iColumn++)
                {
                    xlWorkSheet.Cells[i + 13, iColumn + 1] = dt.Rows[i][iColumn].ToString().Trim();
                }
            }

            Microsoft.Office.Interop.Excel.Range cellA1 = xlWorkSheet.Cells;
            cellA1.WrapText = false;

            xlWorkBook.SaveAs(strFileExport, mis, mis, mis, mis, mis, Excel.XlSaveAsAccessMode.xlExclusive, mis, mis, mis, mis, mis);
            xlWorkBook.Close(true, mis, mis);
            xlApp.Quit();
            System.Runtime.InteropServices.Marshal.ReleaseComObject(xlApp);
            System.Runtime.InteropServices.Marshal.ReleaseComObject(xlWorkBook);
            xlApp = null;
            xlWorkBook = null;
            xlWorkSheet = null;
            GC.Collect();
            //RadAjaxManager1.Alert("Generate Successfully");
            System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
            response.ClearContent();
            response.Clear();
            Response.ContentType = "application/xlsx";
            response.AddHeader("Content-Disposition", "attachment; filename=" + strExcelName);
            Response.TransmitFile(strFileExport);
            response.Flush();
            Response.SuppressContent = true;
            response.End();
        }
        catch (Exception ex)
        {
            RadAjaxManager1.Alert(ex.Message);
        }
    }
}