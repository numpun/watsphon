﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageInfo.master" AutoEventWireup="true" CodeFile="TranferList.aspx.cs" Inherits="TranferList" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Label ID="lblUserID" runat="server" Visible="false" Text="Label"></asp:Label>
    <asp:Label ID="lblUserLevelID" runat="server" Visible="false" Text="Label"></asp:Label>
    <asp:Panel ID="PanelSearch" runat="server">
        <ul id="Ul3" class="formList">
            <li>
                <table width="90%">
                    <tr>
                        <td width="100px" align="right"><b>DataSource :</b></td>
                        <td align="left">
                            <style>
                                /** Columns */
                                .rcbHeader ul,
                                .rcbFooter ul,
                                .rcbItem ul,
                                .rcbHovered ul,
                                .rcbDisabled ul {
                                    margin: 0;
                                    padding: 0;
                                    width: 100%;
                                    display: inline-block;
                                    list-style-type: none;
                                }

                                .exampleRadComboBox.RadComboBoxDropDown .rcbHeader {
                                    padding: 5px 27px 4px 7px;
                                }

                                .rcbScroll {
                                    overflow: scroll !important;
                                    overflow-x: hidden !important;
                                }

                                .col1 {
                                    width: 60%;
                                    margin: 0;
                                    padding: 0 5px 0 0;
                                    line-height: 14px;
                                    float: left;
                                }

                                .col2 {
                                    margin: 0;
                                    width: 15%;
                                    padding: 0 5px 0 0;
                                    line-height: 14px;
                                    float: left;
                                }


                                /** Multiple rows and columns */
                                .multipleRowsColumns .rcbItem,
                                .multipleRowsColumns .rcbHovered {
                                    float: left;
                                    margin: 0 1px;
                                    min-height: 13px;
                                    overflow: hidden;
                                    padding: 2px 19px 2px 6px;
                                    width: 195px;
                                }


                                .results {
                                    display: block;
                                    margin-top: 20px;
                                }
                            </style>
                            <telerik:RadComboBox
                                runat="server"
                                ID="rcbSource"
                                EmptyMessage="...Select Datasource..."
                                LoadingMessage="กำลังโหลดข้อมูล ..."
                                Height="200px"
                                Width="800px"
                                AutoPostBack="True"
                                MarkFirstMatch="true"
                                HighlightTemplatedItems="true"
                                OnClientItemsRequested="UpdateItemCountField"
                                OnSelectedIndexChanged="rcbSource_SelectedIndexChanged"
                                OnItemDataBound="rcbSource_ItemDataBound"
                                DropDownCssClass="exampleRadComboBox">
                                <HeaderTemplate>
                                    <ul>

                                        <li class="col1">แหล่งที่มา(DataSourceName)</li>
                                        <li class="col2">ทั้งหมด</li>
                                        <li class="col2">คงเหลือ(ในตัว)</li>
                                    </ul>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <ul>
                                        <li class="col1">
                                            <%# DataBinder.Eval(Container.DataItem, "SourceName") %></li>
                                        <li class="col2">
                                            <%# DataBinder.Eval(Container.DataItem, "TotalSource") %></li>
                                        <li class="col2">
                                            <%# DataBinder.Eval(Container.DataItem, "DataSUM") %></li>
                                    </ul>
                                </ItemTemplate>
                            </telerik:RadComboBox>
                            <telerik:RadScriptBlock runat="server">
                                <script type="text/javascript">
                                    function UpdateItemCountField(sender, args) {
                                        //Set the footer text.
                                        sender.get_dropDownElement().lastChild.innerHTML = "A total of " + sender.get_items().get_count() + " items";
                                    }
                                </script>
                            </telerik:RadScriptBlock>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <br />
                            <hr />
                        </td>
                    </tr>
                </table>
            </li>
        </ul>
    </asp:Panel>
    <asp:Panel ID="PanelList" runat="server">
        <div style="width: 90%;">
            <div style="width: 44%; float: left;">
                <table width="100%" border="0">
                    <tr>
                        <td>
                            <p><b>ต้นทาง</b></p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <telerik:RadComboBox
                                runat="server"
                                ID="rcbUserS"
                                EmptyMessage="...Select Level..."
                                CausesValidation="false"
                                AutoPostBack="true"
                                OnSelectedIndexChanged="rcbUserS_SelectedIndexChanged"
                                Width="300px">
                                <Items>
                                    <telerik:RadComboBoxItem runat="server" Text="--เลือก--" Value="" />
                                    <telerik:RadComboBoxItem runat="server" Text="Supervisor" Value="S" />
                                    <telerik:RadComboBoxItem runat="server" Text="Agent" Value="T" />
                                </Items>
                            </telerik:RadComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <br />
                            <telerik:RadGrid
                                ID="RadGrid1"
                                runat="server"
                                Width="90%"
                                Height="300px"
                                AutoGenerateColumns="false"
                                GridLines="None"
                                BorderWidth="1"
                                AllowPaging="false"
                                AllowSorting="true"
                                Style="outline: none"
                                AllowFilteringByColumn="True"
                                AllowMultiRowSelection="True">
                                <PagerStyle Mode="NextPrevAndNumeric" />
                                <MasterTableView
                                    AutoGenerateColumns="False"
                                    DataKeyNames="Sequence,sta_total,sta_new,sta_success,sta_Refuse,sta_WrongNumber,sta_Duplicated,sta_Recheck,sta_Appoint,sta_Business,sta_NoContact,sta_Error"
                                    ClientDataKeyNames="Sequence,sta_total,sta_new,sta_success,sta_Refuse,sta_WrongNumber,sta_Duplicated,sta_Recheck,sta_Appoint,sta_Business,sta_NoContact,sta_Error"
                                    NoMasterRecordsText="&lt; ไม่มีข้อมูล &gt;"
                                    CommandItemDisplay="none"
                                    AllowMultiColumnSorting="True">
                                    <CommandItemTemplate>
                                        <table width="100%">
                                            <tr>
                                                <td>test button and set CommandItemDisplay="Top"
                                                </td>
                                            </tr>
                                        </table>
                                    </CommandItemTemplate>
                                    <Columns>
                                        <telerik:GridClientSelectColumn UniqueName="ClientSelectColumn">
                                            <HeaderStyle Width="50px" HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Center" />
                                        </telerik:GridClientSelectColumn>
                                        <telerik:GridBoundColumn AutoPostBackOnFilter="true"
                                            CurrentFilterFunction="Contains"
                                            ShowFilterIcon="false"
                                            FilterControlWidth="100%"
                                            HeaderText="ชื่อ-นามสกุล"
                                            DataField="UserName"
                                            UniqueName="UserName">
                                            <HeaderStyle HorizontalAlign="Center" Wrap="False" Width="200" />
                                            <ItemStyle HorizontalAlign="Left" Wrap="False" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn AutoPostBackOnFilter="true"
                                            CurrentFilterFunction="Contains"
                                            ShowFilterIcon="false"
                                            FilterControlWidth="100%"
                                            HeaderText="Total"
                                            DataField="sta_total"
                                            UniqueName="sta_total">
                                            <HeaderStyle HorizontalAlign="Center" Wrap="False" Width="60" />
                                            <ItemStyle HorizontalAlign="Right" Wrap="False" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn AutoPostBackOnFilter="true"
                                            CurrentFilterFunction="Contains"
                                            ShowFilterIcon="false"
                                            FilterControlWidth="100%"
                                            HeaderText="New"
                                            DataField="sta_new"
                                            UniqueName="sta_new">
                                            <HeaderStyle HorizontalAlign="Center" Wrap="False" Width="60" />
                                            <ItemStyle HorizontalAlign="Right" Wrap="False" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn AutoPostBackOnFilter="true"
                                            CurrentFilterFunction="Contains"
                                            ShowFilterIcon="false"
                                            FilterControlWidth="100%"
                                            HeaderText="Business"
                                            DataField="sta_Business"
                                            UniqueName="sta_Business">
                                            <HeaderStyle HorizontalAlign="Center" Wrap="False" Width="60" />
                                            <ItemStyle HorizontalAlign="Right" Wrap="False" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn AutoPostBackOnFilter="true"
                                            CurrentFilterFunction="Contains"
                                            ShowFilterIcon="false"
                                            FilterControlWidth="100%"
                                            HeaderText="Duplicated"
                                            DataField="sta_Duplicated"
                                            UniqueName="sta_Duplicated">
                                            <HeaderStyle HorizontalAlign="Center" Wrap="False" Width="60" />
                                            <ItemStyle HorizontalAlign="Right" Wrap="False" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn AutoPostBackOnFilter="true"
                                            CurrentFilterFunction="Contains"
                                            ShowFilterIcon="false"
                                            FilterControlWidth="100%"
                                            HeaderText="NoContact"
                                            DataField="sta_NoContact"
                                            UniqueName="sta_NoContact">
                                            <HeaderStyle HorizontalAlign="Center" Wrap="False" Width="60" />
                                            <ItemStyle HorizontalAlign="Right" Wrap="False" />
                                        </telerik:GridBoundColumn>

                                    </Columns>
                                </MasterTableView>
                                <ClientSettings
                                    AllowColumnsReorder="true"
                                    AllowColumnHide="True"
                                    EnableRowHoverStyle="True"
                                    ReorderColumnsOnClient="true">
                                    <Selecting AllowRowSelect="True" />
                                    <Resizing AllowColumnResize="true" />
                                    <Scrolling AllowScroll="True" UseStaticHeaders="false" SaveScrollPosition="True"></Scrolling>
                                </ClientSettings>
                            </telerik:RadGrid>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <br />
                            <hr />
                        </td>
                    </tr>
                </table>
            </div>
            <div style="width: 44%; float: right;">
                <table width="100%" border="0">
                    <tr>
                        <td>
                            <p><b>ปลายทาง</b></p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <telerik:RadComboBox
                                runat="server"
                                ID="rcbUserE"
                                EmptyMessage="...Select Level..."
                                CausesValidation="false"
                                AutoPostBack="true"
                                OnSelectedIndexChanged="rcbUserE_SelectedIndexChanged"
                                Width="300px">
                                <Items>
                                    <telerik:RadComboBoxItem runat="server" Text="--เลือก--" Value="" />
                                    <telerik:RadComboBoxItem runat="server" Text="Supervisor" Value="S" />
                                    <telerik:RadComboBoxItem runat="server" Text="Agent" Value="T" />
                                </Items>
                            </telerik:RadComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <br />
                            <telerik:RadGrid
                                ID="RadGrid2"
                                runat="server"
                                Width="90%"
                                Height="300px"
                                AutoGenerateColumns="false"
                                GridLines="None"
                                BorderWidth="1"
                                AllowPaging="false"
                                AllowSorting="true"
                                AllowFilteringByColumn="True"
                                AllowMultiRowSelection="True">
                                <PagerStyle Mode="NextPrevAndNumeric" />
                                <MasterTableView
                                    AutoGenerateColumns="False"
                                    DataKeyNames="Sequence,sta_total,sta_new,sta_success,sta_Refuse,sta_WrongNumber,sta_Duplicated,sta_Recheck,sta_Appoint,sta_Business,sta_NoContact,sta_Error"
                                    ClientDataKeyNames="Sequence,sta_total,sta_new,sta_success,sta_Refuse,sta_WrongNumber,sta_Duplicated,sta_Recheck,sta_Appoint,sta_Business,sta_NoContact,sta_Error"
                                    NoMasterRecordsText="&lt; ไม่มีข้อมูล &gt;"
                                    CommandItemDisplay="none"
                                    AllowMultiColumnSorting="True">
                                    <CommandItemTemplate>
                                        <table width="100%">
                                            <tr>
                                                <td>test button and set CommandItemDisplay="Top"
                                                </td>
                                            </tr>
                                        </table>
                                    </CommandItemTemplate>
                                    <Columns>
                                        <telerik:GridClientSelectColumn UniqueName="ClientSelectColumn">
                                            <HeaderStyle Width="50px" HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Center" />
                                        </telerik:GridClientSelectColumn>
                                        <telerik:GridBoundColumn AutoPostBackOnFilter="true"
                                            CurrentFilterFunction="Contains"
                                            ShowFilterIcon="false"
                                            FilterControlWidth="100%"
                                            HeaderText="ชื่อ-นามสกุล"
                                            DataField="UserName"
                                            UniqueName="UserName">
                                            <HeaderStyle HorizontalAlign="Center" Wrap="False" Width="200" />
                                            <ItemStyle HorizontalAlign="Left" Wrap="False" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn AutoPostBackOnFilter="true"
                                            CurrentFilterFunction="Contains"
                                            ShowFilterIcon="false"
                                            FilterControlWidth="100%"
                                            HeaderText="Total"
                                            DataField="sta_total"
                                            UniqueName="sta_total">
                                            <HeaderStyle HorizontalAlign="Center" Wrap="False" Width="60" />
                                            <ItemStyle HorizontalAlign="Right" Wrap="False" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn AutoPostBackOnFilter="true"
                                            CurrentFilterFunction="Contains"
                                            ShowFilterIcon="false"
                                            FilterControlWidth="100%"
                                            HeaderText="New"
                                            DataField="sta_new"
                                            UniqueName="sta_new">
                                            <HeaderStyle HorizontalAlign="Center" Wrap="False" Width="60" />
                                            <ItemStyle HorizontalAlign="Right" Wrap="False" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn AutoPostBackOnFilter="true"
                                            CurrentFilterFunction="Contains"
                                            ShowFilterIcon="false"
                                            FilterControlWidth="100%"
                                            HeaderText="Business"
                                            DataField="sta_Business"
                                            UniqueName="sta_Business">
                                            <HeaderStyle HorizontalAlign="Center" Wrap="False" Width="60" />
                                            <ItemStyle HorizontalAlign="Right" Wrap="False" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn AutoPostBackOnFilter="true"
                                            CurrentFilterFunction="Contains"
                                            ShowFilterIcon="false"
                                            FilterControlWidth="100%"
                                            HeaderText="Duplicated"
                                            DataField="sta_Duplicated"
                                            UniqueName="sta_Duplicated">
                                            <HeaderStyle HorizontalAlign="Center" Wrap="False" Width="60" />
                                            <ItemStyle HorizontalAlign="Right" Wrap="False" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn AutoPostBackOnFilter="true"
                                            CurrentFilterFunction="Contains"
                                            ShowFilterIcon="false"
                                            FilterControlWidth="100%"
                                            HeaderText="NoContact"
                                            DataField="sta_NoContact"
                                            UniqueName="sta_NoContact">
                                            <HeaderStyle HorizontalAlign="Center" Wrap="False" Width="60" />
                                            <ItemStyle HorizontalAlign="Right" Wrap="False" />
                                        </telerik:GridBoundColumn>

                                    </Columns>
                                </MasterTableView>
                                <ClientSettings
                                    AllowColumnsReorder="true"
                                    AllowColumnHide="True"
                                    EnableRowHoverStyle="True"
                                    ReorderColumnsOnClient="true">
                                    <Selecting AllowRowSelect="True" />
                                    <Resizing AllowColumnResize="true" />
                                    <Scrolling AllowScroll="True" UseStaticHeaders="false" SaveScrollPosition="True"></Scrolling>
                                </ClientSettings>
                            </telerik:RadGrid>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <br />
                            <hr />
                        </td>
                    </tr>
                </table>

            </div>
            <table width="90%" border="0">
                <tr>
                    <td>
                        <table width="100%" border="0">
                            <tr>
                                <td>
                                    <b>จำนวนที่พบ (Found)</b>&nbsp;
                            <hr />
                                    <table width="90%" border="0">
                                        <tr>
                                            <td align="left" width="100px">
                                                <b>NEW :</b>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtNew" runat="server" Width="200px" Enabled="false" Text="0"></asp:TextBox>
                                            </td>
                                            <td align="left" width="100px" style="background: #c3c3c3;">
                                                <b>Refuse :</b>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtRefuse" runat="server" Width="200px" Enabled="false" Text="0"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <b>Business :</b>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtBusiness" runat="server" Width="200px" Enabled="false" Text="0"></asp:TextBox>
                                            </td>

                                            <td style="background: #c3c3c3;">
                                                <b>Recheck :</b>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtRecheck" runat="server" Width="200px" Enabled="false" Text="0"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <b>Duplicated :</b>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtDuplicateed" runat="server" Width="200px" Enabled="false" Text="0"></asp:TextBox>
                                            </td>

                                            <td style="background: #c3c3c3;">
                                                <b>Appoint :</b>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtAppoint" runat="server" Width="200px" Enabled="false" Text="0"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <b>No Contact :</b>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtNoContact" runat="server" Width="200px" Enabled="false" Text="0"></asp:TextBox>
                                            </td>
                                            <td style="background: #c3c3c3;">
                                                <b>Wrong N :</b>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtWrongN" runat="server" Width="200px" Enabled="false" Text="0"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>

                                            <td style="background: #c3c3c3;">
                                                <b>Success :</b>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtSuccess" runat="server" Width="200px" Enabled="false" Text="0"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4">
                                                <hr />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><b>ทั้งหมด :</b></td>
                                            <td>
                                                <asp:TextBox ID="txtTotal" runat="server" Width="200px" Enabled="false" Text="0"></asp:TextBox></td>
                                            <td>
                                                <b>พนง.(คน) :</b>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtEmp" runat="server" Width="200px" Enabled="false" Text="0"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <b>ที่โอนได้ :</b>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtComit" runat="server" Width="200px" Enabled="false" Text="0"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <b>คงเหลือ :</b>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtRemain" runat="server" Width="200px" Enabled="false" Text="0"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                    <br />
                                    <table width="100%" bgcolor="#CCFFCC">
                                        <tr>
                                            <td align="left">
                                                <asp:Button ID="BtnCal" Font-Bold="True" Width="120px" runat="server" Text="คำนวน" OnClick="BtnCal_Click" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td>
                                    <b>จำนวนที่โอนได้ต่อคน</b>&nbsp;
                            <hr />
                                    <table width="100%">
                                        <tr>
                                            <td colspan="2">
                                                <br />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <br />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <br />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <br />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <br />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" width="120px">
                                                <asp:CheckBox ID="chkNew" runat="server" Text="NEW" Font-Bold="True" />
                                            </td>
                                            <td >
                                                <telerik:RadNumericTextBox ID="txtAssignNew" runat="server"
                                                    NumberFormat-DecimalDigits="0"
                                                    MaxLength="4" Value="0" Width="100">
                                                </telerik:RadNumericTextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:CheckBox ID="chkBusy" runat="server" Text="Business" Font-Bold="True" />
                                            </td>
                                            <td>
                                                <telerik:RadNumericTextBox ID="txtAssignBusy" runat="server"
                                                    NumberFormat-DecimalDigits="0"
                                                    MaxLength="4" Value="0" Width="100">
                                                </telerik:RadNumericTextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:CheckBox ID="chkPresented" runat="server" Text="Duplicated" Font-Bold="True" />
                                            </td>
                                            <td>
                                                <telerik:RadNumericTextBox ID="txtAssignPresented" runat="server"
                                                    NumberFormat-DecimalDigits="0"
                                                    MaxLength="4" Value="0" Width="100">
                                                </telerik:RadNumericTextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:CheckBox ID="chkNoContact" runat="server" Text="NoContact" Font-Bold="True" />
                                            </td>
                                            <td>
                                                <telerik:RadNumericTextBox ID="txtAssignNoContact" runat="server"
                                                    NumberFormat-DecimalDigits="0"
                                                    MaxLength="4" Value="0" Width="100">
                                                </telerik:RadNumericTextBox>
                                            </td>
                                        </tr>
                                        
                                    </table>
                                    <br />
                                    <table width="100%" bgcolor="#CCFFCC">
                                        <tr>
                                            <td style="float: right;">
                                                <asp:CheckBox ID="cbkToNew" Visible="false" runat="server" Text="เปลี่ยนสถานะเป็น New  " Font-Bold="True" ForeColor="Red" />
                                                <asp:Button ID="btnAssign" runat="server" OnClick="btnAssign_Click"
                                                    Text="โอนรายชื่อ" Width="120px" Font-Bold="True" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server"></telerik:RadAjaxLoadingPanel>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" EnableAJAX="true" EnablePageHeadUpdate="false" EnableViewState="false">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="rcbSource">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="PanelList" LoadingPanelID="RadAjaxLoadingPanel1"></telerik:AjaxUpdatedControl>
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="rcbUserS">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid1" LoadingPanelID="RadAjaxLoadingPanel1"></telerik:AjaxUpdatedControl>
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="rcbUserE">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid2" LoadingPanelID="RadAjaxLoadingPanel1"></telerik:AjaxUpdatedControl>
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="BtnCal">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="PanelList" LoadingPanelID="RadAjaxLoadingPanel1"></telerik:AjaxUpdatedControl>
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
</asp:Content>

