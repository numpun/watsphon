﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageInfo.master" AutoEventWireup="true" CodeFile="UserManagement.aspx.cs" Inherits="UserManagement" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .tblfrom td {
            padding: 5px 10px;
        }

        .btnGreen {
            width: 45%;
            background-color: #1CB94E;
            border: none;
            outline: none;
            color: #fff;
            font-size: 14px;
            font-weight: normal;
            padding: 10px 0;
            border-radius: 2px;
            text-transform: uppercase;
        }

        .btnBlue {
            width: 45%;
            background-color: #3C59A8;
            border: none;
            outline: none;
            color: #fff;
            font-size: 14px;
            font-weight: normal;
            padding: 10px 0;
            border-radius: 2px;
            text-transform: uppercase;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Label ID="lblUserID" runat="server" Visible="false" Text="Label"></asp:Label>
    <asp:Label ID="lblUserLevelID" runat="server" Visible="false" Text="Label"></asp:Label>
    <asp:Label ID="lblEditID" runat="server" Visible="false" Text="Label"></asp:Label>
    <asp:Panel ID="PanelAll" runat="server">
        <table width="100%" border="0">
            <tr>
                <td width="60%">
                    <asp:Panel ID="PanelList" runat="server">
                        <table>
                            <tr>
                                <td>
                                    <asp:Label ID="lblToolBarMode" Visible="false" runat="server" Text=""></asp:Label>
                                    <telerik:RadToolBar
                                        ID="RadToolBar1"
                                        runat="server"
                                        OnButtonClick="RadToolBar1_ButtonClick">
                                        <Items>
                                            <telerik:RadToolBarButton Text="เพิ่ม"
                                                Value="Add" Width="100px"
                                                ImageUrl="Images/icons/add.png"
                                                Enabled="true"
                                                CommandName="Add"
                                                Target="_blank" />
                                            <telerik:RadToolBarButton IsSeparator="true">
                                            </telerik:RadToolBarButton>
                                            <telerik:RadToolBarButton
                                                Text="แก้ไข" Width="100px"
                                                Value="Edit"
                                                ImageUrl="Images/icons/edit.png"
                                                Enabled="true"
                                                CommandName="Edit" />
                                        </Items>
                                    </telerik:RadToolBar>
                                    <%--<telerik:RadToolBar
                        ID="RdtbMain"
                        runat="server"
                        EnableRoundedCorners="true"
                        EnableShadows="true"
                        OnClientButtonClicking="onToolBarClientButtonClicking">
                        <Items>
                            <telerik:RadToolBarButton Text="Add New" Value="New" ImageUrl="Images/icons/add.png" Enabled="true" CommandName="new" NavigateUrl="UserManagementFrm.aspx"
                                Target="_blank" />
                            <telerik:RadToolBarButton IsSeparator="true">
                            </telerik:RadToolBarButton>
                            <telerik:RadToolBarButton Text="Refresh" Value="Refresh" ImageUrl="Images/icons/refresh.png" Enabled="true" CommandName="refresh" />
                        </Items>
                    </telerik:RadToolBar>--%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <telerik:RadGrid
                                        ID="RadGrid1"
                                        runat="server"
                                        Width="100%"
                                        Height="500px"
                                        AutoGenerateColumns="false"
                                        GridLines="None"
                                        BorderWidth="1"
                                        AllowPaging="false"
                                        AllowSorting="true"
                                        Style="outline: none"
                                        AllowFilteringByColumn="True"
                                        AllowMultiRowSelection="False"
                                        OnItemCommand="RadGrid1_ItemCommand">
                                        <PagerStyle Mode="NextPrevAndNumeric" />
                                        <MasterTableView
                                            AutoGenerateColumns="False"
                                            DataKeyNames="UserID"
                                            ClientDataKeyNames="UserID"
                                            PageSize="13"
                                            NoMasterRecordsText="&lt; ไม่มีข้อมูล &gt;"
                                            AllowMultiColumnSorting="false"
                                            AllowPaging="true"
                                            EnableHeaderContextMenu="true">
                                            <CommandItemTemplate>
                                                <table width="100%">
                                                    <tr>
                                                        <td>test button and set CommandItemDisplay="Top"
                                                        </td>
                                                    </tr>
                                                </table>
                                            </CommandItemTemplate>
                                            <Columns>
                                                <telerik:GridClientSelectColumn UniqueName="ClientSelectColumn">
                                                    <HeaderStyle Width="50px" HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </telerik:GridClientSelectColumn>
                                                <telerik:GridBoundColumn AutoPostBackOnFilter="true"
                                                    CurrentFilterFunction="Contains"
                                                    ShowFilterIcon="false"
                                                    FilterControlWidth="100%"
                                                    HeaderText="รหัสพนักงาน"
                                                    DataField="UserID">
                                                    <HeaderStyle HorizontalAlign="Center" Wrap="False" Width="80px" />
                                                    <ItemStyle HorizontalAlign="Center" Wrap="False" />
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn AutoPostBackOnFilter="true"
                                                    CurrentFilterFunction="Contains"
                                                    ShowFilterIcon="false"
                                                    FilterControlWidth="100%"
                                                    HeaderText="UserName"
                                                    DataField="UserName">
                                                    <HeaderStyle HorizontalAlign="Center" Wrap="False" />
                                                    <ItemStyle HorizontalAlign="Center" Wrap="False" />
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn
                                                    HeaderStyle-HorizontalAlign="Center"
                                                    HeaderText="ตำแหน่ง"
                                                    DataField="UserLevelID"
                                                    UniqueName="UserLevelID">
                                                    <HeaderStyle HorizontalAlign="Center" Width="100px" />
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <FilterTemplate>
                                                        <telerik:RadComboBox
                                                            runat="server"
                                                            ID="cmbGroup"
                                                            DropDownWidth="120px" Width="90px"
                                                            CausesValidation="false"
                                                            SelectedValue='<%# ((GridItem)Container).OwnerTableView.GetColumn("UserLevelID").CurrentFilterValue %>'
                                                            OnClientSelectedIndexChanged="GrpIndexChanged">
                                                            <Items>
                                                                <telerik:RadComboBoxItem Text="All" />
                                                                <telerik:RadComboBoxItem runat="server" Text="Agent" Value="T" />
                                                                <telerik:RadComboBoxItem runat="server" Text="Sup" Value="S" />
                                                                <telerik:RadComboBoxItem runat="server" Text="QC" Value="Q" />
                                                            </Items>
                                                        </telerik:RadComboBox>
                                                        <telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
                                                            <script type="text/javascript">
                                                                function GrpIndexChanged(sender, args) {
                                                                    if (args.get_item() != null) {
                                                                        var tableView = $find("<%# ((GridItem)Container).OwnerTableView.ClientID %>");
                                                                        tableView.filter("UserLevelID", args.get_item().get_value(), "EqualTo");
                                                                    }
                                                                }
                                                            </script>
                                                        </telerik:RadScriptBlock>
                                                    </FilterTemplate>
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn AutoPostBackOnFilter="true"
                                                    CurrentFilterFunction="Contains"
                                                    ShowFilterIcon="false"
                                                    FilterControlWidth="100%"
                                                    HeaderText="ชื่อ-นามสกุลพนักงาน"
                                                    DataField="FullName">
                                                    <HeaderStyle HorizontalAlign="Center" Wrap="False" />
                                                    <ItemStyle HorizontalAlign="Center" Wrap="False" />
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn AutoPostBackOnFilter="true"
                                                    CurrentFilterFunction="Contains"
                                                    ShowFilterIcon="false"
                                                    FilterControlWidth="100%"
                                                    HeaderText="สถานะ"
                                                    DataField="Active">
                                                    <HeaderStyle HorizontalAlign="Center" Wrap="False" Width="70px" />
                                                    <ItemStyle HorizontalAlign="Center" Wrap="False" />
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn AutoPostBackOnFilter="true"
                                                    CurrentFilterFunction="Contains"
                                                    ShowFilterIcon="false"
                                                    FilterControlWidth="100%"
                                                    HeaderText="วันที่สร้าง"
                                                    DataField="CreateDate">
                                                    <HeaderStyle HorizontalAlign="Center" Wrap="False" Width="120px" />
                                                    <ItemStyle HorizontalAlign="Center" Wrap="False" />
                                                </telerik:GridBoundColumn>
                                            </Columns>
                                        </MasterTableView>
                                        <ClientSettings
                                            AllowColumnsReorder="true"
                                            AllowColumnHide="True"
                                            EnableRowHoverStyle="True"
                                            ReorderColumnsOnClient="true">
                                            <Selecting AllowRowSelect="True" />
                                            <Resizing AllowColumnResize="true" />
                                            <Scrolling AllowScroll="True"
                                                UseStaticHeaders="True"
                                                SaveScrollPosition="True"></Scrolling>
                                        </ClientSettings>
                                    </telerik:RadGrid>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
                <td valgin="top">
                    <asp:Panel ID="PanelFrom" runat="server">
                        <table border="0" class="tblfrom">
                            <tr>
                                <td>
                                    <telerik:RadTabStrip ID="RadTabStrip1" runat="server" SelectedIndex="0" MultiPageID="RadMultiPage1" CausesValidation="False">
                                        <Tabs>
                                            <telerik:RadTab ImageUrl="Images/icons/info.png" Text="ข้อมูลพนักงาน">
                                            </telerik:RadTab>
                                            <telerik:RadTab ImageUrl="Images/icons/history.png" Text="ประวัติการแก้ไข">
                                            </telerik:RadTab>
                                            <telerik:RadTab ImageUrl="Images/icons/loginhis.png" Text="ประวัติการเข้าใช้งาน">
                                            </telerik:RadTab>
                                        </Tabs>
                                    </telerik:RadTabStrip>
                                    <hr />
                                    <telerik:RadMultiPage runat="server" ID="RadMultiPage1" SelectedIndex="0" Width="100%">
                                        <telerik:RadPageView runat="server" ID="RadPageView1">
                                            <table width="100%">
                                                <tr>
                                                    <td width="30%" align="right"><strong>ชื่อผู้ใช้ : </strong></td>
                                                    <td align="left">
                                                        <telerik:RadTextBox ID="txtUsername" runat="server"></telerik:RadTextBox>
                                                        <strong style="color: red;">*</strong>
                                                        <hr />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="30%" align="right"><strong>รหัสผ่าน : </strong></td>
                                                    <td align="left">
                                                        <telerik:RadTextBox ID="txtPassword" runat="server"></telerik:RadTextBox>
                                                        <strong style="color: red;">*</strong>
                                                        <hr />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="30%" align="right"><strong>ชื่อ : </strong></td>
                                                    <td align="left">
                                                        <telerik:RadTextBox ID="txtFname" runat="server"></telerik:RadTextBox>
                                                        <strong style="color: red;">*</strong>
                                                        <hr />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="30%" align="right"><strong>นามสกุล : </strong></td>
                                                    <td align="left">
                                                        <telerik:RadTextBox ID="txtLname" runat="server"></telerik:RadTextBox>
                                                        <strong style="color: red;">*</strong>
                                                        <hr />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="30%" align="right"><strong>ชื่อเล่น : </strong></td>
                                                    <td align="left">
                                                        <telerik:RadTextBox ID="txtNname" runat="server"></telerik:RadTextBox>
                                                        <strong style="color: red;">*</strong>
                                                        <hr />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="30%" align="right"><strong>เบอร์โทร : </strong></td>
                                                    <td align="left">
                                                        <telerik:RadTextBox ID="txtTel" runat="server"></telerik:RadTextBox>
                                                        <strong style="color: red;">*</strong>
                                                        <hr />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="30%" align="right"><strong>ตำแหน่ง : </strong></td>
                                                    <td align="left">
                                                        <telerik:RadComboBox ID="rcbUserLevel" runat="server">
                                                            <Items>
                                                                <telerik:RadComboBoxItem Text="--เลือกตำแหน่ง--" Value="" />
                                                                <telerik:RadComboBoxItem Text="Agent" Value="T" />
                                                                <telerik:RadComboBoxItem Text="Supvisor" Value="S" />
                                                                <telerik:RadComboBoxItem Text="QC" Value="Q" />
                                                            </Items>
                                                        </telerik:RadComboBox>
                                                        <hr />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="30%" align="right"></td>
                                                    <td align="left">
                                                        <asp:Button ID="BtnSaveUser" OnClick="BtnSaveUser_Click" OnClientClick="if ( !confirm('-ยืนยันการบันทึก-?')) return false;" CssClass="btnBlue" runat="server" Text="บันทึก" />
                                                        <asp:Button ID="BtnBack" CssClass="btnGreen" runat="server" Text="ยกเลิก" OnClick="BtnBack_Click" />
                                                        <hr />
                                                    </td>
                                                </tr>
                                            </table>
                                        </telerik:RadPageView>
                                        <telerik:RadPageView runat="server" ID="RadPageView2">
                                            <telerik:RadGrid
                                                ID="RadGrid2"
                                                runat="server"
                                                Width="100%"
                                                Height="500px"
                                                AutoGenerateColumns="false"
                                                GridLines="None"
                                                BorderWidth="1"
                                                AllowPaging="false"
                                                AllowSorting="true">
                                                <PagerStyle Mode="NextPrevAndNumeric" />
                                                <MasterTableView
                                                    AutoGenerateColumns="False"
                                                    DataKeyNames="ID"
                                                    ClientDataKeyNames="ID"
                                                    PageSize="20"
                                                    NoMasterRecordsText="&lt; ไม่มีข้อมูล &gt;"
                                                    AllowMultiColumnSorting="false"
                                                    AllowPaging="true"
                                                    EnableHeaderContextMenu="true">
                                                    <CommandItemTemplate>
                                                        <table width="100%">
                                                            <tr>
                                                                <td>test button and set CommandItemDisplay="Top"
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </CommandItemTemplate>
                                                    <Columns>                                                     
                                                        <telerik:GridBoundColumn AutoPostBackOnFilter="true"
                                                            CurrentFilterFunction="Contains"
                                                            ShowFilterIcon="false"
                                                            FilterControlWidth="100%"
                                                            HeaderText="รายละเอียด"
                                                            DataField="LogDetail">
                                                            <HeaderStyle HorizontalAlign="Center" Wrap="False" Width="120px" />
                                                            <ItemStyle HorizontalAlign="Center" Wrap="False" />
                                                        </telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn AutoPostBackOnFilter="true"
                                                            CurrentFilterFunction="Contains"
                                                            ShowFilterIcon="false"
                                                            FilterControlWidth="100%"
                                                            HeaderText="ทำโดย"
                                                            DataField="UpFullName">
                                                            <HeaderStyle HorizontalAlign="Center" Wrap="False" Width="120px" />
                                                            <ItemStyle HorizontalAlign="Center" Wrap="False" />
                                                        </telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn AutoPostBackOnFilter="true"
                                                            CurrentFilterFunction="Contains"
                                                            ShowFilterIcon="false"
                                                            FilterControlWidth="100%"
                                                            HeaderText="วันที่"
                                                            DataField="CreateDate">
                                                            <HeaderStyle HorizontalAlign="Center" Wrap="False" Width="120px" />
                                                            <ItemStyle HorizontalAlign="Center" Wrap="False" />
                                                        </telerik:GridBoundColumn>
                                                    </Columns>
                                                </MasterTableView>
                                                <ClientSettings
                                                    AllowColumnsReorder="true"
                                                    AllowColumnHide="True"
                                                    EnableRowHoverStyle="True"
                                                    ReorderColumnsOnClient="true">
                                                    <Selecting AllowRowSelect="True" />
                                                    <Resizing AllowColumnResize="true" />
                                                    <Scrolling AllowScroll="True"
                                                        UseStaticHeaders="True"
                                                        SaveScrollPosition="True"></Scrolling>
                                                </ClientSettings>
                                            </telerik:RadGrid>
                                        </telerik:RadPageView>
                                        <telerik:RadPageView runat="server" ID="RadPageView3">
                                            <telerik:RadGrid
                                                ID="RadGrid3"
                                                runat="server"
                                                Width="100%"
                                                Height="500px"
                                                AutoGenerateColumns="false"
                                                GridLines="None"
                                                BorderWidth="1"
                                                AllowPaging="false"
                                                AllowSorting="true">
                                                <PagerStyle Mode="NextPrevAndNumeric" />
                                                <MasterTableView
                                                    AutoGenerateColumns="False"
                                                    DataKeyNames="ID"
                                                    ClientDataKeyNames="ID"
                                                    PageSize="20"
                                                    NoMasterRecordsText="&lt; ไม่มีข้อมูล &gt;"
                                                    AllowMultiColumnSorting="false"
                                                    AllowPaging="true"
                                                    EnableHeaderContextMenu="true">
                                                    <CommandItemTemplate>
                                                        <table width="100%">
                                                            <tr>
                                                                <td>test button and set CommandItemDisplay="Top"
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </CommandItemTemplate>
                                                    <Columns>                                                     
                                                        <telerik:GridBoundColumn AutoPostBackOnFilter="true"
                                                            CurrentFilterFunction="Contains"
                                                            ShowFilterIcon="false"
                                                            FilterControlWidth="100%"
                                                            HeaderText="รายละเอียด"
                                                            DataField="LogDetail">
                                                            <HeaderStyle HorizontalAlign="Center" Wrap="False" Width="120px" />
                                                            <ItemStyle HorizontalAlign="Center" Wrap="False" />
                                                        </telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn AutoPostBackOnFilter="true"
                                                            CurrentFilterFunction="Contains"
                                                            ShowFilterIcon="false"
                                                            FilterControlWidth="100%"
                                                            HeaderText="วันที่"
                                                            DataField="CreateDate">
                                                            <HeaderStyle HorizontalAlign="Center" Wrap="False" Width="120px" />
                                                            <ItemStyle HorizontalAlign="Center" Wrap="False" />
                                                        </telerik:GridBoundColumn>
                                                    </Columns>
                                                </MasterTableView>
                                                <ClientSettings
                                                    AllowColumnsReorder="true"
                                                    AllowColumnHide="True"
                                                    EnableRowHoverStyle="True"
                                                    ReorderColumnsOnClient="true">
                                                    <Selecting AllowRowSelect="True" />
                                                    <Resizing AllowColumnResize="true" />
                                                    <Scrolling AllowScroll="True"
                                                        UseStaticHeaders="True"
                                                        SaveScrollPosition="True"></Scrolling>
                                                </ClientSettings>
                                            </telerik:RadGrid>
                                        </telerik:RadPageView>
                                    </telerik:RadMultiPage>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Telerik"></telerik:RadAjaxLoadingPanel>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" EnableAJAX="true" EnablePageHeadUpdate="false" EnableViewState="false">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="RadGrid1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid1" LoadingPanelID="RadAjaxLoadingPanel1"></telerik:AjaxUpdatedControl>
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="BtnSaveUser">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="PanelFrom" LoadingPanelID="RadAjaxLoadingPanel1"></telerik:AjaxUpdatedControl>
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
        <%--<AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="RadToolBar1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="PanelAll" LoadingPanelID="RadAjaxLoadingPanel1"></telerik:AjaxUpdatedControl>
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>--%>
    </telerik:RadAjaxManager>
</asp:Content>

