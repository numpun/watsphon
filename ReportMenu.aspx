﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageInfo.master" AutoEventWireup="true" CodeFile="ReportMenu.aspx.cs" Inherits="ReportMenu" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Label ID="lblUserID" runat="server" Text="Label" Visible="false"></asp:Label>
    <asp:Label ID="lblUserLevelID" runat="server" Text="Label" Visible="false"></asp:Label>
     <table width="100%" border="0">
                    <tr>
                        <td width="25%" height="30px" align="center">
                            <asp:ImageButton
                                ID="ImageButton1" Width="250px" Height="250px"
                                runat="server"
                                ImageUrl="~/Images/icons/report.png"
                                PostBackUrl="ReportOutBoundRawData.aspx" />
                            <br />
                            <b>
                                <asp:Label
                                    ID="Label1"
                                    runat="server"
                                    Text="RawDataOutBound">
                                </asp:Label>
                            </b>
                        </td>
                        <td width="25%" height="30px" align="center">
                            <asp:ImageButton
                                ID="ImageButton2" Width="250px" Height="250px"
                                runat="server"
                                ImageUrl="~/Images/icons/report.png"
                                PostBackUrl="#" />
                            <br />
                            <b>
                                <asp:Label ID="Label2"
                                    runat="server"
                                    Text="#">
                                </asp:Label>
                            </b>
                        </td>
                        <td width="25%" height="30px" align="center">
                            <asp:ImageButton
                                ID="ImageButton3" Width="250px" Height="250px"
                                runat="server"
                                ImageUrl="~/Images/icons/report.png"
                                PostBackUrl="#" />
                            <br />
                            <b>
                                <asp:Label ID="Label3"
                                    runat="server"
                                    Text="#">
                                </asp:Label>
                            </b>
                        </td>
                        <td width="25%" height="30px" align="center">
                            <asp:ImageButton
                                ID="ImageButton4" Width="250px" Height="250px"
                                runat="server"
                                ImageUrl="~/Images/icons/report.png"
                                PostBackUrl="#" />
                            <br />
                            <b>
                                <asp:Label ID="Label4"
                                    runat="server"
                                    Text="#">
                                </asp:Label>
                            </b>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <br />
                            <br />
                            <br />
                            <br />
                        </td>
                    </tr>

                </table>
</asp:Content>

