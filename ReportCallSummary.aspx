﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageInfo.master" AutoEventWireup="true" CodeFile="ReportCallSummary.aspx.cs" Inherits="ReportCallSummary" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .btnGreen {
            width: 90%;
            background-color: #1CB94E;
            border: none;
            outline: none;
            color: #fff;
            font-size: 14px;
            font-weight: normal;
            padding: 14px 0;
            border-radius: 2px;
            text-transform: uppercase;
        }

        .btnBlue {
            width: 90%;
            background-color: #3C59A8;
            border: none;
            outline: none;
            color: #fff;
            font-size: 14px;
            font-weight: normal;
            padding: 14px 0;
            border-radius: 2px;
            text-transform: uppercase;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <telerik:RadScriptManager ID="RadScriptManager1" runat="server"></telerik:RadScriptManager>
    <telerik:RadSkinManager ID="RadSkinManager1" runat="server" Skin="Web20"></telerik:RadSkinManager>
    <asp:Label ID="lblUserID" runat="server" Text="Label" Visible="false"></asp:Label>
    <asp:Label ID="lblUserLevelID" runat="server" Text="Label" Visible="false"></asp:Label>
    <table width="100%" border="0">
        <tr>
            <td>
                <br />
            </td>
        </tr>
        <tr>
            <td width="90px">ตั้งแต่วันที่ :</td>
            <td width="200px" align="left">
                <telerik:RadDatePicker ID="rdpDateS"
                    runat="server"
                    Width="170px"
                    ValidationGroup="rp"
                    DateInput-EmptyMessage="..Select date.." DateInput-DateFormat="dd/MM/yyyy"
                    ZIndex="10000">
                    <Calendar ID="Calendar3" runat="server">
                        <SpecialDays>
                            <telerik:RadCalendarDay Repeatable="Today">
                            </telerik:RadCalendarDay>
                        </SpecialDays>
                    </Calendar>
                </telerik:RadDatePicker>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1"
                    runat="server"
                    ControlToValidate="rdpDateS"
                    SetFocusOnError="True"
                    Display="Dynamic"
                    ErrorMessage="*"
                    ValidationGroup="rp"
                    ForeColor="#FF3300">
                </asp:RequiredFieldValidator>
            </td>
            <td width="50px">ถึง :</td>
            <td width="210px" align="left">
                <telerik:RadDatePicker ID="rdpDateE"
                    runat="server"
                    Width="170px"
                    ValidationGroup="rp"
                    DateInput-EmptyMessage="..Select date.." DateInput-DateFormat="dd/MM/yyyy"
                    ZIndex="10000">
                    <Calendar ID="Calendar2" runat="server">
                        <SpecialDays>
                            <telerik:RadCalendarDay Repeatable="Today">
                            </telerik:RadCalendarDay>
                        </SpecialDays>
                    </Calendar>
                </telerik:RadDatePicker>

                <asp:RequiredFieldValidator ID="RequiredFieldValidator111"
                    runat="server"
                    ControlToValidate="rdpDateE"
                    SetFocusOnError="True"
                    Display="Dynamic"
                    ErrorMessage="*"
                    ValidationGroup="rp"
                    ForeColor="#FF3300">
                </asp:RequiredFieldValidator>

            </td>
            <td>
                <asp:Button ID="BtnFindReport" 
                    OnClick="BtnFindReport_Click"
                    CssClass="btnBlue"
                    runat="server"
                    Text="ดึงรายงาน" />
            </td>
        </tr>
        <tr>
            <td colspan="5">
                <br />
            </td>
        </tr>
    </table>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server"></telerik:RadAjaxManager>
</asp:Content>

