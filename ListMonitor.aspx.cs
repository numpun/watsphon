﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Data;

public partial class ListMonitor : System.Web.UI.Page
{
    GetExecution GE = new GetExecution();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            if (Session["UserID"] == null)
            {
                Response.Redirect("~/Default.aspx");
            }

            lblUserLavel.Text = Session["UserLevelID"].ToString();
            lblUserID.Text = Session["UserID"].ToString();

            rdpDateS.SelectedDate = DateTime.Today;
            rdpDateE.SelectedDate = DateTime.Today;
            LoadControl();
            GETAPPLIST(ref cmbAppStatus);
        }
    }

    public void LoadControl()
    {
        try
        {
            if (lblUserLavel.Text.Trim() == "Q")
            {
                //RadPageView2.Visible = false;
            }
        }
        catch (Exception ex)
        {

        }
    }

   
 
    private void GETAPPLIST(ref RadComboBox obj)
    {
        try
        {
            #region
            string[] arrDateS = rdpDateS.DateInput.SelectedDate.Value.ToShortDateString().Split('/');
            string[] arrDateE = rdpDateE.DateInput.SelectedDate.Value.ToShortDateString().Split('/');
            //'2014-12-03 00:00:00'
            string DateStart = "";
            string DateEnd = "";

            string chkMounthS = arrDateS[1];
            string chkDayS = arrDateS[0];
            string chkMounthE = arrDateE[1];
            string chkDayE = arrDateE[0];

            string GetmounthS = "";
            string GetdayS = "";
            string GetmounthE = "";
            string GetdayE = "";


            if (chkMounthS.Length < 2)
            {
                GetmounthS = "0" + arrDateS[1];
            }
            else
            {
                GetmounthS = arrDateS[1];
            }

            if (chkDayS.Length < 2)
            {
                GetdayS = "0" + arrDateS[0];
            }
            else
            {
                GetdayS = arrDateS[0];
            }

            if (chkMounthE.Length < 2)
            {
                GetmounthE = "0" + arrDateE[1];
            }
            else
            {
                GetmounthE = arrDateE[1];
            }

            if (chkDayE.Length < 2)
            {
                GetdayE = "0" + arrDateE[0];
            }
            else
            {
                GetdayE = arrDateE[0];
            }

            DateStart = arrDateS[2] + "-" + GetmounthS + "-" + GetdayS + " 00:00:00";
            DateEnd = arrDateE[2] + "-" + GetmounthE + "-" + GetdayE + " 23:59:59";
            #endregion


            //RadAjaxManager1.Alert(DateStart + "<>" + DateEnd);

            string strSQL = " SELECT App.AppID, App.AppName, COUNT(1) as APPSUM, App.Ordering ";
            strSQL += " FROM TblCustomer_master C ";
            strSQL += " INNER JOIN TblAppStatus App ON C.AppStatus=App.AppID ";
            strSQL += " INNER JOIN TblUser U ON C.OwnerID=U.UserID ";
            strSQL += " WHERE C.StatusID='2' ";
            strSQL += " AND CONVERT(VARCHAR(24),C.ClosedDate,120) BETWEEN '" + DateStart + "' AND '" + DateEnd + "' ";

            strSQL += " GROUP BY App.AppName, App.AppID, App.Ordering ";
            strSQL += " ORDER BY App.Ordering ";

            DataTable dt = GE.SELECT_Executeion(strSQL);
            if (dt.Rows.Count > 0)
            {
                int SumAll = 0;
                foreach (DataRow dr in dt.Rows)
                {
                    SumAll += Convert.ToInt32(dr["APPSUM"]);
                }
                DataRow newrow = dt.NewRow();
                newrow["APPID"] = "0";
                newrow["APPNAME"] = "ทั้งหมด (รวม)";
                newrow["APPSUM"] = SumAll.ToString();
                dt.Rows.Add(newrow);
                dt.AcceptChanges();

                bool isSubmit = false;
                foreach (DataRow dr in dt.Rows)
                {
                    RadComboBoxItem item = new RadComboBoxItem();
                    //item.Text = dr["source_name"].ToString();
                    item.Value = dr["AppID"].ToString();
                    item.Text = dr["AppName"].ToString();
                    item.ToolTip = dr["APPSUM"].ToString();

                    item.Attributes.Add("APPSTATUSNAME", dr["AppName"].ToString());
                    item.Attributes.Add("APPSUM", dr["APPSUM"].ToString());
                    obj.Items.Add(item);
                    item.DataBind();
                    if (dr["AppID"].ToString() == "2")
                    {
                        isSubmit = true;
                    }
                }
                GETCUSLIST();
            }
        }
        catch (Exception ex)
        {
            RadAjaxManager1.Alert(ex.Message);
        }
    }

    private void GETCUSLIST()
    {
        try
        {
            if (rdpDateS.SelectedDate != null && rdpDateE.SelectedDate != null)
            {
                if (rdpDateS.SelectedDate.Value.ToString().Length > 0 && rdpDateE.SelectedDate.Value.ToString().Length > 0)
                {
                    #region
                    string[] arrDateS = rdpDateS.DateInput.SelectedDate.Value.ToShortDateString().Split('/');
                    string[] arrDateE = rdpDateE.DateInput.SelectedDate.Value.ToShortDateString().Split('/');
                    //'2014-12-03 00:00:00'
                    string DateStart = "";
                    string DateEnd = "";

                    string chkMounthS = arrDateS[1];
                    string chkDayS = arrDateS[0];
                    string chkMounthE = arrDateE[1];
                    string chkDayE = arrDateE[0];

                    string GetmounthS = "";
                    string GetdayS = "";
                    string GetmounthE = "";
                    string GetdayE = "";


                    if (chkMounthS.Length < 2)
                    {
                        GetmounthS = "0" + arrDateS[1];
                    }
                    else
                    {
                        GetmounthS = arrDateS[1];
                    }

                    if (chkDayS.Length < 2)
                    {
                        GetdayS = "0" + arrDateS[0];
                    }
                    else
                    {
                        GetdayS = arrDateS[0];
                    }

                    if (chkMounthE.Length < 2)
                    {
                        GetmounthE = "0" + arrDateE[1];
                    }
                    else
                    {
                        GetmounthE = arrDateE[1];
                    }

                    if (chkDayE.Length < 2)
                    {
                        GetdayE = "0" + arrDateE[0];
                    }
                    else
                    {
                        GetdayE = arrDateE[0];
                    }

                    DateStart = arrDateS[2] + "-" + GetmounthS + "-" + GetdayS + " 00:00:00";
                    DateEnd = arrDateE[2] + "-" + GetmounthE + "-" + GetdayE + " 23:59:59";
                    #endregion

                    //RadAjaxManager1.Alert(DateStart + "<>" + DateEnd);

                    DataTable dt = GetData(cmbAppStatus.SelectedValue, DateStart, DateEnd);
                    gvList.DataSource = dt;
                    gvList.DataBind();
                }
                else
                {
                    RadAjaxManager1.Alert("กรุณาระบุวันที่!!");
                    return;
                }
            }
            else
            {
                RadAjaxManager1.Alert("กรุณาระบุวันที่ ที่ต้องการออกรายงาน!!");
            }
        }
        catch (Exception ex)
        {
            RadAjaxManager1.Alert(ex.Message);
        }
    }

    private DataTable GetData(string AppStatus, string DateStart, string DateEnd)
    {
        try
        {
            string strSQL = " SELECT C.CusID AS Sequence, C.Comment, C.ClosedDate, U.Fname+' '+U.Lname AS AgentClosedName  ";
            strSQL += " , c.*, c.Title+' '+c.Name AS CustomerName, c.HomePhone as SHomePhone, c.mobile as SMobile, c.OfficePhone as Soffice_phone, C.LastCallDate ";
            strSQL += " , UQ.Fname+' '+UQ.Lname AS AgentQCName ";
            strSQL += " FROM TblCustomer_master C ";
            strSQL += " LEFT JOIN TblUser UQ ON UQ.UserID = C.QCID ";
            strSQL += " LEFT JOIN TblUser U ON U.UserID = C.ClosedID ";
            strSQL += " WHERE C.StatusID = 2 ";
            strSQL += " AND CONVERT(VARCHAR(24),C.ClosedDate,120) BETWEEN '" + DateStart + "' and '" + DateEnd + "' ";

            if (cmbAppStatus.SelectedValue != "0")
                strSQL += " AND C.AppStatus = '" + AppStatus + "' ";

            strSQL += " Order by C.ClosedDate DESC ";

            DataTable dt = GE.SELECT_Executeion(strSQL);
            return dt;
        }
        catch (Exception ex)
        {
            RadAjaxManager1.Alert(ex.Message);
            return null;
        }
    }


    protected void rdpDateS_SelectedDateChanged(object sender, Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs e)
    {
        cmbAppStatus.ClearSelection();
        cmbAppStatus.Text = string.Empty;
        cmbAppStatus.Items.Clear();

        gvList.DataSource = new object[0];
        gvList.DataBind();
        GETAPPLIST(ref cmbAppStatus);
    }
    protected void rdpDateE_SelectedDateChanged(object sender, Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs e)
    {

        cmbAppStatus.ClearSelection();
        cmbAppStatus.Text = string.Empty;
        cmbAppStatus.Items.Clear();

        gvList.DataSource = new object[0];
        gvList.DataBind();
        GETAPPLIST(ref cmbAppStatus);
    }
    protected void cmbAppStatus_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {        
        gvList.DataSource = null;
        gvList.DataBind();
        GETCUSLIST();
    }
    protected void gvList_ItemCommand(object sender, GridCommandEventArgs e)
    {
        try
        {
            if (e.Item is GridDataItem)
            {

                if (e.CommandName == "Redirect")
                {
                    GridDataItem item = (GridDataItem)e.Item;
                    LinkButton lnkbtn = (LinkButton)item["TempCol"].FindControl("LinkButton1");
                    HiddenField hidf = (HiddenField)item["TempCol"].FindControl("HiddenField1");
                    Response.Redirect("FrmData.aspx?CusID=" + hidf.Value);
                }
            }
            GETCUSLIST();
        }
        catch (Exception ex)
        {
            RadAjaxManager1.Alert(ex.Message);
        }
    }


    protected void gvList_ColumnCreated(object sender, GridColumnCreatedEventArgs e)
    {
        if (lblUserLavel.Text.Trim() == "Q")
        {

        }
        else if (lblUserLavel.Text.Trim() == "S")
        {

        }
        else
        {
           
        }
    }
    protected void gvList_ItemCreated(object sender, GridItemEventArgs e)
    {
        if (e.Item is GridDataItem)
        {
            string KeySequence = (e.Item as GridDataItem).GetDataKeyValue("Sequence").ToString();
        }
    }
    protected void gvList_ItemDataBound(object sender, GridItemEventArgs e)
    {
        if (e.Item.ItemType == GridItemType.Item || e.Item.ItemType == GridItemType.AlternatingItem)
        {
            if (e.Item is GridDataItem)
            {
                string isSubmit = (e.Item as GridDataItem)["AppStatus"].Text.Trim();
                if (isSubmit == "3")
                    (e.Item as GridDataItem)["AppStatus"].ForeColor = System.Drawing.Color.Green;
                if (isSubmit == "4")
                    (e.Item as GridDataItem)["AppStatus"].ForeColor = System.Drawing.Color.Red;
                if (isSubmit == "2")
                    (e.Item as GridDataItem)["AppStatus"].ForeColor = System.Drawing.Color.Blue;
            }
        }
    }

   
}