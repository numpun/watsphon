﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageInfo.master" AutoEventWireup="true" CodeFile="ListMonitor.aspx.cs" Inherits="ListMonitor" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Label ID="lblUserLavel" runat="server" Text="" Visible="False"></asp:Label>
    <asp:Label ID="lblUserID" runat="server" Text="" Visible="False"></asp:Label>
    <table width="100%">
        <tr>
            <td>
                <asp:Panel ID="pnlFilter" runat="server" HorizontalAlign="Left">
                    <table>
                        <tr>
                            <td width="100px">วันที่เริ่มต้น
                            </td>
                            <td width="200px">
                                <telerik:RadDatePicker ID="rdpDateS"
                                    runat="server"
                                    OnSelectedDateChanged="rdpDateS_SelectedDateChanged"
                                    Width="130px"
                                    DateInput-EmptyMessage="..Select date.." DateInput-DateFormat="dd/MM/yyyy"
                                    ZIndex="10000" AutoPostBack="True">
                                    <Calendar ID="Calendar1" runat="server">
                                        <SpecialDays>
                                            <telerik:RadCalendarDay Repeatable="Today">
                                            </telerik:RadCalendarDay>
                                        </SpecialDays>
                                    </Calendar>
                                </telerik:RadDatePicker>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1"
                                    runat="server"
                                    ControlToValidate="rdpDateS"
                                    SetFocusOnError="True"
                                    Display="Dynamic"
                                    ErrorMessage="*"
                                    ForeColor="#FF3300">
                                </asp:RequiredFieldValidator>
                                วันที่สิ้นสุด
                                <telerik:RadDatePicker ID="rdpDateE"
                                    runat="server"
                                    OnSelectedDateChanged="rdpDateE_SelectedDateChanged"
                                    Width="125px"
                                    DateInput-EmptyMessage="..Select date.." DateInput-DateFormat="dd/MM/yyyy"
                                    ZIndex="10000" AutoPostBack="True">
                                    <Calendar ID="Calendar2" runat="server">
                                        <SpecialDays>
                                            <telerik:RadCalendarDay Repeatable="Today">
                                            </telerik:RadCalendarDay>
                                        </SpecialDays>
                                    </Calendar>
                                </telerik:RadDatePicker>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2"
                                    runat="server"
                                    ControlToValidate="rdpDateE"
                                    SetFocusOnError="True"
                                    Display="Dynamic"
                                    ErrorMessage="*"
                                    ForeColor="#FF3300">
                                </asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6">
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td width="100px">สถานะ :</td>
                            <td align="left">
                                <telerik:RadComboBox ID="cmbAppStatus" runat="server"
                                    OnSelectedIndexChanged="cmbAppStatus_SelectedIndexChanged"
                                    LoadingMessage="กำลังโหลดข้อมูล ..."
                                    MarkFirstMatch="false"
                                    Width="360px"
                                    DropDownWidth="360px"
                                    HighlightTemplatedItems="true"
                                    AutoPostBack="True">
                                    <HeaderTemplate>
                                        <table style="width: 300px" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td style="width: 250px;" align="center">สถานะ</td>
                                                <td style="width: 50px;" align="center">จำนวน(ลค.)</td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <table style="width: 300px" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td style="width: 200px;">
                                                    <%# DataBinder.Eval(Container, "Attributes['APPSTATUSNAME']")%>
                                                </td>
                                                <td style="width: 50px;" align="right">
                                                    <%# DataBinder.Eval(Container, "Attributes['APPSUM']")%>
                                                </td>
                                            </tr>
                                        </table>
                                    </ItemTemplate>
                                </telerik:RadComboBox>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                <br />
                <asp:Panel ID="PanelHead" runat="server">
                    <telerik:RadTabStrip ID="RadTabStrip1" runat="server" SelectedIndex="0" MultiPageID="RadMultiPage1" CausesValidation="False">
                        <Tabs>
                            <telerik:RadTab ImageUrl="Images/icons/success.png" Text="Success">
                            </telerik:RadTab>
                           <%-- <telerik:RadTab ImageUrl="Images/icons/monitor.png" Text="Monitor">
                            </telerik:RadTab>--%>
                        </Tabs>
                    </telerik:RadTabStrip>
                </asp:Panel>
                <asp:Panel ID="PanelListMonitor" runat="server">
                    <telerik:RadMultiPage runat="server" ID="RadMultiPage1" SelectedIndex="0" Width="100%">
                        <telerik:RadPageView runat="server" ID="RadPageView1">
                            <telerik:RadGrid
                                ID="gvList"
                                runat="server"
                                Width="90%"
                                Height="450px"
                                AutoGenerateColumns="false"
                                GridLines="None"
                                BorderWidth="0"
                                AllowSorting="true"
                                Style="outline: none"
                                AllowFilteringByColumn="True"
                                OnColumnCreated="gvList_ColumnCreated"
                                OnItemCommand="gvList_ItemCommand"
                                OnItemCreated="gvList_ItemCreated"
                                OnItemDataBound="gvList_ItemDataBound"
                                AllowMultiRowSelection="true">
                                <PagerStyle Mode="NextPrevAndNumeric" />
                                <MasterTableView
                                    AutoGenerateColumns="False"
                                    DataKeyNames="Sequence"
                                    ClientDataKeyNames="Sequence"
                                    NoMasterRecordsText="&lt; ไม่มีข้อมูล &gt;"
                                    CommandItemDisplay="none"
                                    AllowMultiColumnSorting="True">
                                    <CommandItemTemplate>
                                        <table width="100%">
                                            <tr>
                                                <td>test button and set CommandItemDisplay="Top"
                                                </td>
                                            </tr>
                                        </table>
                                    </CommandItemTemplate>

                                    <Columns>

                                        <%--<telerik:GridClientSelectColumn UniqueName="ClientSelectColumn">
                                            <HeaderStyle Width="32px" HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Center" />
                                        </telerik:GridClientSelectColumn>--%>
                                        <%--0--%>
                                        <telerik:GridTemplateColumn
                                            AutoPostBackOnFilter="true"
                                            CurrentFilterFunction="Contains"
                                            ShowFilterIcon="false"
                                            SortExpression="D_productName"
                                            UniqueName="TempCol"
                                            AllowFiltering="false">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="LinkButton1" CommandName="Redirect" Text='<img src="Images/icons/view.png">' runat="server"></asp:LinkButton>
                                                <asp:HiddenField ID="HiddenField1" runat="server" Value='<%# Eval("Sequence") %>' />
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" Width="50px" />
                                            <ItemStyle HorizontalAlign="Center" Font-Bold="true" Wrap="true" ForeColor="Red" />
                                        </telerik:GridTemplateColumn>
                                        <%--1--%>
                                        <telerik:GridTemplateColumn
                                            AutoPostBackOnFilter="true"
                                            CurrentFilterFunction="Contains"
                                            ShowFilterIcon="false"
                                            FilterControlWidth="100%"
                                            DataField="Sequence"
                                            UniqueName="Sequence"
                                            SortExpression="Sequence"
                                            HeaderText="รหัสลูกค้า" ItemStyle-Width="80px" HeaderStyle-Width="80px">
                                            <ItemTemplate>
                                                <asp:HyperLink ID="hlCusID"
                                                    runat="server"
                                                    Text='<%# Eval("Sequence") %>' ToolTip='<%# Eval("Sequence") %>'>
                                                </asp:HyperLink>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Center" Wrap="False" />
                                        </telerik:GridTemplateColumn>
                                        <%--2--%>
                                        <telerik:GridBoundColumn AutoPostBackOnFilter="true"
                                            CurrentFilterFunction="Contains"
                                            ShowFilterIcon="false"
                                            FilterControlWidth="100%"
                                            HeaderText="ชื่อ - นามสกุลลูกค้า"
                                            DataField="CustomerName"
                                            UniqueName="CustomerName">
                                            <HeaderStyle HorizontalAlign="Center" Wrap="False" Width="90" />
                                            <ItemStyle HorizontalAlign="Center" Wrap="False" />
                                        </telerik:GridBoundColumn>
                                        <%--3--%>
                                        <telerik:GridBoundColumn AutoPostBackOnFilter="true"
                                            CurrentFilterFunction="Contains"
                                            ShowFilterIcon="false"
                                            FilterControlWidth="100%"
                                            HeaderText="วันที่ปิดงาน"
                                            DataField="ClosedDate"
                                            UniqueName="ClosedDate">
                                            <HeaderStyle HorizontalAlign="Center" Wrap="False" Width="90" />
                                            <ItemStyle HorizontalAlign="Center" Wrap="False" />
                                        </telerik:GridBoundColumn>
                                        <%--4--%>
                                        <telerik:GridBoundColumn AutoPostBackOnFilter="true"
                                            CurrentFilterFunction="Contains"
                                            ShowFilterIcon="false"
                                            FilterControlWidth="100%"
                                            HeaderText="พนักงานที่ปิดงาน"
                                            DataField="AgentClosedName"
                                            UniqueName="AgentClosedName">
                                            <HeaderStyle HorizontalAlign="Center" Wrap="False" Width="90" />
                                            <ItemStyle HorizontalAlign="Center" Wrap="False" />
                                        </telerik:GridBoundColumn>
                                        <%--5--%>
                                        <telerik:GridBoundColumn AutoPostBackOnFilter="true"
                                            CurrentFilterFunction="Contains"
                                            ShowFilterIcon="false"
                                            FilterControlWidth="100%"
                                            HeaderText="หมายเหตุล่าสุด"
                                            DataField="Comment"
                                            UniqueName="Comment">
                                            <HeaderStyle HorizontalAlign="Center" Wrap="False" Width="90" />
                                            <ItemStyle HorizontalAlign="Center" Wrap="False" />
                                        </telerik:GridBoundColumn>
                                        <%--6--%>
                                        <telerik:GridBoundColumn AutoPostBackOnFilter="true"
                                            CurrentFilterFunction="Contains"
                                            ShowFilterIcon="false"
                                            FilterControlWidth="100%"
                                            HeaderText="ผลQC"
                                            DataField="AppStatus"
                                            UniqueName="AppStatus">
                                            <HeaderStyle HorizontalAlign="Center" Wrap="False" Width="50" />
                                            <ItemStyle HorizontalAlign="Center" Wrap="False" />
                                        </telerik:GridBoundColumn>
                                    </Columns>
                                </MasterTableView>
                                <ClientSettings
                                    AllowColumnsReorder="true"
                                    AllowColumnHide="True"
                                    EnableRowHoverStyle="True"
                                    ReorderColumnsOnClient="true">
                                    <Selecting AllowRowSelect="True" />
                                    <Resizing AllowColumnResize="true" />
                                    <Scrolling AllowScroll="True" UseStaticHeaders="false" SaveScrollPosition="True"></Scrolling>
                                </ClientSettings>
                            </telerik:RadGrid>
                        </telerik:RadPageView>
                        
                    </telerik:RadMultiPage>
                </asp:Panel>
            </td>
        </tr>
    </table>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server"></telerik:RadAjaxLoadingPanel>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" EnableAJAX="true" EnablePageHeadUpdate="false" EnableViewState="false">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="cmbAppStatus">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="gvList" LoadingPanelID="RadAjaxLoadingPanel1"></telerik:AjaxUpdatedControl>
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
</asp:Content>

