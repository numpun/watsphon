﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Data;
using System.Text;

public partial class TranferList : System.Web.UI.Page
{
    GetExecution GE = new GetExecution();
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!this.IsPostBack)
            {
                if (Session["UserID"] == null)
                {
                    Response.Redirect("~/Default.aspx");
                }

                lblUserID.Text = Session["UserID"].ToString();
                lblUserLevelID.Text = Session["UserLevelID"].ToString();
                btnAssign.Enabled = false;
                LoadDataSource();
                LoadControl();
                

            }
        }
        catch (Exception ex)
        {
            RadAjaxManager1.Alert(ex.Message);
        }
    }

    public void LoadControl()
    {
        try
        {
            PanelList.Visible = false;
            RadGrid1.DataSource = new Object[0];
            RadGrid1.DataBind();
            RadGrid2.DataSource = new Object[0];
            RadGrid2.DataBind();
        }
        catch (Exception ex)
        {
            RadAjaxManager1.Alert(ex.Message);
        }
    }

    public void LoadDataSource()
    {
        try
        {
            string strSQL = " SELECT D.SourceID, D.SourceName, D.TotalSource ";
            strSQL += ", CASE ";
            strSQL += " (SELECT COUNT(1) CusID FROM TblCustomer_master CC WHERE CC.OwnerID = '"+ lblUserID.Text.Trim() +"' AND CC.SourceID = D.SourceID) ";
            strSQL += " WHEN 0 THEN '0' ";
            strSQL += " ELSE (SELECT COUNT(1) CusID FROM TblCustomer_master CC WHERE CC.OwnerID = '"+ lblUserID.Text.Trim() +"' AND CC.SourceID = D.SourceID) ";
            strSQL += " END AS DataSUM ";
            strSQL += " FROM TblCustomer_master C ";
            strSQL += " INNER JOIN TblDataSource D on C.SourceID=D.SourceID ";
            strSQL += " WHERE D.Active = 'Y' ";
            strSQL += " GROUP BY D.SourceName, D.SourceID, D.TotalSource  ";
            strSQL += " Order by D.SourceID DESC  ";

            DataTable dt = GE.SELECT_Executeion(strSQL);
            if (dt.Rows.Count > 0)
            {
                rcbSource.DataSource = dt;
                rcbSource.DataBind();
            }
        }
        catch (Exception ex)
        {
            RadAjaxManager1.Alert(ex.Message);
        }
    }
    protected void rcbSource_ItemDataBound(object sender, RadComboBoxItemEventArgs e)
    {
        e.Item.Text = ((DataRowView)e.Item.DataItem)["SourceName"].ToString();
        e.Item.Value = ((DataRowView)e.Item.DataItem)["SourceID"].ToString();
    }

    private DataTable ListUser(string UserLevelID)
    {
        try
        {
            string strSP = " ";
            if (rcbSource.SelectedIndex >= 0)
            {
                strSP += " AND C.SourceID  ='" + rcbSource.SelectedValue + "' ";
            }
            else
            {
                RadAjaxManager1.Alert("กรุณาเลือก DataSource");
                return null;
            }

            string strSQL = " Select UserID as Sequence  "
                + ", FName + ' ' + LName as UserName, NName "
                + ", UserLevelID as UserLevel "
                + ", (select COUNT(1) from TblCustomer_master c where u.UserID=c.OwnerID " + strSP + " ) as sta_total "
                + ", (select COUNT(1) from TblCustomer_master c where u.UserID=c.OwnerID and c.StatusID = '1' " + strSP + " ) as sta_new "
                + ", (select COUNT(1) from TblCustomer_master c where u.UserID=c.OwnerID and c.StatusID = '2' " + strSP + ") as sta_success "
                + ", (select COUNT(1) from TblCustomer_master c where u.UserID=c.OwnerID and c.StatusID = '3' " + strSP + ") as sta_Refuse "
                + ", (select COUNT(1) from TblCustomer_master c where u.UserID=c.OwnerID and c.StatusID = '4' " + strSP + ") as sta_WrongNumber "
                + ", (select COUNT(1) from TblCustomer_master c where u.UserID=c.OwnerID and c.StatusID = '5' " + strSP + ") as sta_Duplicated "
                + ", (select COUNT(1) from TblCustomer_master c where u.UserID=c.OwnerID and c.StatusID = '6' " + strSP + ") as sta_Recheck "
                + ", (select COUNT(1) from TblCustomer_master c where u.UserID=c.OwnerID and c.StatusID = '7' " + strSP + ") as sta_Appoint "
                + ", (select COUNT(1) from TblCustomer_master c where u.UserID=c.OwnerID and c.StatusID = '8' " + strSP + ") as sta_Business "
                + ", (select COUNT(1) from TblCustomer_master c where u.UserID=c.OwnerID and c.StatusID = '9' " + strSP + ") as sta_NoContact "
                + ", (select COUNT(1) from TblCustomer_master c where u.UserID=c.OwnerID and c.StatusID = '10' " + strSP + ") as sta_Error "
                + " From TblUser u "
                + " Where Active='Y' AND UserLevelID = '"+ UserLevelID +"' ";

            DataTable dt = GE.SELECT_Executeion(strSQL);
            return dt;
        }
        catch (Exception ex)
        {
            RadAjaxManager1.Alert(ex.Message);
            return null;
        }
    }
    private void GetListUserS()
    {
        try
        {
            if (rcbUserS.SelectedValue != "")
            {
                DataTable dt = ListUser(rcbUserS.SelectedValue);
                if (dt.Rows.Count > 0)
                {
                    RadGrid1.DataSource = dt;
                    RadGrid1.DataBind();
                }
                else
                {
                    RadGrid1.DataSource = new Object[0];
                    RadGrid1.DataBind();
                }
            }
            else
            {
                RadGrid1.DataSource = new Object[0];
                RadGrid1.DataBind();
            }
        }
        catch (Exception ex)
        {
            RadAjaxManager1.Alert(ex.Message);
        }
    }

    private void GetListUserE()
    {
        try
        {
            if (rcbUserE.SelectedValue != "")
            {
                DataTable dt = ListUser(rcbUserE.SelectedValue);
                if (dt.Rows.Count > 0)
                {
                    RadGrid2.DataSource = dt;
                    RadGrid2.DataBind();
                }
                else
                {
                    RadGrid2.DataSource = new Object[0];
                    RadGrid2.DataBind();
                }
            }
            else
            {
                RadGrid2.DataSource = new Object[0];
                RadGrid2.DataBind();
            }
        }
        catch (Exception ex)
        {
            RadAjaxManager1.Alert(ex.Message);
        }
    }

    protected void rcbSource_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        try
        {
            PanelList.Visible = true;
            GetListUserS();
            GetListUserE();
            ClearControl();
        }
        catch (Exception ex)
        {
            RadAjaxManager1.Alert(ex.Message);
        }
    }
    protected void rcbUserS_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        try
        {
            GetListUserS();
            ClearControl();
        }
        catch (Exception ex)
        {
            RadAjaxManager1.Alert(ex.Message);
        }
    }
    protected void rcbUserE_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        try
        {
            GetListUserE();
            ClearControl();
        }
        catch (Exception ex)
        {
            RadAjaxManager1.Alert(ex.Message);
        }
    }
    protected void BtnCal_Click(object sender, EventArgs e)
    {
        try
        {
            ClearControl();
            int user_check = 0;
            int des_check = 0;
            foreach (GridDataItem data in RadGrid1.MasterTableView.GetSelectedItems())
            {
                user_check = user_check + 1;
            }
            if (user_check == 0)
            {
                RadAjaxManager1.Alert("!!! กรุณาเลือก User ต้นทาง!!!");
                ClearControl();
                rcbUserS.Focus();
                return;
            }

            foreach (GridDataItem data in RadGrid2.MasterTableView.GetSelectedItems())
            {
                des_check = des_check + 1;
            }
            if (des_check == 0)
            {
                RadAjaxManager1.Alert("!!! กรุณาเลือก User ปลายทาง !!!");
                ClearControl();
                rcbUserE.Focus();
                return;
            }

            int iTotalEmpUser = des_check;
            string sta_total = "0";
            string sta_new = "0";
            string sta_success = "0";
            string sta_business = "0";
            string sta_refuse= "0";
            string sta_duplicated = "0";
            string sta_recheck= "0";
            string sta_appoint = "0";
            string sta_wrongn = "0";
            string sta_Nocontact = "0";



            if (CALCULATOR_USERSOURCE(ref  sta_total, ref  sta_new, ref  sta_success, ref  sta_business, ref  sta_refuse, ref  sta_duplicated, ref  sta_recheck, ref  sta_appoint, ref  sta_wrongn, ref  sta_Nocontact) == true)
            {
                //double iTotalHotLead = Math.Floor(Convert.ToDouble(sta_total) / iTotalEmpUser);

                double iTota_new = Math.Floor(Convert.ToDouble(sta_new) / iTotalEmpUser);
                //double iTota_Success = Math.Floor(Convert.ToDouble(sta_success) / iTotalEmpUser);
                //double iTota_Refuse = Math.Floor(Convert.ToDouble(sta_refuse) / iTotalEmpUser);
                double iTota_Duplicated = Math.Floor(Convert.ToDouble(sta_duplicated) / iTotalEmpUser);
                //double iTota_Recheck = Math.Floor(Convert.ToDouble(sta_recheck) / iTotalEmpUser);
                //double iTota_Wrongn = Math.Floor(Convert.ToDouble(sta_wrongn) / iTotalEmpUser);
                //double iTota_Appoint = Math.Floor(Convert.ToDouble(sta_appoint) / iTotalEmpUser);
                double iTota_Business = Math.Floor(Convert.ToDouble(sta_business) / iTotalEmpUser);
                double iTota_Nocontact = Math.Floor(Convert.ToDouble(sta_Nocontact) / iTotalEmpUser);


                //---- view
                txtNew.Text = sta_new;
                txtSuccess.Text = sta_success;
                txtRefuse.Text = sta_refuse;
                txtDuplicateed.Text = sta_duplicated;
                txtRecheck.Text = sta_recheck;
                txtWrongN.Text = sta_wrongn;
                txtAppoint.Text = sta_appoint;
                txtBusiness.Text = sta_business;
                txtNoContact.Text = sta_Nocontact;


                //---- cal per user
                txtEmp.Text = iTotalEmpUser.ToString();
                txtAssignNew.Text = iTota_new.ToString();
                txtAssignBusy.Text = iTota_Business.ToString();
                txtAssignPresented.Text = iTota_Duplicated.ToString();
                txtAssignNoContact.Text = iTota_Nocontact.ToString();

                double iComit = (iTota_new + iTota_Business + iTota_Duplicated + iTota_Nocontact) * iTotalEmpUser;
                txtTotal.Text = sta_total;
                txtComit.Text = iComit.ToString();
                txtRemain.Text = (Math.Floor(Convert.ToDouble(sta_total) - iComit)).ToString();

                btnAssign.Enabled = (txtEmp.Text.Trim() != "0" && txtTotal.Text.Trim() != "0") ? true : false;
            }
        }
        catch (Exception ex)
        {
            ClearControl();
            RadAjaxManager1.Alert(ex.Message);
        }
    }

    private void ClearControl()
    {
        try
        {
            txtAssignNew.Text = "0";
            txtAssignBusy.Text = "0";
            txtAssignPresented.Text = "0";
            txtAssignNoContact.Text = "0";

            txtNew.Text = "0";
            txtSuccess.Text = "0";
            txtRefuse.Text = "0";
            txtDuplicateed.Text = "0";
            txtRecheck.Text = "0";
            txtWrongN.Text = "0";
            txtAppoint.Text = "0";
            txtBusiness.Text = "0";
            txtNoContact.Text = "0";

            txtTotal.Text = "0";
            txtEmp.Text = "0";
            txtComit.Text = "0";
            txtRemain.Text = "0";
            
            chkNew.Checked = false;
            chkBusy.Checked = false;
            chkPresented.Checked = false;
            chkNoContact.Checked = false;
            btnAssign.Enabled = false;
        }
        catch (Exception ex)
        {
            RadAjaxManager1.Alert(ex.Message);
        }
    }

    private bool CALCULATOR_USERSOURCE(ref string sta_total, ref string sta_new, ref string sta_success, ref string sta_business, ref string sta_refuse, ref string sta_duplicated, ref string sta_recheck, ref string sta_appoint, ref string sta_wrongn, ref string sta_Nocontact)
    {
        bool bReturn;

        int iTota_total = 0;
        int iTota_new = 0;
        int iTota_success = 0;
        int iTota_business = 0;
        int iTota_refuse = 0;
        int iTota_duplicated = 0;
        int iTota_recheck = 0;
        int iTota_appoint = 0;
        int iTota_wrongn = 0;
        int iTota_Nocontact = 0;


        try
        {
            foreach (GridDataItem data in RadGrid1.MasterTableView.GetSelectedItems())
            {
                string seq = data.OwnerTableView.DataKeyValues[data.ItemIndex]["Sequence"].ToString();
                int i_total = Convert.ToInt32(data.OwnerTableView.DataKeyValues[data.ItemIndex]["sta_total"].ToString());
                int i_new = Convert.ToInt32(data.OwnerTableView.DataKeyValues[data.ItemIndex]["sta_new"].ToString());
                int i_success = Convert.ToInt32(data.OwnerTableView.DataKeyValues[data.ItemIndex]["sta_success"].ToString());  
                int i_refuse = Convert.ToInt32(data.OwnerTableView.DataKeyValues[data.ItemIndex]["sta_Refuse"].ToString());
                int i_wrongn = Convert.ToInt32(data.OwnerTableView.DataKeyValues[data.ItemIndex]["sta_WrongNumber"].ToString());
                int i_duplicated = Convert.ToInt32(data.OwnerTableView.DataKeyValues[data.ItemIndex]["sta_Duplicated"].ToString());
                int i_recheck = Convert.ToInt32(data.OwnerTableView.DataKeyValues[data.ItemIndex]["sta_Recheck"].ToString());
                int i_appoint = Convert.ToInt32(data.OwnerTableView.DataKeyValues[data.ItemIndex]["sta_Appoint"].ToString());
                int i_business = Convert.ToInt32(data.OwnerTableView.DataKeyValues[data.ItemIndex]["sta_Business"].ToString());
                int i_nocontact = Convert.ToInt32(data.OwnerTableView.DataKeyValues[data.ItemIndex]["sta_NoContact"].ToString());
             
                iTota_total = iTota_total + i_total;
                iTota_new = iTota_new + i_new;
                iTota_success = iTota_success + i_success;
                iTota_refuse = iTota_refuse + i_refuse;
                iTota_wrongn = iTota_wrongn + i_wrongn;
                iTota_duplicated = iTota_duplicated + i_duplicated;
                iTota_recheck = iTota_recheck + i_recheck;
                iTota_appoint = iTota_appoint + i_appoint;
                iTota_business = iTota_business + i_business;
                iTota_Nocontact = iTota_Nocontact + i_nocontact;

            }

            sta_total = Convert.ToString(iTota_total);
            sta_new = Convert.ToString(iTota_new);
            sta_success = Convert.ToString(iTota_success);
            sta_refuse = Convert.ToString(iTota_refuse);
            sta_wrongn = Convert.ToString(iTota_wrongn);
            sta_duplicated = Convert.ToString(iTota_duplicated);
            sta_recheck = Convert.ToString(iTota_recheck);
            sta_appoint = Convert.ToString(iTota_appoint);
            sta_business = Convert.ToString(iTota_business);
            sta_Nocontact = Convert.ToString(iTota_Nocontact);


            

            bReturn = true;
        }
        catch (Exception ex)
        {
            sta_total = "0";
            sta_new = "0";
            sta_success = "0";
            sta_refuse = "0";
            sta_wrongn = "0";
            sta_duplicated = "0";
            sta_recheck = "0";
            sta_appoint = "0";
            sta_business = "0";
            sta_Nocontact = "0";

            bReturn = false;
            RadAjaxManager1.Alert(ex.Message);
        }
        return bReturn;
    }

    protected void btnAssign_Click(object sender, EventArgs e)
    {
        try
        {
            if (chkNew.Checked == false && chkBusy.Checked == false && chkPresented.Checked == false && chkNoContact.Checked == false)
            {
                RadAjaxManager1.Alert("เลือกสถานะที่ต้องการแจก");
                return;
            }

            bool bReturn = false;
            if (chkNew.Checked == true)
            {
                if (ASSIGNDS_USER("New", txtAssignNew.Text.Trim()))
                    bReturn = true;
            }

            if (chkBusy.Checked == true)
            {
                if (ASSIGNDS_USER("Busy", txtAssignBusy.Text.Trim()))
                    bReturn = true;
            }

            if (chkPresented.Checked == true)
            {
                if (ASSIGNDS_USER("Presented", txtAssignPresented.Text.Trim()))
                    bReturn = true;
            }

            if (chkNoContact.Checked == true)
            {
                if (ASSIGNDS_USER("NoContact", txtAssignBusy.Text.Trim()))
                    bReturn = true;
            }

            if (bReturn)
            {
                RadAjaxManager1.Alert("โอน List ทั้งหมด : " + intAmountAssign.ToString() + " รายการ");

                ClearControl();
                GetListUserS();
                GetListUserE();
                btnAssign.Enabled = false;
            }
        }
        catch (Exception ex)
        {
            RadAjaxManager1.Alert(ex.Message);
        }
    }

    int intAmountAssign = 0;
    private bool ASSIGNDS_USER(string sStatus, string sDistribute)
    {
        if (CHKERRORUSER_H2H() == true)
        {
            //int i;
            //CheckBox CheckID = default(CheckBox);
            decimal d = Convert.ToDecimal(sDistribute);
            int j = 0;
            StringBuilder SB;

            if (d != 0)
            {
                foreach (GridDataItem data in RadGrid2.MasterTableView.GetSelectedItems())
                {
                    string seq = data.OwnerTableView.DataKeyValues[data.ItemIndex]["Sequence"].ToString();
                    DataTable dtCUS = MMDSUSER(sStatus);
                    //========= Loop รายชื่อลูกค้า
                    if (dtCUS.Rows.Count != 0)
                    {
                        for (j = 0; j <= d - 1; j++)
                        {
                            SB = new StringBuilder();
                            //gvUserDes.Rows[i].Cells[3].Text
                            SB.Append(" UPDATE TblCustomer_master SET OwnerID='" + seq + "', AssignID='"+ lblUserID.Text.Trim() +"', AssignDate=GETDATE() ");
                            if (cbkToNew.Checked)
                            {
                                SB.Append(", StatusID='1', SubStatusID='1' ");
                                SB.Append(", Comments='', LastCallDate=null ");
                                //## update set callhistory faq del
                            }
                            SB.Append("WHERE CusID='" + dtCUS.Rows[j]["CusID"] + "' ");
                            SB.Append("AND OwnerID='" + dtCUS.Rows[j]["OwnerID"] + "' ");

                            string strupdate = SB.ToString();
                            GE.UPDATE_Executeion(strupdate);
                            bool iStatus = true;
                            if (iStatus)
                            {
                                intAmountAssign++;
                            }
                            
                        }
                    }
                }
                return true;
            }
            else
            {
                RadAjaxManager1.Alert("!!! ไม่สามารถโอน List ได้ [Agent กับจำนวนรายชื่อที่โอนไม่เหมาะสม] !!!");
                return false;
            }
        }
        else
        {
            RadAjaxManager1.Alert("!!! ไม่สามารถโอน ตรวจสอบการเลือกข้อมูล !!!");
            return false;
        }
    }
    private bool CHKERRORUSER_H2H()
    {
        int user_check = 0;
        int des_check = 0;
        foreach (GridDataItem data in RadGrid1.MasterTableView.GetSelectedItems())
        {
            user_check = user_check + 1;
        }
        if (user_check == 0)
        {
            RadAjaxManager1.Alert("!!! กรุณาเลือก User ต้นทาง!!!");
            rcbUserS.Focus();
            return false;
        }
        foreach (GridDataItem data in RadGrid2.MasterTableView.GetSelectedItems())
        {
            des_check = des_check + 1;
        }
        if (des_check == 0)
        {
            RadAjaxManager1.Alert("!!! กรุณาเลือก User ปลายทาง !!!");
            rcbUserE.Focus();
            return false;
        }

        try
        {
            if (chkNew.Checked)
            {
                if (txtAssignNew.Text.Trim() == "")
                {
                    RadAjaxManager1.Alert("!!! กรุณาระบุจำนวน ลองใหม่อีกครั้ง !!!");
                    txtAssignNew.Focus();
                    return false;
                }
                if (txtAssignNew.Value > int.Parse(txtNew.Text))
                {
                    RadAjaxManager1.Alert("!!! ระบุจำนวนเกิน ลองใหม่อีกครั้ง !!!");
                    txtAssignNew.Focus();
                    return false;
                }
            }
            if (chkBusy.Checked)
            {
                if (txtAssignBusy.Text.Trim() == "")
                {
                    RadAjaxManager1.Alert("!!! กรุณาระบุจำนวน ลองใหม่อีกครั้ง !!!");
                    txtAssignBusy.Focus();
                    return false;
                }
                if (txtAssignBusy.Value > int.Parse(txtBusiness.Text))
                {
                    RadAjaxManager1.Alert("!!! ระบุจำนวนเกิน ลองใหม่อีกครั้ง !!!");
                    txtAssignBusy.Focus();
                    return false;
                }
            }
            if (chkPresented.Checked)
            {
                if (txtAssignPresented.Text.Trim() == "")
                {
                    RadAjaxManager1.Alert("!!! กรุณาระบุจำนวน ลองใหม่อีกครั้ง !!!");
                    txtAssignPresented.Focus();
                    return false;
                }
                if (txtAssignPresented.Value > int.Parse(txtDuplicateed.Text))
                {
                    RadAjaxManager1.Alert("!!! ระบุจำนวนเกิน ลองใหม่อีกครั้ง !!!");
                    txtAssignPresented.Focus();
                    return false;
                }
            }
            if (chkNoContact.Checked)
            {
                if (txtAssignNoContact.Text.Trim() == "")
                {
                    RadAjaxManager1.Alert("!!! กรุณาระบุจำนวน ลองใหม่อีกครั้ง !!!");
                    txtAssignNoContact.Focus();
                    return false;
                }
                if (txtAssignNoContact.Value > int.Parse(txtNoContact.Text))
                {
                    RadAjaxManager1.Alert("!!! ระบุจำนวนเกิน ลองใหม่อีกครั้ง !!!");
                    txtAssignNoContact.Focus();
                    return false;
                }
            }
        }
        catch (Exception ex)
        {
            RadAjaxManager1.Alert("!!! กำหนดข้อมูลไม่เหมาะสม ลองใหม่อีกครั้ง !!!");
            txtAssignNew.Focus();
            return false;
        }

        return true;
    }

    private DataTable MMDSUSER(string sStatus)
    {
        string sOwnerID = "";
        foreach (GridDataItem data in RadGrid1.MasterTableView.GetSelectedItems())
        {
            string seq = data.OwnerTableView.DataKeyValues[data.ItemIndex]["Sequence"].ToString();
            if (sOwnerID == "")
            {
                sOwnerID = seq;
            }
            else
            {
                sOwnerID = sOwnerID + "," + seq;
            }
        }
        try
        {
            string strSP = " ";
            if (rcbSource.SelectedIndex >= 0)
            {
                strSP += " and c.SourceID='" + rcbSource.SelectedValue + "' ";
            }
            StringBuilder SB;
            SB = new StringBuilder();
            SB.Append(" SELECT c.CusID, c.OwnerID FROM TblCustomer_master c ");
            SB.Append(" WHERE c.Active = 'Y' ");
            SB.Append(" AND c.OwnerID IN (" + sOwnerID + ") " + strSP + " ");
            if (sStatus == "New")
            {
                SB.Append("AND c.StatusID='1' ");
            }
            if (sStatus == "Busy")
            {
                SB.Append("AND c.StatusID='5' ");
            }
            if (sStatus == "Presented")
            {
                SB.Append("AND c.StatusID='4' ");
            }
            if (sStatus == "NoContact")
            {
                SB.Append("AND c.StatusID='6' ");
            }
            
            SB.Append("ORDER BY newid() ");

            string strsql = SB.ToString();
            DataTable dt = GE.SELECT_Executeion(strsql);
            if (dt.Rows.Count > 0)
            {
                return dt;
            }
            else
            {
                dt = null;
                return dt;
            }
        }
        catch (Exception ex)
        {
            RadAjaxManager1.Alert(ex.Message);
            return null;
        }
    }
}