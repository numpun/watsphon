﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using Excel = Microsoft.Office.Interop.Excel;
using System.Configuration;
using System.Text;
using Telerik.Web.UI;

public partial class ReportOutBoundRawData : System.Web.UI.Page
{
    GetExecution GE = new GetExecution();
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!this.IsPostBack)
            {
                if (Session["UserID"] != null)
                {
                    ScriptManager.GetCurrent(this.Page).RegisterPostBackControl(BtnFindReport);
                    lblUserID.Text = Session["UserID"].ToString();
                    lblUserLevelID.Text = Session["UserLevelID"].ToString();
                    rdpDateS.SelectedDate = DateTime.Today;
                    rdpDateE.SelectedDate = DateTime.Today;
                    GETAPPLIST(ref cmbAppStatus);
                }
                else
                {
                    Response.Redirect("~/Default.aspx");
                }
            }

        }
        catch (Exception ex)
        {
            RadAjaxManager1.Alert(ex.Message);
        }
    }

    private void GETAPPLIST(ref RadComboBox obj)
    {
        try
        {
            #region
            string[] arrDateS = rdpDateS.DateInput.SelectedDate.Value.ToShortDateString().Split('/');
            string[] arrDateE = rdpDateE.DateInput.SelectedDate.Value.ToShortDateString().Split('/');
            //'2014-12-03 00:00:00'
            string DateStart = "";
            string DateEnd = "";

            string chkMounthS = arrDateS[1];
            string chkDayS = arrDateS[0];
            string chkMounthE = arrDateE[1];
            string chkDayE = arrDateE[0];

            string GetmounthS = "";
            string GetdayS = "";
            string GetmounthE = "";
            string GetdayE = "";


            if (chkMounthS.Length < 2)
            {
                GetmounthS = "0" + arrDateS[1];
            }
            else
            {
                GetmounthS = arrDateS[1];
            }

            if (chkDayS.Length < 2)
            {
                GetdayS = "0" + arrDateS[0];
            }
            else
            {
                GetdayS = arrDateS[0];
            }

            if (chkMounthE.Length < 2)
            {
                GetmounthE = "0" + arrDateE[1];
            }
            else
            {
                GetmounthE = arrDateE[1];
            }

            if (chkDayE.Length < 2)
            {
                GetdayE = "0" + arrDateE[0];
            }
            else
            {
                GetdayE = arrDateE[0];
            }

            DateStart = arrDateS[2] + "-" + GetmounthS + "-" + GetdayS + " 00:00:00";
            DateEnd = arrDateE[2] + "-" + GetmounthE + "-" + GetdayE + " 23:59:59";
            #endregion


            //RadAjaxManager1.Alert(DateStart + "<>" + DateEnd);

            string strSQL = " SELECT App.StatusID, App.StatusNameTH, COUNT(1) as APPSUM, App.Ordering ";
            strSQL += " FROM TblCustomer_master C ";
            strSQL += " INNER JOIN TblStatus App ON C.StatusID=App.StatusID ";
            strSQL += " INNER JOIN TblUser U ON C.OwnerID=U.UserID ";
            strSQL += " WHERE App.Active = 'Y' AND App.StatusID <> 1 ";
            strSQL += " AND C.Active = 'Y'  ";
            strSQL += " AND CONVERT(VARCHAR(24),C.LastCallDate,120) BETWEEN '" + DateStart + "' AND '" + DateEnd + "' ";

            strSQL += " GROUP BY App.StatusNameTH, App.StatusID, App.Ordering ";
            strSQL += " ORDER BY App.Ordering ";

            DataTable dt = GE.SELECT_Executeion(strSQL);
            if (dt.Rows.Count > 0)
            {
                int SumAll = 0;
                foreach (DataRow dr in dt.Rows)
                {
                    SumAll += Convert.ToInt32(dr["APPSUM"]);
                }
                DataRow newrow = dt.NewRow();
                newrow["StatusID"] = "0";
                newrow["StatusNameTH"] = "ทั้งหมด (รวม)";
                newrow["APPSUM"] = SumAll.ToString();
                dt.Rows.Add(newrow);
                dt.AcceptChanges();

                bool isSubmit = false;
                foreach (DataRow dr in dt.Rows)
                {
                    RadComboBoxItem item = new RadComboBoxItem();
                    //item.Text = dr["source_name"].ToString();
                    item.Value = dr["StatusID"].ToString();
                    item.Text = dr["StatusNameTH"].ToString();
                    item.ToolTip = dr["APPSUM"].ToString();

                    item.Attributes.Add("APPSTATUSNAME", dr["StatusNameTH"].ToString());
                    item.Attributes.Add("APPSUM", dr["APPSUM"].ToString());
                    obj.Items.Add(item);
                    item.DataBind();
                    if (dr["StatusID"].ToString() != "1")
                    {
                        isSubmit = true;
                    }
                }
            }
        }
        catch (Exception ex)
        {
            RadAjaxManager1.Alert(ex.Message);
        }
    }
    protected void rdpDateS_SelectedDateChanged(object sender, Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs e)
    {
        GETAPPLIST(ref cmbAppStatus);
    }
    protected void rdpDateE_SelectedDateChanged(object sender, Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs e)
    {
        GETAPPLIST(ref cmbAppStatus);
    }


    protected void BtnFindReport_Click(object sender, EventArgs e)
    {
        try
        {
            #region
            string[] arrDateS = rdpDateS.DateInput.SelectedDate.Value.ToShortDateString().Split('/');
            string[] arrDateE = rdpDateE.DateInput.SelectedDate.Value.ToShortDateString().Split('/');
            //'2014-12-03 00:00:00'
            string dateS = "";
            string dateE = "";

            string chkMounthS = arrDateS[1];
            string chkDayS = arrDateS[0];
            string chkMounthE = arrDateE[1];
            string chkDayE = arrDateE[0];

            string GetmounthS = "";
            string GetdayS = "";
            string GetmounthE = "";
            string GetdayE = "";


            if (chkMounthS.Length < 2)
            {
                GetmounthS = "0" + arrDateS[1];
            }
            else
            {
                GetmounthS = arrDateS[1];
            }

            if (chkDayS.Length < 2)
            {
                GetdayS = "0" + arrDateS[0];
            }
            else
            {
                GetdayS = arrDateS[0];
            }

            if (chkMounthE.Length < 2)
            {
                GetmounthE = "0" + arrDateE[1];
            }
            else
            {
                GetmounthE = arrDateE[1];
            }

            if (chkDayE.Length < 2)
            {
                GetdayE = "0" + arrDateE[0];
            }
            else
            {
                GetdayE = arrDateE[0];
            }

            dateS = arrDateS[2] + "-" + GetmounthS + "-" + GetdayS + " 00:00:00";
            dateE = arrDateE[2] + "-" + GetmounthE + "-" + GetdayE + " 23:59:59";
            #endregion
            //RadAjaxManager1.Alert(dateS + "<>" + dateE);
            DataTable dt = GetData(dateS, dateE);

            //**** เปลี่ยนออกแบบ Excel แทน
            if (dt.Rows.Count > 0)
            {
                ExportToExcelTemplate(dt);
            }
            else
            {
                RadAjaxManager1.Alert("ไม่มีข้อมูลออกรายงาน");
            }
        }
        catch (Exception ex)
        {
            RadAjaxManager1.Alert(ex.Message);
        }
    }

    private DataTable GetData(string DateStart, string DateEnd)
    {
        try
        {
            string strSQL = @" SELECT cm.CustomerCode as f1
                                , cm.CarID as f2
                                , cm.CDBID as f3
                                , c.Tupe as f4
                                , c.Title as f5
                                , c.Name as f6
                                , c.BrNo as f7
                                , SUBSTRING(
                                          + CASE WHEN isnull(c.mCustomerHNumber,'') <> '-' THEN isnull(c.mCustomerHNumber,'') ELSE '' END +
                                          + CASE WHEN isnull(c.mCustomerHV,'') <> '-' THEN ' ' + isnull(c.mCustomerHV,'') ELSE '' END +
                                          + CASE WHEN isnull(c.mCustomerHM,'') <> '-' THEN ' ม.' + isnull(c.mCustomerHM,'') ELSE '' END +
                                          + CASE WHEN isnull(c.mCustomerSoi,'') <> '-' THEN ' ซ.' + isnull(c.mCustomerSoi,'') ELSE '' END +
                                          + CASE WHEN isnull(c.mCustomerRoad,'') <> '-' THEN ' ถ.' + isnull(c.mCustomerRoad,'') ELSE '' END
                                          + CASE WHEN isnull(c.mCustomerSubDisName,'') <> '' THEN
					                                CASE WHEN Replace(isnull(c.mCustomerProvinceName,''),'N/A','') = 'กรุงเทพมหานคร'
						                                 THEN ' แขวง' + Replace(isnull(c.mCustomerSubDisName,''),'N/A','')
						                                 ELSE ' ต.' + Replace(isnull(c.mCustomerSubDisName,''),'N/A','')
						                                 END
			                                ELSE '' END,1,1000) as f8
                                , SUBSTRING(
		                                 + CASE WHEN isnull(c.mCustomerDistrictName,'') <> '' THEN
					                                CASE WHEN Replace(isnull(c.mCustomerProvinceName,''),'N/A','') = 'กรุงเทพมหานคร'
						                                 THEN '' + Replace(isnull(c.mCustomerDistrictName,''),'N/A','')
						                                 ELSE 'อ.' + Replace(isnull(c.mCustomerDistrictName,''),'N/A','')
						                                 END
			                                ELSE '' END +
			                                + CASE WHEN isnull(c.mCustomerProvinceName,'') <> '' THEN
					                                CASE WHEN Replace(isnull(c.mCustomerProvinceName,''),'N/A','') = 'กรุงเทพมหานคร'
						                                 THEN '' + Replace(isnull(c.mCustomerProvinceName,''),'N/A','')
						                                 ELSE 'จ.' + Replace(isnull(c.mCustomerProvinceName,''),'N/A','')
						                                 END
			                                ELSE '' END,1,500) as f9
                                , Replace(isnull(c.mCustomerDistrictName,''),'เขต','') as f10
                                , c.Zipcode as f11
                                , c.TaxID as f12
                                , c.CompleateAccount as f13
                                , Replace(Replace(c.HomePhone,'-','N/A'),'','N/A')  as f14
                                , Replace(Replace(c.OfficePhone,'-','N/A'),'','N/A') as f15
                                , Replace(Replace(c.Mobile,'-','N/A'),'','N/A') as f16
                                , Replace(Replace(c.Fax,'-','N/A'),'','N/A') as f17
                                , Replace(Replace(c.Email,'-','N/A'),'','N/A') as f18
                                , c.Gender as f19
                                , c.MaritalStatus as f20
                                , Replace(Replace(c.NumberOfChilden,'-','N/A'),'','N/A') as f21
                                , Replace(Replace(c.Birthdate,'-','N/A'),'','N/A') as f22
                                , c.Occupation as f23
                                , c.Hobby as f24
                                , c.Income as f25
                                , cm.PerferredLanguage as f26
                                , c.ContactType as f27
                                , c.PreferContactChanel as f28
                                , c.ContactTitle as f29
                                , c.ContactName as f30
                                , SUBSTRING(
                                          + CASE WHEN isnull(c.mCTHNumber,'') <> '-' THEN Replace(isnull(c.mCTHNumber,''),'N/A','') ELSE '' END +
                                          + CASE WHEN isnull(c.mCTHV,'') <> '-' THEN ' ' + Replace(isnull(c.mCTHV,''),'N/A','1') ELSE '' END +
                                          + CASE WHEN isnull(c.mCTHM,'') <> '-' THEN ' ม.' + Replace(isnull(c.mCTHM,''),'N/A','') ELSE '' END +
                                          + CASE WHEN isnull(c.mCTSoi,'') <> '-' THEN ' ซ.' + Replace(isnull(c.mCTSoi,''),'N/A','') ELSE '' END +
                                          + CASE WHEN isnull(c.mCTRoad,'') <> '-' THEN ' ถ.' + Replace(isnull(c.mCTRoad,''),'N/A','') ELSE '' END
                                          + CASE WHEN isnull(c.mSubDisName,'') <> '' THEN
					                                CASE WHEN Replace(isnull(c.mCTProvinceName,''),'N/A','') = 'กรุงเทพมหานคร'
						                                 THEN ' แขวง' + Replace(isnull(c.mSubDisName,''),'N/A','')
						                                 ELSE ' ต.' + Replace(isnull(c.mSubDisName,''),'N/A','')
						                                 END
			                                ELSE '' END,1,1000) as f31
                                , SUBSTRING(
                                           +
		                                   CASE WHEN isnull(c.mCTDistrictName,'') <> '' THEN
					                                CASE WHEN Replace(isnull(c.mCTProvinceName,''),'N/A','') = 'กรุงเทพมหานคร'
						                                 THEN '' + Replace(isnull(c.mCTDistrictName,''),'N/A','')
						                                 ELSE 'อ.' + Replace(isnull(c.mCTDistrictName,''),'N/A','')
						                                 END
			                                ELSE '' END +
			                                + CASE WHEN isnull(c.mCTProvinceName,'') <> '' THEN
					                                CASE WHEN Replace(isnull(c.mCTProvinceName,''),'N/A','') = 'กรุงเทพมหานคร'
						                                 THEN '' + Replace(isnull(c.mCTProvinceName,''),'N/A','')
						                                 ELSE 'จ.' + Replace(isnull(c.mCTProvinceName,''),'N/A','')
						                                 END
			                                ELSE '' END,1,500) as f32
                                , c.ContactZipcode as f33
                                , Replace(Replace(c.Contact,'-','N/A'),'','N/A') as f34
                                , Replace(Replace(c.ContactOfficePhone,'-','N/A'),'','N/A') as f35
                                , c.ContactMobile as f36
                                , c.ContactFax as f37
                                , c.ContactEmail as f38
                                , c.ContactGender as f39
                                , c.ContactMaritalStatus as f40
                                , Replace(Replace(c.ContactNumberOfChilden,'-','N/A'),'','N/A')  as f41
                                , Replace(Replace(c.ContactBirthdate,'-','N/A'),'','N/A') as f42
                                , c.ContactOccupation as f43
                                , c.ContactHobby as f44
                                , c.ContactIncome as f45
                                , cm.BaranchOwner as f46
                                , cm.VIN as f47
                                , cm.License as f48
                                , cm.CarVaiance as f49
                                , cm.CarModel as f50
                                , cm.CarMY as f51
                                , cm.RetailDate as f52
                                , cm.RetailBranch as f53
                                , cm.LastServiceBr as f54
                                , cm.LastServiceDate as f55
                                , cm.SalesCodeorName as f56
                                , cm.LastMileage as f57
                                , cm.SACode as f58
                                , cm.SAName as f59
                                , cm.CarUsageType as f60
                                , cm.Remarks as f61
                                , cm.IsEdit as f62
                                , CASE WHEN cm.IsEdit = 'N' THEN '' ELSE cm.IsReason END as f63
                                , s.StatusNameTH as f64
                                , ss.SubStatusNameTH as f65
                                , u.Fname+' '+u.Lname as f66
                                , cm.UpdateDate as f67
                                FROM TblCustomer c
                                LEFT JOIN TblCustomer_master cm ON c.ReferCusID = cm.CusID
                                LEFT JOIN TblUser u ON u.UserID = cm.OwnerID
                                LEFT JOIN TblStatus s ON s.StatusID = cm.StatusID
                                LEFT JOIN TblSubStatus ss ON ss.SubStatusID = cm.SubStatusID
                                ";
            strSQL += " WHERE cm.AppStatus <> 1 "
                   + " AND cm.StatusID = 2 AND cm.SubStatusID IN(2,3) "
                   + " AND CONVERT(VARCHAR(24),cm.LastCallDate,120) BETWEEN '" + DateStart + "' AND '" + DateEnd + "' ";


            DataTable dt = GE.SELECT_Executeion(strSQL);
            return dt;
        }
        catch (Exception ex)
        {
            RadAjaxManager1.Alert(ex.Message);
            return null;
        }
    }

    public void ExportToExcelTemplate(DataTable dt)
    {
        try
        {
            Excel.Application xlApp;
            Excel.Workbook xlWorkBook;
            Excel.Worksheet xlWorkSheet = null;
            object mis = Type.Missing;

            string strTemplate = @"C:\inetpub\wwwroot\volvo.outbound\_FileTemplate\OutBoundRawData.xlsx";

            string strExcelName = "OUTBOUNDRAWDATA_" + DateTime.Now.ToString("yyyyMMddHHmmssss") + ".xlsx";

            string strFileExport = @"C:\inetpub\wwwroot\volvo.outbound\_FileExport\" + strExcelName;

            xlApp = new Excel.Application();
            xlWorkBook = xlApp.Workbooks.Open(strTemplate, 0, true, 2, "", "", true, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
            xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
            xlWorkSheet.Name = "OutBoundRawData";


            string sDate = rdpDateS.DateInput.SelectedDate.Value.ToShortDateString();
            string eDate = rdpDateE.DateInput.SelectedDate.Value.ToShortDateString();
            xlWorkSheet.Cells[3, 1] = "Report Name : [ Outbound ] Raw Data by List";
            xlWorkSheet.Cells[4, 1] = "Report Date : " + DateTime.Now.ToString("dd/MM/yyyy hh:mm");
            xlWorkSheet.Cells[5, 1] = "ช่วงวันที่ระบุ : " + sDate.Trim() + "  ถึง  " + eDate.Trim();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                for (int iColumn = 0; iColumn < dt.Columns.Count; iColumn++)
                {
                    xlWorkSheet.Cells[i + 10, iColumn + 1] = dt.Rows[i][iColumn].ToString().Trim();
                }
            }

            Microsoft.Office.Interop.Excel.Range cellA1 = xlWorkSheet.Cells;
            cellA1.WrapText = false;

            xlWorkBook.SaveAs(strFileExport, mis, mis, mis, mis, mis, Excel.XlSaveAsAccessMode.xlExclusive, mis, mis, mis, mis, mis);
            xlWorkBook.Close(true, mis, mis);
            xlApp.Quit();
            System.Runtime.InteropServices.Marshal.ReleaseComObject(xlApp);
            System.Runtime.InteropServices.Marshal.ReleaseComObject(xlWorkBook);
            xlApp = null;
            xlWorkBook = null;
            xlWorkSheet = null;
            GC.Collect();
            //RadAjaxManager1.Alert("Generate Successfully");
            System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
            response.ClearContent();
            response.Clear();
            Response.ContentType = "application/xlsx";
            response.AddHeader("Content-Disposition", "attachment; filename=" + strExcelName);
            Response.TransmitFile(strFileExport);
            response.Flush();
            Response.SuppressContent = true;
            response.End();
        }
        catch (Exception ex)
        {
            RadAjaxManager1.Alert(ex.Message);
        }
    }
}
