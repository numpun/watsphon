﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageInfo.master" AutoEventWireup="true" CodeFile="ListAgent.aspx.cs" Inherits="ListAgent" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Label ID="lblUserID" runat="server" Visible="false" Text="Label"></asp:Label>
    <asp:Label ID="lblUserLevelID" runat="server" Visible="false" Text="Label"></asp:Label>
    <asp:Label ID="lblLastStatus" runat="server" Visible="false" Text="0"></asp:Label>
    <asp:Panel ID="PanelHead" runat="server">
    </asp:Panel>
    <asp:Panel ID="PanelListTsr" runat="server">
        <div style="width: 30%; border: 0px solid #000; float: left;">
            <table>
                <tr>
                    <td>
                        <telerik:RadGrid
                            ID="RadGridStatus"
                            runat="server"
                            Width="90%"
                            Height="100%"
                            GridLines="None"
                            BorderWidth="1"
                            Style="outline: none"
                            AutoGenerateColumns="false"
                            OnItemCommand="RadGridStatus_ItemCommand">
                            <PagerStyle Mode="NextPrevAndNumeric" />
                            <HeaderStyle CssClass="Appointmentheader" />
                            <MasterTableView
                                AutoGenerateColumns="False"
                                DataKeyNames="SubStatusID"
                                ClientDataKeyNames="SubStatusID"
                                CommandItemDisplay="none"
                                EnableHeaderContextMenu="true">
                                <Columns>
                                    <telerik:GridTemplateColumn
                                        AutoPostBackOnFilter="true"
                                        CurrentFilterFunction="Contains"
                                        UniqueName="TempCol"
                                        AllowFiltering="false">
                                        <ItemTemplate>
                                            <asp:LinkButton
                                                ID="LinkButton1"
                                                CommandName="Select"
                                                ForeColor="red"
                                                Text='<img src="Images/icons/flag.png">'
                                                runat="server"></asp:LinkButton>
                                            <asp:HiddenField
                                                ID="HiddenField1"
                                                runat="server"
                                                Value='<%# Eval("SubStatusID") %>' />
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" Width="30px" />
                                        <ItemStyle HorizontalAlign="Center" Font-Bold="true" Wrap="true" ForeColor="Red" />
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridBoundColumn
                                        HeaderText="id"
                                        DataField="SubStatusID"
                                        UniqueName="SubStatusID"
                                        Visible="false">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Left" Wrap="False" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn
                                        HeaderText="สถานะ"
                                        DataField="SubStatusNameTH"
                                        UniqueName="SubStatusNameTH">
                                        <HeaderStyle HorizontalAlign="Center" Width="130px" />
                                        <ItemStyle HorizontalAlign="Left" Wrap="False" />
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn
                                        HeaderText="จำนวน"
                                        DataField="Rec"
                                        UniqueName="Rec">
                                        <HeaderStyle HorizontalAlign="Center" Width="30px" />
                                        <ItemStyle HorizontalAlign="Center" Wrap="False" />
                                    </telerik:GridBoundColumn>

                                </Columns>
                            </MasterTableView>


                            <ClientSettings
                                AllowColumnsReorder="true"
                                AllowColumnHide="True"
                                EnableRowHoverStyle="True"
                                Selecting-AllowRowSelect="true">
                                <Selecting AllowRowSelect="True" />
                                <Resizing AllowColumnResize="true" />
                            </ClientSettings>

                        </telerik:RadGrid>
                    </td>
                </tr>
                <tr>
                    <td>
                        <br />
                        <br />
                    </td>
                </tr>
                 <tr>
                    <td>
                        <telerik:RadGrid
                            ID="RadGridStatusSuccess"
                            runat="server"
                            Width="90%"
                            Height="100%"
                            GridLines="None"
                            BorderWidth="1"
                            Style="outline: none"
                            AutoGenerateColumns="false" OnItemCommand="RadGridStatusSuccess_ItemCommand">
                            <PagerStyle Mode="NextPrevAndNumeric" />
                            <HeaderStyle CssClass="Appointmentheader" />
                            <MasterTableView
                                AutoGenerateColumns="False"
                                DataKeyNames="SubStatusID"
                                ClientDataKeyNames="SubStatusID"
                                CommandItemDisplay="none"
                                EnableHeaderContextMenu="true">
                                <Columns>
                                    <telerik:GridTemplateColumn
                                        AutoPostBackOnFilter="true"
                                        CurrentFilterFunction="Contains"
                                        UniqueName="TempCol"
                                        AllowFiltering="false">
                                        <ItemTemplate>
                                            <asp:LinkButton
                                                ID="LinkButton3"
                                                CommandName="Select"
                                                ForeColor="red"
                                                Text='<img src="Images/icons/flag.png">'
                                                runat="server"></asp:LinkButton>
                                            <asp:HiddenField
                                                ID="HiddenField3"
                                                runat="server"
                                                Value='<%# Eval("SubStatusID") %>' />
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" Width="30px" />
                                        <ItemStyle HorizontalAlign="Center" Font-Bold="true" Wrap="true" ForeColor="Red" />
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridBoundColumn
                                        HeaderText="id"
                                        DataField="SubStatusID"
                                        UniqueName="SubStatusID"
                                        Visible="false">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Left" Wrap="False" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn
                                        HeaderText="สถานะ(สำเร็จ)"
                                        DataField="SubStatusNameTH"
                                        UniqueName="SubStatusNameTH">
                                        <HeaderStyle HorizontalAlign="Center" Width="130px" />
                                        <ItemStyle HorizontalAlign="Left" Wrap="False" />
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn
                                        HeaderText="จำนวน"
                                        DataField="Rec"
                                        UniqueName="Rec">
                                        <HeaderStyle HorizontalAlign="Center" Width="30px" />
                                        <ItemStyle HorizontalAlign="Center" Wrap="False" />
                                    </telerik:GridBoundColumn>

                                </Columns>
                            </MasterTableView>


                            <ClientSettings
                                AllowColumnsReorder="true"
                                AllowColumnHide="True"
                                EnableRowHoverStyle="True"
                                Selecting-AllowRowSelect="true">
                                <Selecting AllowRowSelect="True" />
                                <Resizing AllowColumnResize="true" />
                            </ClientSettings>

                        </telerik:RadGrid>
                    </td>
                </tr>
            </table>
        </div>
        <div style="width: 68%; height: 500px; border: 0px solid #000; float: right;">
            <telerik:RadGrid
                            ID="RadGridShowList"
                            runat="server"
                            Width="95%"
                            Height="500px"
                            GridLines="None"
                            BorderWidth="1"
                            AllowPaging="false"
                            AllowSorting="true"
                            Style="outline: none"
                            AllowFilteringByColumn="false"
                            AllowMultiRowSelection="True"
                            OnPageIndexChanged="RadGridShowList_PageIndexChanged"
                            OnItemDataBound="RadGridShowList_ItemDataBound" 
                            OnItemCommand="RadGridShowList_ItemCommand">
                            <PagerStyle Mode="NextPrevAndNumeric" />
                            <HeaderStyle CssClass="Appointmentheader" />
                            <MasterTableView
                                AutoGenerateColumns="False"
                                DataKeyNames="CusID"
                                ClientDataKeyNames="CusID"
                                CommandItemDisplay="none"
                                EnableHeaderContextMenu="true"
                                AllowPaging="True"
                                PageSize="20">
                                <Columns>
                                      <telerik:GridTemplateColumn
                                        AutoPostBackOnFilter="true"
                                        CurrentFilterFunction="Contains"
                                        UniqueName="TempCol"
                                        AllowFiltering="false">
                                        <ItemTemplate>
                                            <asp:LinkButton
                                                ID="LinkButton2"
                                                CommandName="Select"
                                                ForeColor="red"
                                                Text='<%# Eval("CusID") %>'
                                                runat="server"></asp:LinkButton>
                                            <asp:HiddenField
                                                ID="HiddenField2"
                                                runat="server"
                                                Value='<%# Eval("CusID") %>' />
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" Width="60px" />
                                        <ItemStyle HorizontalAlign="Center" Font-Bold="true" Wrap="true" ForeColor="Red" />
                                    </telerik:GridTemplateColumn>
                                    <%-- <telerik:GridBoundColumn
                                        HeaderText="customer_code"
                                        DataField="CustomerCode">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" Wrap="False" />
                                    </telerik:GridBoundColumn>--%>
                                    <telerik:GridBoundColumn
                                        HeaderText="full_name"
                                        DataField="Fullname">
                                        <HeaderStyle HorizontalAlign="Center" Width="230px" />
                                        <ItemStyle HorizontalAlign="Center" Wrap="False" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn
                                        HeaderText="home_phone"
                                        DataField="SHomePhone">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn
                                        HeaderText="mobile"
                                        DataField="SMobile">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn
                                        HeaderText="office_phone"
                                        DataField="Soffice_phone">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center"/>
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn
                                        HeaderText="lastcall_date"
                                        DataField="LastCallDate">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn
                                        HeaderText="called"
                                        DataField="CntCall">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center"  />
                                    </telerik:GridBoundColumn>
                                </Columns>
                            </MasterTableView>
                            <ClientSettings
                                AllowColumnsReorder="true"
                                AllowColumnHide="True"
                                EnableRowHoverStyle="True"
                                ReorderColumnsOnClient="true">
                                <Selecting AllowRowSelect="True" />
                                <Resizing AllowColumnResize="true" />
                                <Scrolling AllowScroll="True"></Scrolling>
                            </ClientSettings>
                        </telerik:RadGrid>
        </div>
    </asp:Panel>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server"></telerik:RadAjaxLoadingPanel>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" EnableAJAX="true" EnablePageHeadUpdate="false" EnableViewState="false">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="RadGridStatus">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGridShowList" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadGridStatusSuccess">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGridShowList" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
              <telerik:AjaxSetting AjaxControlID="RadGridShowList">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGridShowList" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
</asp:Content>

