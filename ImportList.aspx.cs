﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;
//using System.Web.UI;
//using System.Web.UI.WebControls;
//using Telerik.Web.UI;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.UI.HtmlControls;
using System.Data;
using System.Collections;
using Telerik.Web.UI;
using System.Data.OleDb;
using System.Configuration;


public partial class ImportList : System.Web.UI.Page
{
    GetExecution GE = new GetExecution();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //if (Session["UserID"] == null)
            //{
            //    Response.Redirect("~/Default.aspx");
            //}

            //Session["FilePath"] = "";
            //lblUserID.Text = Session["UserID"].ToString();
            //lblUserLevelID.Text = Session["UserLevelID"].ToString();
        }
    }
    protected void buttonSubmitUpload_Click(object sender, EventArgs e)
    {
        try
        {
           
            if (RadUpload1.UploadedFiles.Count < 1)
            {
                RadAjaxManager1.Alert("No file");
                return;
            }
            //cmbDealer.Text + "_" + DateTime.Now.ToString("yyyyMMddHHmmssss");
            string nameFile = RadUpload1.UploadedFiles[0].FileName;
            string pathFile = Server.MapPath("~/_File_Attach/" + nameFile);

            if (File.Exists(pathFile))
            {
                RadAjaxManager1.Alert("ไฟล์ที่เลือกซ้ำ!! ไฟล์ข้อมูลนี้เคยมีการ Import data เข้าระบบแล้ว  เตือน!! การ Import ไฟล์เดิมซ้ำอาจส่งผลให้ข้อมูลลูกค้าซ้ำซ้อนและเกิดข้อผิดพลาดได้. ลองใหม่อีกครั้ง");
                return;
            }
            RadUpload1.UploadedFiles[0].SaveAs(pathFile, true);
            Session["FileName"] = nameFile.Split('.')[0].ToString();
            Session["FilePath"] = pathFile;

            string strCon = "";
            string typeFile = RadUpload1.UploadedFiles[0].FileName.Split('.')[1].ToString();
            if (typeFile.Trim() == "xls")
            {
                //strCon = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + @pathFile + ";Extended Properties=Excel 12.0;";
                strCon = "Provider=Microsoft.Jet.Oledb.4.0;Data Source=" + @pathFile + ";Extended Properties=Excel 8.0;";
                string strSQL = " INSERT INTO TblDataSource (SourceName,TotalSource,Active,CreateID,CreateDate) "
                               + " VALUES( "
                               + " '" + nameFile.Split('.')[0].ToString() + "',0,'Y','" + lblUserID.Text.Trim() + "',GETDATE()) ";
                GE.INSERT_Executeion(strSQL);
                DataTable MAXDATAID = GE.SELECT_Executeion(" SELECT MAX(SourceID) AS MaxID FROM TblDataSource ");
                lblDataSourceID.Text = MAXDATAID.Rows[0]["MaxID"].ToString();
            }
            else
            {
                RadAjaxManager1.Alert(" ไฟล์ต้องเป็น Excel 2003 (.xls) เท่านั้น !! ");
                return;
            }


            if (pathFile.Trim().Length > 0)
            {
                OleDbConnection con = new OleDbConnection(strCon);
                con.Open();
                DataTable dt = null;
                dt = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                if (dt.Rows.Count > 0)
                {
                    PreviewData(dt.Rows[0]["TABLE_NAME"].ToString());
                    BtnConfirm.Visible = true;
                }
                else
                {
                    RadAjaxManager1.Alert("ผิดพลาด ไม่พบข้อมูลในไฟล์ Excel!! ลองใหม่อีกครั้ง");
                }
            }
        }
        catch (Exception ex)
        {
            string FilePath = Session["FilePath"].ToString();
            if (File.Exists(FilePath))
            {
                File.Delete(FilePath);
            }
            RadAjaxManager1.Alert(ex.Message);
        }
    }

    private void PreviewData(string SheetName)
    {
        try
        {
            DataTable dtSSI = new DataTable();
            dtSSI.Columns.Add(new DataColumn("cus_1", typeof(string)));
            dtSSI.Columns.Add(new DataColumn("cus_2", typeof(string)));
            dtSSI.Columns.Add(new DataColumn("cus_3", typeof(string)));
            dtSSI.Columns.Add(new DataColumn("cus_4", typeof(string)));
            dtSSI.Columns.Add(new DataColumn("cus_5", typeof(string)));
            dtSSI.Columns.Add(new DataColumn("cus_6", typeof(string)));
            dtSSI.Columns.Add(new DataColumn("cus_7", typeof(string)));
            dtSSI.Columns.Add(new DataColumn("cus_8", typeof(string)));
            dtSSI.Columns.Add(new DataColumn("cus_9", typeof(string)));
            dtSSI.Columns.Add(new DataColumn("cus_10", typeof(string)));
            dtSSI.Columns.Add(new DataColumn("cus_11", typeof(string)));


            string strCon = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Session["FilePath"].ToString() + ";Extended Properties='Excel 8.0;HDR=Yes;IMEX=1';";

            string sqlExcel = "select * from [" + SheetName + "][B:B]";
            OleDbConnection con = new OleDbConnection(strCon);
            con.Open();
            OleDbDataAdapter da = new OleDbDataAdapter(sqlExcel, con);
            DataSet ds = new DataSet();
            da.Fill(ds);
            DataTable dt = ds.Tables[0];
            if (dt.Rows.Count < 1)
            {
                RadAjaxManager1.Alert("No information on Sheet.");
                return;
            }

            foreach (DataRow dr in dt.Rows)
            {
                if (dr[0].ToString().Trim().Length > 0)
                {
                    if (dr[3].ToString().Trim().Length > 250)
                    {
                        RadAjaxManager1.Alert("ข้อมูล ProductSubject ในไฟล์ Excel ไม่ถูกต้อง (ความยาวเกิน 250 ตัวอักษร)!! กรุณาตรวจสอบความถูกต้องในไฟล์ Excel ให้เรียบร้อยก่อนนำเข้าข้อมูล และทำการเริ่มต้นใหม่อีกครั้ง");
                        GE.UPDATE_Executeion("UPDATE TblDataSource SET Active = 'N' WHERE SourceID = '" + lblDataSourceID.Text.Trim() + "' ");
                        return;
                    }
                    DataRow drNew = dtSSI.NewRow();
                    drNew["cus_1"] = dr[0].ToString();
                    drNew["cus_2"] = dr[1].ToString();
                    drNew["cus_3"] = dr[2].ToString();
                    drNew["cus_4"] = dr[3].ToString();
                    drNew["cus_5"] = dr[4].ToString();
                    drNew["cus_6"] = dr[5].ToString();
                    drNew["cus_7"] = dr[6].ToString();
                    drNew["cus_8"] = dr[7].ToString();
                    drNew["cus_9"] = dr[8].ToString();
                    drNew["cus_10"] = dr[9].ToString();
                    drNew["cus_11"] = dr[10].ToString();
                    dtSSI.Rows.Add(drNew);
                }
            }
            RadGrid1.DataSource = dtSSI;
            RadGrid1.DataBind();
            Session["dtSSI"] = dtSSI;
            lblTotalReccord.Text = "พบทั้งหมด : " + dtSSI.Rows.Count.ToString() + " รายการ";
           // GE.UPDATE_Executeion("UPDATE TblDataSource SET TotalSource = " + dtSSI.Rows.Count.ToString() + " WHERE SourceID = '" + lblDataSourceID.Text.Trim() + "' ");

        }
        catch (Exception ex)
        {
            RadAjaxManager1.Alert("ERROR: กรุณาตรวจสอบ Sheet และคอลั่มในไฟล์ที่ Import ให้ถูกต้องตามรูปแบบไฟล์ import อีกครั้ง!!  แนะนำในการ upload ไฟล์นี้อีกครั้งให้เปลี่ยนชื่อไฟล์ใหม่ " + ex.Message);
        }
    }
    protected void BtnConfirm_Click(object sender, EventArgs e)
    {
        try
        {
            DataTable dtSSI = (DataTable)Session["dtSSI"];
            foreach (DataRow drTmp in dtSSI.Rows)
            {
                string strSQL = " INSERT INTO TblCustomer (OwnerID,Fname,Lname,SourceID,Tel1,Tel2,Tel3,CntCall,Active,AppStatus,StatusID,SubStatusID "
                                                        + " ,ProductName,ProductSubject,ProductDatePost,CusUserName,ProductPrice,WebSite,CusProvince,CusWebName,ProductDateData) "
                                                        + " VALUES ('" + lblUserID.Text.Trim() + "','','','" + lblDataSourceID.Text.Trim() + "','" + drTmp["cus_1"].ToString() + "','" + drTmp["cus_2"].ToString() + "','',0,'Y',1,1,1 "
                                                        + " ,'" + drTmp["cus_3"].ToString() + "','" + drTmp["cus_4"].ToString() + "','" + drTmp["cus_5"].ToString() + "','" + drTmp["cus_8"].ToString() + "','" + drTmp["cus_7"].ToString() + "','" + drTmp["cus_6"].ToString() + "','" + drTmp["cus_9"].ToString() + "','" + drTmp["cus_10"].ToString() + "','" + drTmp["cus_11"].ToString() + "') "
                                                        ;
                GE.INSERT_Executeion(strSQL);
                    
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "AlertMessage",
                      "alert('นำเข้าข้อมูลสำเร็จ'); window.location='" +
                      Request.ApplicationPath + "/ImportList.aspx';", true);
        }
        catch (Exception ex)
        {
            RadAjaxManager1.Alert(ex.Message);
            GE.UPDATE_Executeion("UPDATE TblDataSource SET Active = 'N' WHERE SourceID = '" + lblDataSourceID.Text.Trim() + "' ");
        }
    }
}